/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.Sensor;
import org.pvoutput.integration.Util;


public class EnasolarLogReader extends AHttpLogReader
{	
	private static Logger LOGGER = Logger.getLogger(EnasolarLogReader.class);

	private boolean calculateEnergy = false;
	
	public void startup()
	throws Exception
	{
		super.startup();
		
		checkNonZeroEnergyStart = false;
		checkDecreasingEnergy = true;
		
		if(urls.length == 1)
		{
			String base = urls[0];
			
			if(!base.endsWith("/"))
			{
				base += "/";
			}
					
			urls = new String[]{base + "data.xml", base + "meters.xml"};
		}
		
		if(properties.containsKey("calculate_energy"))
		{
			String s = properties.getProperty("calculate_energy");
			
			calculateEnergy = "on".equalsIgnoreCase(s) || "yes".equalsIgnoreCase(s);
		}
		
		LOGGER.info("Calculate Energy: " + calculateEnergy);
		
		EnasolarFileWriter w = new EnasolarFileWriter();
		w.start();
	}
	
	public String getName()
	{
		return "enasolar";
	}
	
	public AHttpLogReader copy()
	{
		return new EnasolarLogReader();
	}
	
	public String getLogFilePrefix()
	{
		return "ENA";
	}
	
		
	private class EnasolarFileWriter extends SimpleLogFileWriter
	{
		public List<Output> parse(Sensor sensor, String s)
		{
			List<Output> outputs = new ArrayList<Output>();

			if(LOGGER.isDebugEnabled())
			{
				LOGGER.info("<<< " + s);
			}
			
			String sOutputPower = Util.getElement(s, "OutputPower");
			String sEnergyToday = Util.getElement(s, "EnergyToday");
			
			int power = (int)(Util.getDouble(sOutputPower, -1)*1000);
			
			double energy = -1;
			
			try
			{
				energy = Integer.parseInt(sEnergyToday, 16)*10;
			}
			catch(Exception e)
			{
				
			}
			
			if(power > -1 && energy > -1)
			{
				Output output = new Output();
				output.power = power;
				output.useEnergy = true;
				
				if(calculateEnergy)
				{
					output.energy = -1;	
				}
				else
				{
					output.energy = energy;
				}
				
				outputs.add(output);
			}
			else
			{
				LOGGER.error("Could not read data: power=" + power + ", energy=" + energy + " raw=" + s);
			}
			
			return outputs;
		}
	}
	
}
