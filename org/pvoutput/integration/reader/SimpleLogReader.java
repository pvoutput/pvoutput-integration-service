/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.pvoutput.integration.ADataLogReader;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.Util;
import org.pvoutput.integration.util.Constant;


public class SimpleLogReader extends ADataLogReader
{
	private static Logger LOGGER = Logger.getLogger(SimpleLogReader.class);
	
	public static final String HEADER = "DATE;TIME;TEMPERATURE;VOLTAGE;POWER;ENERGY;TOTAL;V7;V8;V9;V10;V11;V12";
	
	protected Date statusDateTime = null;
	protected Map<Date,Output> logEntries = new TreeMap<Date,Output>();
	protected SimpleDateFormat logFormat = new SimpleDateFormat("yyyyMMdd HH:mm");
	protected SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	protected SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	protected NumberFormat nfkwh = NumberFormat.getNumberInstance(Locale.US);
	protected NumberFormat nfwatts = NumberFormat.getNumberInstance(Locale.US);
	protected NumberFormat nftemp = NumberFormat.getNumberInstance(Locale.US);
	protected String delimiter = ";";
	
	protected SimpleDateFormat logfileDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	private String host;
	private int port = 80;
	private String query;
	private boolean secure = false;
	
	protected String columnNameTime;
	protected String columnNameDate;
	protected String columnNameEnergy;
	protected String columnNamePower;
	protected String columnNameTemperature;
	protected String columnNameVoltage;
	
	protected String columnNameV7;
	protected String columnNameV8;
	protected String columnNameV9;
	protected String columnNameV10;
	protected String columnNameV11;
	protected String columnNameV12;
	
	protected int columnIndexTime = -1;
	protected int columnIndexDate = -1;
	protected int columnIndexEnergy = -1;
	protected int columnIndexPower = -1;
	protected int columnIndexTemperature = -1;
	protected int columnIndexVoltage = -1;
	
	protected int columnIndexV7 = -1;
	protected int columnIndexV8 = -1;
	protected int columnIndexV9 = -1;
	protected int columnIndexV10 = -1;
	protected int columnIndexV11 = -1;
	protected int columnIndexV12 = -1;
	
	public SimpleLogReader()
	{
		nfkwh.setMinimumFractionDigits(3);
		nfkwh.setMaximumFractionDigits(3);
		nfkwh.setGroupingUsed(false);
		
		nfwatts.setMinimumFractionDigits(0);
		nfwatts.setMaximumFractionDigits(0);
		nfwatts.setGroupingUsed(false);
		
		nftemp.setMinimumFractionDigits(1);
		nftemp.setMaximumFractionDigits(1);
		nftemp.setGroupingUsed(false);
	}
	
	public void setSourceHeaders()
	{	
		if(properties != null)
		{
			String s = properties.getProperty("column.time");
			
			if(s != null && s.length() > 0)
			{
				columnNameTime = s;
			}
			
			s = properties.getProperty("column.power");
			
			if(s != null && s.length() > 0)
			{
				columnNamePower = s;
			}
			
			s = properties.getProperty("column.energy");
			
			if(s != null && s.length() > 0)
			{
				columnNameEnergy = s;
			}
			
			s = properties.getProperty("column.temperature");
			
			if(s != null && s.length() > 0)
			{
				columnNameTemperature = s;
			}
			
			s = properties.getProperty("column.voltage");
			
			if(s != null && s.length() > 0)
			{
				columnNameVoltage = s;
			}
			
			s = properties.getProperty("column.v7");
			
			if(s != null && s.length() > 0)
			{
				columnNameV7 = s;
			}
			
			s = properties.getProperty("column.v8");
			
			if(s != null && s.length() > 0)
			{
				columnNameV8 = s;
			}
			
			s = properties.getProperty("column.v9");
			
			if(s != null && s.length() > 0)
			{
				columnNameV9 = s;
			}
			
			s = properties.getProperty("column.v10");
			
			if(s != null && s.length() > 0)
			{
				columnNameV10 = s;
			}
			
			s = properties.getProperty("column.v11");
			
			if(s != null && s.length() > 0)
			{
				columnNameV11 = s;
			}
			
			s = properties.getProperty("column.v12");
			
			if(s != null && s.length() > 0)
			{
				columnNameV12 = s;
			}
		}
	}
	
	protected boolean validateHeaders() throws Exception
	{
		if(columnIndexEnergy == -1)
		{
			LOGGER.error("Could not find '" + columnNameEnergy + "' label");

			return false;
		}

		if(columnIndexPower == -1)
		{
			LOGGER.error("Could not find '" + columnNamePower + "' label");

			return false;
		}

		if(columnIndexTime == -1)
		{
			LOGGER.error("Could not find '" + columnNameTime + "' label");

			return false;
		}

		if(columnIndexDate == -1)
		{
			LOGGER.error("ERROR: Could not find '" + columnNameDate + "' label");

			return false;
		}

		return true;
	}
	
	@Override
	public String getLogFilename()
	{
		String pattern = "yyyy-MM-dd";
		
		String name = getLogFilename(pattern);
		
		if(name != null)
		{
			return name;
		}
		
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		return df.format(new java.util.Date()) + ".log";
	}
	
	@Override
    public File getLogFile()
    {
    	String pattern = "yyyy-MM-dd";
    	
    	File file = getLogFile(pattern);
    	
    	if(file != null)
    	{
    		return file;
    	}

        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return new File(dir, df.format(new java.util.Date()) + ".log");
    }
	
	@Override
	public void get()
	{
	}
	
	@Override
	public void startup() 
	throws Exception
	{	    
		checkNonZeroEnergyStart = true;
		checkDecreasingEnergy = true;
		
		if(properties.containsKey("delimiter"))
		{
			String s = (String)properties.get("delimiter");
			
			if(s.length() == 1)
			{
				delimiter = s;
				
				LOGGER.info("Delimiter: [" + delimiter + "]");
			}
		}
		
		File log = getLogFile();

		if(log.exists())
		{
			BufferedReader r = null;
			
			try
			{
				r = new BufferedReader(new FileReader(log));
				
				String line;
				
				int n = 0;
				
				while((line = r.readLine()) != null)
				{
					String[] v = line.split(delimiter);
					
					if(v != null && v.length == 7 && n > 0)
					{
						Output output = new Output();
						
						try
						{
							output.time = logFormat.parse(v[0] + " " + v[1]);
							output.temperature = Util.getDouble(v[2], Constant.DEFAULT_TEMPERATURE);
							output.voltage = Util.getDouble(v[3], Constant.DEFAULT_VOLTAGE);
							output.power = Integer.parseInt(v[4]);
							output.energy_change = Double.parseDouble(v[5]);
							output.energy =  Double.parseDouble(v[6]);
							
							logEntries.put(output.time, output);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
					else if(v != null && v.length == 6 && n > 0)
					{
						Output output = new Output();
						
						try
						{
							output.time = logFormat.parse(v[0] + " " + v[1]);
							output.temperature = Util.getDouble(v[2], Constant.DEFAULT_TEMPERATURE);
							output.power = Integer.parseInt(v[3]);
							output.energy_change = Double.parseDouble(v[4]);
							output.energy =  Double.parseDouble(v[3]);
							
							logEntries.put(output.time, output);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
					else if(v != null && v.length == 5 && n > 0)
					{
						Output output = new Output();
						
						try
						{
							output.time = logFormat.parse(v[0] + " " + v[1]);
							output.power = Integer.parseInt(v[2]);
							output.energy_change = Double.parseDouble(v[3]);
							output.energy =  Double.parseDouble(v[2]);
							
							logEntries.put(output.time, output);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
					

					n++;
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Could not read data", e);
			}
			finally
			{
				if(r != null)
				{
					try
					{
						r.close();
					}
					catch(Exception e)
					{
						
					}
				}
			}
		}		
		
		LOGGER.info("Read " + logEntries.size() + " log entries");
	}
	
	@Override
	public void shutdown()
	{	
	}
	
    @Override
    public String getLogDateFormat()
    {
        return "yyyyMMdd";
    }

    @Override
    public String getLogTimeFormat()
    {
        return "HH:mm";
    }
    
    @Override
	public void setLabels()
	{
        if(labelDate == null)
        	labelDate = "DATE";
        
        if(labelTime == null)
        	labelTime = "TIME";
        
        if(labelEnergy == null)
        	labelEnergy = "TOTAL";
        
        if(labelPower == null)
        	labelPower = "POWER";
        
        if(labelTemperature == null)
        	labelTemperature = "TEMPERATURE";
        
        if(labelVoltage == null)
        	labelVoltage = "VOLTAGE";
        
        if(labelV7 == null)
        	labelV7 = "V7";
        
        if(labelV8 == null)
        	labelV8 = "V8";
        
        if(labelV9 == null)
        	labelV9 = "V9";
        
        if(labelV10 == null)
        	labelV10 = "V10";
        
        if(labelV11 == null)
        	labelV11 = "V11";
        
        if(labelV12 == null)
        	labelV12 = "V12";
        
        setSourceHeaders();
	}
    
    @Override
    public void read() throws Exception
    {
        initReader();
        
		BufferedReader br = null;

		try
		{
			br = new BufferedReader(new FileReader(logfile));
			String line;
			boolean startRead = false;
			
			indexDate = 0;
			
            int columns = 0;

			while((line = br.readLine()) != null)
			{
				if(!startRead)
				{
					String[] measurementArray = line.split(delimiter);

                    columns = measurementArray.length;

					for(int i = 0; i < columns; i++)
					{
						if(labelEnergy.equalsIgnoreCase(measurementArray[i]))
						{
							indexEnergy = i;
						}
						else if(labelPower.equalsIgnoreCase(measurementArray[i]))
						{
							indexPower = i;
						}
						else if(labelTime.equalsIgnoreCase(measurementArray[i]))
						{
							indexTime = i;
						}
						else if(labelDate.equalsIgnoreCase(measurementArray[i]))
						{
							indexDate = i;
						}
						else if(labelTemperature.equalsIgnoreCase(measurementArray[i]))
						{
							indexTemperature = i;
						}
						else if(labelVoltage.equalsIgnoreCase(measurementArray[i]))
						{
							indexVoltage = i;
						}
						else if(labelV7.equalsIgnoreCase(measurementArray[i]))
						{
							indexV7 = i;
						}
						else if(labelV8.equalsIgnoreCase(measurementArray[i]))
						{
							indexV8 = i;
						}
						else if(labelV9.equalsIgnoreCase(measurementArray[i]))
						{
							indexV9 = i;
						}
						else if(labelV10.equalsIgnoreCase(measurementArray[i]))
						{
							indexV10 = i;
						}
						else if(labelV11.equalsIgnoreCase(measurementArray[i]))
						{
							indexV11 = i;
						}
						else if(labelV12.equalsIgnoreCase(measurementArray[i]))
						{
							indexV12 = i;
						}
					}

					if(!validateLabels())
					{
						return;
					}
					
                    startRead = true;
				}
				else
				{
					String[] data = line.split(delimiter);

                    if(data.length >= columns)
                    {
                    	SimpleDateFormat df = new SimpleDateFormat(getLogDateFormat() + " " + getLogTimeFormat());
                    	
                    	Date datetime = df.parse(data[indexDate] + " " + data[indexTime]);
                    	
                        add(data
                        	, datetime
                        	, data[indexEnergy]
                        	, data[indexPower]
                        	, indexTemperature > -1 ? data[indexTemperature] : null
                        	, indexVoltage > -1 ? data[indexVoltage] : null
                        	, "1");

                        if(date == null)
                        {
                        	SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
                        	
                        	// should not be null
                        	if(logFileDateFormat == null)
                        	{
                        		setDataFormats();
                        	}
                        	
                            date = logFileDateFormat.parse(data[indexDate]);
                            graphSubtitle = df2.format(date);
                        }
                    }
				}
			}
		}
		catch(Exception e)
		{
			throw e;
		}
        finally
        {
            if(br != null) 
            {	
            	try
            	{
            		br.close();
            	}
            	catch(Exception e)
            	{
            	}
            }
        }
	}

    protected void addLog(Calendar c) 
	{
		int today = c.get(Calendar.DAY_OF_YEAR);
		
		BufferedWriter bw = null;
		
		try
		{				
			File logfile = getLogFile();

			bw = new BufferedWriter(new FileWriter(logfile));
			
			if(logfile.length() == 0)
			{
				bw.write(HEADER);
				bw.newLine();
				bw.flush();
			}
			
			Iterator<Output> i = logEntries.values().iterator();
			
			double lastEnergy = -1;
			
			while(i.hasNext())
			{
				Output output = i.next();
				
				c.setTime(output.time);
				
				int currentDay = c.get(Calendar.DAY_OF_YEAR);
				
				// remove yesterday's entries
				if(currentDay != today)
				{
					i.remove();
					
					continue;
				}
				
				// check energy value
				if(systemSize > -1 && getDirection() == EXPORT)
				{
					Calendar c2 = Calendar.getInstance();
					c2.setTime(output.time);
					int hour = c2.get(Calendar.HOUR_OF_DAY);
					int minute = c2.get(Calendar.MINUTE);

					int t = hour * 60 + minute;
					
					double gss = output.energy/(systemSize+0.0);
					
					if(!isValidEnergyEfficiency(gss, t))
					{
						if(LOGGER.isDebugEnabled())
						{
							LOGGER.warn("Energy [" + output.energy + "] too high at [" + output.time + "] eff[" + gss + "] size[" + systemSize + "]");
						}
						
						continue;
					}
				}
				
				// skip where energy has decreased due to not enough inverters reporting
				if(checkDecreasingEnergy)
				{
					if(output.energy <  lastEnergy)
					{
						LOGGER.warn("Skipped " + timeFormat.format(output.time) + " [" + output.energy + "/" + lastEnergy + "]");
						
						continue;
					}
				}
				
				bw.write(getDataLine(output));
				
				bw.newLine();
				
				lastEnergy = output.energy;
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Could not read data", e);
		}
		finally
		{
			if(bw != null)
			{
				try
				{
					bw.close();
				}
				catch(Exception e)
				{
					
				}
			}
		}
	}

    protected String getDataLine(Output output)
    {
    	StringBuffer s = new StringBuffer();
		s.append(dateFormat.format(output.time))
		.append(delimiter)
		.append(timeFormat.format(output.time))
		.append(delimiter);
		
		if(Util.validTemperature(output.temperature))
		{
			s.append(nftemp.format(output.temperature).replace(',', '.'));
		}
		else
		{
			s.append(nftemp.format(Constant.DEFAULT_TEMPERATURE).replace(',', '.'));
		}
		
		s.append(delimiter);
		
		if(Util.validVoltage(output.voltage))
		{
			s.append(nftemp.format(output.voltage).replace(',', '.'));
		}
		else
		{
			s.append(nftemp.format(Constant.DEFAULT_VOLTAGE).replace(',', '.'));
		}
		
		s.append(delimiter)
		.append(nfwatts.format(output.power).replace(',', '.'))
		.append(delimiter)
		.append(nfkwh.format(output.energy_change).replace(',', '.'))
		.append(delimiter)
		.append(nfkwh.format(output.energy).replace(',', '.'));
		
		s.append(delimiter);
		
		if(!Double.isNaN(output.v7))
		{
			s.append(nfkwh.format(output.v7));
		}
		else
		{
			s.append("NaN");
		}
		
		s.append(delimiter);
		
		if(!Double.isNaN(output.v8))
		{
			s.append(nfkwh.format(output.v8));
		}
		else
		{
			s.append("NaN");
		}
		
		s.append(delimiter);
		
		if(!Double.isNaN(output.v9))
		{
			s.append(nfkwh.format(output.v9));
		}
		else
		{
			s.append("NaN");
		}
		
		s.append(delimiter);
		
		if(!Double.isNaN(output.v10))
		{
			s.append(nfkwh.format(output.v10));
		}
		else
		{
			s.append("NaN");
		}
		
		s.append(delimiter);
		
		if(!Double.isNaN(output.v11))
		{
			s.append(nfkwh.format(output.v11));
		}
		else
		{
			s.append("NaN");
		}
		
		s.append(delimiter);
		
		if(!Double.isNaN(output.v12))
		{
			s.append(nfkwh.format(output.v12));
		}
		else
		{
			s.append("NaN");
		}
		
		return s.toString();
    }
    
	protected boolean isSecure()
	{
		return secure;
	}
	
	protected void parseURL(String url) 
	{
		if(url.startsWith("http://"))
		{
			url = url.substring(7);
		}
		else if(url.startsWith("https://"))
		{
			url = url.substring(8);
			secure = true;
			port = 443;
		}
				
		int portIndex = url.indexOf(':');
		int queryIndex = url.indexOf('/');
		
		if(portIndex > -1)
		{
			host = url.substring(0, portIndex);
			
			if(queryIndex > -1)
			{
				port = Util.getNumber(url.substring(portIndex+1, queryIndex));
			}
			else
			{
				port = Util.getNumber(url.substring(portIndex+1));
			}
			
			if(queryIndex > -1)
			{
				query = url.substring(queryIndex);
			}
		}
		else
		{
			if(queryIndex > -1)
			{
				host = url.substring(0, queryIndex);
				
				query = url.substring(queryIndex);
			}
			else
			{
				host = url;
			}
		}
		
		if(query != null)
		{
			query = query.trim();
		}
		
		if(port < 1)
		{
			if(secure)
			{
				port = 443;
			}
			else
			{
				port = 80;
			}
		}
		
		if(LOGGER.isDebugEnabled())
		{
			LOGGER.debug(">>> http://" + host + ":" + port + query);
		}
	}
	
	protected String getHttpHost()
	{
		return host;
	}
	
	protected int getHttpPort()
	{
		return port;
	}
	
	protected String getHttpQuery()
	{
		return query;
	}

}
