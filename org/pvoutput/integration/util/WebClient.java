/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.util;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.log4j.Logger;
import org.pvoutput.integration.Main;

public class WebClient 
{
	private static Logger LOGGER = Logger.getLogger(WebClient.class);
	
	private WebClient()
	{
			
	}
	
	public static HttpClient getWebClient()
	{	
		
		ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager();
		cm.setMaxTotal(100);
		
		HttpClient httpclient = new DefaultHttpClient(cm);
		HttpParams params = httpclient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, Constant.HTTP_CONNECT_TIMEOUT);
		HttpConnectionParams.setSoTimeout(params, Constant.HTTP_SO_TIMEOUT);
		HttpConnectionParams.setLinger(params, 30);
		
		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};
		
		try
		{
			SSLContext sslcontext = SSLContext.getInstance("SSL");
			sslcontext.init(null, trustAllCerts, new java.security.SecureRandom());
			SSLSocketFactory sf = new SSLSocketFactory(sslcontext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			Scheme https = new Scheme("https", 443, sf);
			cm.getSchemeRegistry().register(https);
		}
		catch(Exception e)
		{
			LOGGER.error("Could not add SSL", e);
		}
		
		return httpclient;
		
	}
	
	public static HttpPost getPVOutputPost(String hostname, int port, String query, String sid, String key, String format, String data)
	{
		StringBuffer url = new StringBuffer();
		url.append("http://").append(hostname).append(":").append(port).append(query);
		
		HttpPost http = new HttpPost(url.toString());
		http.setHeader("X-Pvoutput-SystemId", sid);
		http.setHeader("X-Pvoutput-Apikey", key);
				
		if(format != null)
		{
			http.setHeader("User-Agent", "PVOutput/" + Main.VERSION + " (SystemId:" + sid + "; Inverter:" + format + ")");
		}
		else
		{
			http.setHeader("User-Agent", "PVOutput/" + Main.VERSION);
		}
		
		if(data != null)
		{
			try
			{
				StringEntity in = new StringEntity(data);
				http.setEntity(in);
			}
			catch(Exception e)
			{
				
			}
		}
		
		return http;
	}
	
	public static HttpGet getPVOutputGet(String hostname, int port, String query, String sid, String key, String format)
	{
		StringBuffer url = new StringBuffer();
		url.append("http://").append(hostname).append(":").append(port).append(query);
		
		LOGGER.info(">>> " + url.toString());
		
		HttpGet http = new HttpGet(url.toString());
		http.setHeader("X-Pvoutput-SystemId", sid);
		http.setHeader("X-Pvoutput-Apikey", key);
		
		if(format != null)
		{
			http.setHeader("User-Agent", "PVOutput/" + Main.VERSION + " (SystemId:" + sid + "; Inverter:" + format + ")");
		}
		else
		{
			http.setHeader("User-Agent", "PVOutput/" + Main.VERSION);
		}
		
		return http;
	}
	
	public static HttpGet getHttpGet(String hostname, int port, String query, boolean secure)
	{
		StringBuffer url = new StringBuffer();
		
		if(secure)
		{
			url.append("https://").append(hostname).append(":").append(port).append(query);
		}
		else
		{
			url.append("http://").append(hostname).append(":").append(port).append(query);
		}
		
		LOGGER.info(">>> " + url.toString());
		
		HttpGet http = new HttpGet(url.toString());		
		http.setHeader("User-Agent", "PVOutput/" + Main.VERSION);
		
		return http;
	}
	
	/*
	public static ContentExchange getExchange(String hostname, int port, String query, boolean secure)
	{
		ContentExchange contentExchange = new ContentExchange();
		contentExchange.setTimeout(15000);
		
		contentExchange.setRequestHeader("User-Agent", "PVOutput/" + Main.VERSION);
		
		StringBuffer b = new StringBuffer();
		
		if(secure)
		{
			b.append("https://").append(hostname).append(":").append(port).append(query);
		}
		else
		{
			b.append("http://").append(hostname).append(":").append(port).append(query);
		}
		
		LOGGER.info(">>> " + b.toString());
		
		contentExchange.setURL(b.toString());
		
		return contentExchange;
	}*/
	
	public static String encodeValues(String s)
	{
		int offset = s.indexOf('?');
		StringBuffer q = new StringBuffer();
		q.append(s.substring(0, offset));
		
		if(offset  > 0)
		{
			q.append("?");
			
			String s1 = s.substring(offset+1);
			
			String[] pairs = s1.split("\\&");
			
			int n = 0;
			
			for(String pair: pairs)
			{
				String[] nameValue = pair.split("=");
				
				if(nameValue != null && nameValue.length == 2)
				{
					if(n > 0)
					{
						q.append("&");
					}
						
					q.append(nameValue[0]).append("=").append(org.eclipse.jetty.util.UrlEncoded.encodeString(nameValue[1]));
				
					n++;
				}
			}
		}
		
		return q.toString();
	}
		
}
