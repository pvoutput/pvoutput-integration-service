/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import org.tanukisoftware.wrapper.WrapperManager;
import org.tanukisoftware.wrapper.WrapperListener;

public class Main implements WrapperListener
{
    private Thread application;
    private Controller controller;
    
    public static final String VERSION = "1.4.7.1";

    private Main()
    {
    	File fconfig = new File("../conf/pvoutput.ini");

		Properties config = new Properties();
		
		if(fconfig.exists())
		{
			FileInputStream in = null;
			
			try
			{
				in = new FileInputStream(fconfig);
				config.load(in);
				
				String timezone = config.getProperty("timezone");
				
				if(timezone != null && timezone.length() > 0)
				{
					TimeZone.setDefault(TimeZone.getTimeZone(timezone));
				}
			}
			catch(Exception e)
			{
			}
			finally
			{
				if(in != null)
				{
					try
					{
						in.close();
					}
					catch(Exception e)
					{
					}
				}
			}
		}
    }

    public Integer start( String[] args )
    {
        System.out.println("*** Starting PVOutput Integration Service v" + VERSION);

        try
        {
        	Properties p = System.getProperties();
        	
        	Set<String> set = p.stringPropertyNames();
        	
        	for(String key: set)
        	{
        		if(key.startsWith("java") || key.startsWith("os"))
        		{
        			System.out.println(">> " + key + "=" + p.getProperty(key));
        		}
        	}
        }
        catch(Exception e)
        {	
        }
        
        try
        {
        	controller = new Controller();
            application = new Thread(controller);
        }
        catch(Exception e)
        {
            System.err.println(e.getMessage());

            return 1;
        }

        application.setDaemon(false);
        application.start();
        
        return null;
    }

    public int stop( int exitCode )
    {
        System.out.println("*** Stopping PVOutput Integration Service v" + VERSION + " (" + exitCode + ")");

        application.interrupt();

        controller.shutdown();
        	
        return exitCode;
    }

    public void controlEvent( int event )
    {
        if (( event == WrapperManager.WRAPPER_CTRL_LOGOFF_EVENT ) && ( WrapperManager.isLaunchedAsService() || WrapperManager.isIgnoreUserLogoffs()))
        {
        }
        else
        {
            WrapperManager.stop(0);
        }
    }

    public static void main( String[] args )
    {
        WrapperManager.start( new Main(), args );
    }
}