/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.pvoutput.integration.ADataLogReader;

public class OelmaierLogReader extends ADataLogReader
{
	private String DELIMITER = ";";
	
	@Override
	public void startup()
	{
		checkNonZeroEnergyStart = true;
		checkDecreasingEnergy = true;
	}
	
	@Override
	public void shutdown()
	{	
	}
	
    @Override
    public String getLogDateFormat() {
        return "dd.MM.yy";
    }

    @Override
    public String getLogTimeFormat() {
        return "HH:mm:ss";
    }
	
	@Override
	public String getLogFilename()
	{
		String pattern = "yyMMdd";
		
		String name = getLogFilename(pattern);
		
		if(name != null)
		{
			return name;
		}
		
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		
		return "min" + df.format(new java.util.Date()) + ".csv";
	}
	
    @Override
    public File getLogFile() {
    	
    	String pattern = "yyMMdd";
    	
    	File f = getLogFile(pattern);

    	if(f != null)
    	{
    		return f;
    	}
    	
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		return new File(dir, "min" + df.format(new java.util.Date()) + ".csv");
    }
    

    @Override
	public void setLabels()
	{
        if(labelTime == null)
        	labelTime = "#Datum";
        
        if(labelTime == null)
        	labelTime = "Uhrzeit";
        
        if(labelEnergy == null)
        	labelEnergy = "DaySum";
        
        if(labelPower == null)
        	labelPower = "Pac";
	}
    
	@Override
	public void get()
	{
	}
		
    @Override
    public void read() throws Exception
    {
    	initReader();
        
        BufferedReader br = null;

		try
		{
			br = new BufferedReader(new FileReader(logfile));
			String line;
			boolean startRead = false;
			
            int columns = 0;

            List<String> fileBuf = new ArrayList<String>();
            
            Date lastTime = null;
            
			while((line = br.readLine()) != null)
			{
				if(!startRead)
				{
					String[] measurementArray = line.split(DELIMITER);

                    columns = measurementArray.length;

					for(int i = 0; i < columns; i++)
					{
						if(labelEnergy.equalsIgnoreCase(measurementArray[i]))
						{
							indexEnergy = i;
						}
						else if(labelPower.equalsIgnoreCase(measurementArray[i]))
						{
							indexPower = i;
						}
						else if(labelTime.equalsIgnoreCase(measurementArray[i]))
						{
							indexTime = i;
						}
						else if(labelDate.equalsIgnoreCase(measurementArray[i]))
						{
							indexDate = i;
						}
					}

					if(!validateLabels())
					{
						return;
					}
					
                    startRead = true;
				}
				else
				{
					String[] data = line.split(DELIMITER);
					
					try
					{
						Date time = logFileTimeFormat.parse(data[indexTime]);	
						
						if(lastTime == null)
						{
							fileBuf.add(line);
						}
						else
						{
							if(time.before(lastTime))
							{
								long diff = lastTime.getTime() - time.getTime();
								
								// 8 hour difference
								if(diff/1000/3600 > 8)
								{
									continue;
								}
								
								fileBuf.add(line);
							}
						}
						
						lastTime = time;
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			
			// read in reverse
			for(int i = fileBuf.size()-1; i > -1; i--)
			{
				String[] data = fileBuf.get(i).split(DELIMITER);

	            if(data.length == columns)
	            {
	                add(data, data[indexTime], data[indexEnergy], data[indexPower], null, null, "1");
	            
	                if(date == null)
	                {
	                	SimpleDateFormat df = new SimpleDateFormat("dd.MM.yy");
	                	
	                    date = logFileDateFormat.parse(data[indexDate]);
	                    graphSubtitle = df.format(date);
	                }
	            }
			}
		}
		catch(Exception e)
		{
			throw e;
		}
        finally
        {
            if(br != null) try{br.close();}catch(Exception e){}
        }
    }

}
