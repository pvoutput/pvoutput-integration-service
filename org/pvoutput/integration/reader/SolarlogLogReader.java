/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.pvoutput.integration.ADataLogReader;
import org.pvoutput.integration.Util;
import org.pvoutput.integration.util.Constant;

public class SolarlogLogReader extends ADataLogReader
{
	private String DELIMITER = ";";
	
	//private static Logger LOGGER = Logger.getLogger(SolarlogLogReader.class);
	
	@Override
	public void startup()
	{
		checkNonZeroEnergyStart = true;
		checkDecreasingEnergy = true;
	}
	
	@Override
	public void shutdown()
	{	
	}
	
    @Override
    public String getLogDateFormat() {
        return "dd.MM.yy";
    }

    @Override
    public String getLogTimeFormat() {
        return "dd.MM.yy HH:mm:ss";
    }
	
	@Override
	public String getLogFilename()
	{
		String pattern = "yyMMdd";
		
		String name = getLogFilename(pattern);
		
		if(name != null)
		{
			return name;
		}

		return "min_day.js";
	}
	
    @Override
    public File getLogFile() {
    	
    	String pattern = "yyMMdd";
    	
    	File f = getLogFile(pattern);

    	if(f != null)
    	{
    		return f;
    	}
    	
		return new File(dir, "min_day.js");
    }
    

    @Override
	public void setLabels()
	{
	}
    
	@Override
	public void get()
	{
	}
		
    @Override
    public void read() throws Exception
    {
    	initReader();
        
        BufferedReader br = null;

        Calendar c = Calendar.getInstance();
		int currentDay = c.get(Calendar.DAY_OF_YEAR);
        
		try
		{
			br = new BufferedReader(new FileReader(logfile));
			String line;
			
            List<String> fileBuf = new ArrayList<String>();
            
            Date lastTime = null;
            
			while((line = br.readLine()) != null)
			{
				// no headers for solar log
				if(line.startsWith("m[mi++]="))
				{
					String[] segs = line.substring(8).replaceAll("\"", "").split("\\|");

					if(segs.length >= 2)
					{
						Date time = null;
						
						try
						{
							time = logFileTimeFormat.parse(segs[0]);
							
							Calendar logc = Calendar.getInstance();
							logc.setTime(time);
							
							if(logc.get(Calendar.DAY_OF_YEAR) != currentDay)
							{
								return;
							}
						}
						catch(Exception e)
						{	
						}
						
						if(time != null)
						{
							int totalPower = 0;
							int totalEnergy = 0;
							boolean valid = true;
							double voltage = Constant.DEFAULT_VOLTAGE;
							double temperature = Constant.DEFAULT_TEMPERATURE;
							
							// aggregate multiple inverter data
							for(int j = 1; j < segs.length; j++)
							{
								String[] data = segs[j].split(DELIMITER);
								
								if(data.length >= 4)
								{
									int power = -1;
									int energy = -1;
									
									
									try
									{
										power = Integer.parseInt(data[0]);
										energy = Integer.parseInt(data[2]);
									}
									catch(Exception e)
									{
										valid = false;
										
										continue;
									}
									
									voltage = Util.getDouble(data[3], Constant.DEFAULT_VOLTAGE);
									
									if(data.length >= 5)
									{
										temperature = Util.getDouble(data[4], Constant.DEFAULT_TEMPERATURE);
									}
									
									totalPower += power;
									totalEnergy += energy;
								}
							}
							
							if(valid)
							{
								StringBuffer newLine = new StringBuffer();
								newLine.append(segs[0]).append(DELIMITER).append(totalPower)
								.append(DELIMITER).append(totalEnergy)
								.append(DELIMITER).append(temperature)
								.append(DELIMITER).append(voltage);
								
								if(lastTime == null)
								{
									fileBuf.add(newLine.toString());
								}
								else
								{
									if(time.before(lastTime))
									{
										long diff = lastTime.getTime() - time.getTime();
										
										// 8 hour difference
										if(diff/1000/3600 > 8)
										{
											continue;
										}
										
										fileBuf.add(newLine.toString());
									}
								}
								
								lastTime = time;
							}
						}
					}
				}
			}
			
			// read in reverse
			for(int i = fileBuf.size()-1; i > -1; i--)
			{
				String[] data = fileBuf.get(i).split(DELIMITER);
				
				Date time = logFileTimeFormat.parse(data[0]);
				
				add(data, time, data[2], data[1], data[3], data[4], "1");
				
				if(date == null)
                {
					SimpleDateFormat df = new SimpleDateFormat("dd.MM.yy");
                    graphSubtitle = df.format(time);
                }
			}
		}
		catch(Exception e)
		{
			throw e;
		}
        finally
        {
            if(br != null) try{br.close();}catch(Exception e){}
        }
    }

}
