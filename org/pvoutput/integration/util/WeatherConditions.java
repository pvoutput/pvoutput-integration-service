/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Util;

public class WeatherConditions 
{
	private static Logger LOGGER = Logger.getLogger(WeatherConditions.class);
	
	private Map<Integer,String> conditions = new HashMap<Integer,String>();
		
	public WeatherConditions()
	{
		init();
	}
	
	public String getCondition(int i)
	{
		if(conditions.containsKey(i))
		{
			return conditions.get(i);
		}
		
		return "Not Sure";
	}
		
	private void init()
	{
		File fconfig = new File("../conf/weather.ini");

		if(fconfig.exists())
		{
			BufferedReader br = null;
			String line;
			
			try
			{
				br = new BufferedReader(new FileReader(fconfig));
				
				while((line = br.readLine()) != null)
				{
					String[] d = line.split(",");
					
					if(d != null && d.length == 3)
					{
						int n = Util.getNumber(d[0]);
						
						if(n > 0)
						{
							conditions.put(n, d[2]);
						}
						else
						{
							continue;
						}
					}
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Processing error", e);
			}
	        finally
	        {
	            if(br != null)
	            {
		            try 
		            {
		            	br.close();
		            }
		            catch(Exception e)
		            {
		            }
	            }
	        }
		}
	}
	
}
