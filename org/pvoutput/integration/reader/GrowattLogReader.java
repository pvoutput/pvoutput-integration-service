/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.util.Constant;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;


public class GrowattLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(GrowattLogReader.class);
		
	private SimpleDateFormat dbTimeFormat = new SimpleDateFormat("yyyyMMdd h:mm:ss aa");
	private String[] serial_numbers = null;
	private boolean calculateEnergy = false;
	
	public void startup()
	throws Exception
	{
		super.startup();
		
		// allow logs to start with non-zero energy data
		checkNonZeroEnergyStart = false;
		checkDecreasingEnergy = true;
		
		if(properties.containsKey("serial_number"))
		{
			String sSerials = (String)properties.get("serial_number");
			
			serial_numbers = sSerials.split(",");
		}
		else
		{
			serial_numbers = new String[]{"000000000"};
			
			LOGGER.error("The 'serial_number' value not set in growatt.ini");
		}
		
		if(properties.containsKey("time_format"))
		{
			String timeformat = (String)properties.get("time_format");
			
			dbTimeFormat = new SimpleDateFormat("yyyyMMdd " + timeformat);
		}
		
		LOGGER.info("Time Format: " + dbTimeFormat.toPattern());
		
		if(properties.containsKey("calculate_energy"))
		{
			String s = properties.getProperty("calculate_energy");
			
			calculateEnergy = "on".equalsIgnoreCase(s) || "yes".equalsIgnoreCase(s);
		}
		
		LOGGER.info("Calculate Energy: " + calculateEnergy);

		Calendar c = Calendar.getInstance();
		
		int n = 1;
		
		for(String serial : serial_numbers)
		{
			File f = new File(dir + "/" + serial.trim(), serial.trim() + "_" + String.valueOf(c.get(Calendar.YEAR)) + String.valueOf(c.get(Calendar.MONTH)+1) + ".MDB");
			
			LOGGER.info("MDB " + n + ": " + f.getAbsolutePath());
			
			n++;
		}
	}
	
	@Override
	public void get()
	{
		logEntries.clear();
		
		int added = 0;
		
		Calendar c = Calendar.getInstance();
		
		for(String serial : serial_numbers)
		{
			Set<Date> fileEntries = new HashSet<Date>();
			
			File f = new File(dir + "/" + serial.trim(), serial.trim() + "_" + String.valueOf(c.get(Calendar.YEAR)) + String.valueOf(c.get(Calendar.MONTH)+1) + ".MDB");
			
			if(f.exists())
			{
				Database db = null;
				double total = 0;
				
				try
				{
					db = Database.open(f, true);
					
					if(db == null)
					{
						LOGGER.error("Could not open database: " + f.getAbsolutePath());
					
						return;
					}
					
					Table table = db.getTable(String.valueOf(c.get(Calendar.DAY_OF_MONTH)));

					if(table == null)
					{
						LOGGER.error("Could not access table: '" + String.valueOf(c.get(Calendar.DAY_OF_MONTH)) + "' in " + f.getAbsolutePath());
					
						return;
					}
					
					java.util.Date lastDate = null;
					 
					for(java.util.Map<String, Object> row : table) 
					{
						String d = dateFormat.format(c.getTime());
						
						Date currentDate = null;
		                long elapse = -1;
		                
						try
						{
							currentDate = dbTimeFormat.parse(d + " " + (String)row.get("TimeNow"));
						}
						catch(ParseException e)
						{
							throw new Exception("Time [" + d + " " + (String)row.get("TimeNow") + "] does not match [" + dbTimeFormat.toPattern() + "]");
						}
						
						if(lastDate != null)
		                {
		                    elapse = currentDate.getTime() - lastDate.getTime();
		                }
												
						try
						{							
							c.setTime(currentDate);
							c.set(Calendar.SECOND, 0);
							c.set(Calendar.MILLISECOND, 0);
							
							if(!fileEntries.contains(c.getTime()))
							{
								fileEntries.add(c.getTime());
							}
							else
							{
								continue;
							}
							
							Output o = null;
							
							if(logEntries.containsKey(c.getTime()))
							{
								o = logEntries.get(c.getTime());
							}
							else
							{
								o = new Output();
								o.time = c.getTime();
								o.power = 0;
								o.energy = 0;
								
								logEntries.put(c.getTime(), o);
							}
							
							Object opac = row.get("Pac");
							
							int power = 0;
							
							if(opac instanceof Double)
							{
								power = (int)Math.round(((Double)opac));
							}
							else if(opac instanceof String)
							{
								power = (int)Math.round(Double.parseDouble((String)opac));
							}
							
							o.power += power;
							
							if(calculateEnergy)
							{
								if(elapse > 0)
				                {
									// limit calculation to 5 minutes
									if(elapse > 60000*Constant.ELAPSED_LIMIT)
									{
										double e = power/((60000/(60000*Constant.ELAPSED_LIMIT+0.0))*60000)*1000;
										
										LOGGER.warn("Elapsed time greater than " + Constant.ELAPSED_LIMIT + " minutes, elapse: " + nfkwh.format((elapse/60000.0)) + ", power: " + nfkwh.format(power) + ", energy: " + nfkwh.format(e));
										
										elapse = 60000*Constant.ELAPSED_LIMIT;
									}
									
				                    double energy = power/((60000/(elapse+0.0))*60000)*1000;
				                    total += energy;
				                }
								
								o.energy += Math.round(total);
							}
							else
							{
								Object oeac = row.get("Eac_today");
								
								if(opac instanceof Double)
								{
									o.energy += ((Double)oeac)*1000;
								}
								else if(opac instanceof String)
								{
									o.energy += Double.parseDouble((String)oeac)*1000;
								}
							}
							
							Object otemp = row.get("Temperature");
							
							if(otemp instanceof Double)
							{
								o.temperature = (Double)otemp;
							}
							else if(otemp instanceof String)
							{
								o.temperature = Double.parseDouble((String)otemp);
							}
							
							Object ovolt = row.get("Vac");
							
							if(ovolt instanceof Double)
							{
								o.voltage = (Double)ovolt;
							}
							else if(ovolt instanceof String)
							{
								o.voltage = Double.parseDouble((String)ovolt);
							}
							
							added++;
							
							lastDate = currentDate;
						}
						catch(Exception e)
						{
							LOGGER.error("Could not add output record", e);
						}
					}
				}
				catch(Exception e)
				{
					LOGGER.error("Could not process data", e);
				}
				finally
				{
					if(db != null)
					{
						try
						{
							db.close();
						}
						catch(Exception e)
						{
							LOGGER.error("Could not close mdb", e);
						}
					}
				}

				if(LOGGER.isDebugEnabled())
				{
					LOGGER.debug("Added " + added + " records from " + f.getAbsolutePath());
				}
			}
			else
			{
				LOGGER.warn("Database file " + f.getAbsolutePath() + " not found");
			}
		}
		
		if(added > 0)
		{
			addLog(c);
		}
	}

	@Override
	public String getLogFilename()
	{
        StringBuffer s = new StringBuffer();
        
        s.append("GW").append("-").append(logfileDateFormat.format(new Date())).append(".log");
        
        return s.toString();
	}
	
	@Override
    public File getLogFile()
    {
        return new File(dir, "GW" + "-" + logfileDateFormat.format(new Date()) + ".log");
    }

}
