/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.pvoutput.integration.ADataLogReader;
import org.pvoutput.integration.Channel;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.Sensor;
import org.pvoutput.integration.Util;
import org.pvoutput.integration.util.Constant;
import org.pvoutput.integration.util.SerialPortDataPeek;


public class CurrentcostLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(CurrentcostLogReader.class);

	private int sensorId = 0;
	private int rate = 57600;
	private String portName = "COM2";
	private CommPort commPort;
	private double energyExport;
	private double energyImport;
	private HashMap<Integer,Sensor> sensors = null;
	private String rule;
	private String temperatureUnit;
	private bsh.Interpreter interpreter;
	private CommPortIdentifier portIdentifier = null;
	private long lastSerialActivity = System.currentTimeMillis();
	private int portDetectWait = 15;
	private int portIdleRecycle = 60;
	
	private int v7Sensor = -1;
	private Map<String,Channel> v7Channels = null;
	
	private int v8Sensor = -1;
	private Map<String,Channel> v8Channels = null;
	
	private int v9Sensor = -1;
	private Map<String,Channel> v9Channels = null;
	
	private int v10Sensor = -1;
	private Map<String,Channel> v10Channels = null;
	
	private int v11Sensor = -1;
	private Map<String,Channel> v11Channels = null;
	
	private int v12Sensor = -1;
	private Map<String,Channel> v12Channels = null;
	
	public CurrentcostLogReader()
	{
		checkNonZeroEnergyStart = false;
	}
	
	@Override
	public String getLogFilename()
	{
        return "CC" + sensorId + "-" + logfileDateFormat.format(new Date()) + ".log";
	}
	
	@Override
    public File getLogFile()
    {
        return new File(dir, "CC" + sensorId + "-" + logfileDateFormat.format(new Date()) + ".log");
    }
	
	public HashMap<Integer,Sensor> getSensors()
	{
		return sensors;
	}
			
	
	public void startup()
	throws Exception
	{		
		sensors = new HashMap<Integer,Sensor>();
		
		if(properties.containsKey("port"))
		{
			portName = properties.getProperty("port", "COM2");
		}
		
		if(properties.containsKey("rule"))
		{
			rule = properties.getProperty("rule");
			
			if(rule != null && rule.trim().length() > 0)
			{
				interpreter = new bsh.Interpreter();
				
				LOGGER.info("Rule: " + rule);
			}
		}
		
		if(properties.containsKey("temperature-unit"))
		{
			if("C".equals(properties.getProperty("temperature-unit", "C")))
			{
				temperatureUnit = "C";
			}
			else
			{
				temperatureUnit = "F";
			}
			
			LOGGER.info("Temperature Unit: " + temperatureUnit);
		}
		
		try
		{
			rate = Integer.parseInt(properties.getProperty("rate", "57600"));
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			portDetectWait = Integer.parseInt(properties.getProperty("port-detect-wait", "15"));
			
			if(portDetectWait < 10 || portDetectWait > 60)
			{
				portDetectWait = 15;
			}
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			portIdleRecycle = Integer.parseInt(properties.getProperty("port-idle-recycle", "60"));
			
			if(portIdleRecycle < 30)
			{
				portIdleRecycle = 30;
			}
		}
		catch(Exception e)
		{	
		}
				
		LOGGER.info("Port: " + portName);
		LOGGER.info("Rate: " + rate);
		LOGGER.info("Port Detect Wait: " + portDetectWait);
		LOGGER.info("Port Idle Recycle: " + portIdleRecycle);
		
		int sensorsIn = 0;
		int sensorsOut = 0;
		
		String sSensors = properties.getProperty("sensors", "0");
		
		try
		{
			String[] aSensors = sSensors.split(",");
			String[] aChannels = properties.getProperty("channels", "").split(",");
			String[] aDirections = properties.getProperty("direction", "in").split(",");
			String[] aCalibration = properties.getProperty("calibration", "1").split(",");
			
			for(int i = 0; i < aSensors.length; i++)
			{
				int sensor = Integer.parseInt(aSensors[i]);
				
				Sensor o = new Sensor();
				o.setDirectory(dir);
				o.setPrefix("CC");
				o.sensor = sensor;
				o.id = i;
				
				if(aChannels != null)
				{
					try
					{
						o.setChannels(aChannels[i]);
					}
					catch(Exception e)
					{
					}
				}
				
				if(aDirections != null && aDirections.length > i)
				{
					if("in".equalsIgnoreCase(aDirections[i]))
					{
						o.direction = ADataLogReader.IMPORT;
						sensorsIn++;
					}
					else if("out".equalsIgnoreCase(aDirections[i]))
					{
						o.direction = ADataLogReader.EXPORT;
						sensorsOut++;
					}
				}
				
				if(aCalibration != null && aCalibration.length > i)
				{
					try
					{
						o.calibration = Double.parseDouble(aCalibration[i]);
					}
					catch(Exception e)
					{
						
					}
				}
				
				sensors.put(o.id, o);
				
				o.init(sensorsOut, sensorsIn, energyExport, energyImport);
				
				LOGGER.info(o.toString());
			}
			
			v7Sensor = Util.getNumber(properties.getProperty("v7.sensor"), -1);
			
			if(v7Sensor > -1)
			{
				StringBuffer s = new StringBuffer();
				
				s.append("v7 sensor ").append(v7Sensor);
				
				String c = properties.getProperty("v7.channels");
				
				if(c != null)
				{
					v7Channels = Channel.parse(c);
					
					s.append(", channels ");
					
					for(Channel chan : v7Channels.values())
					{
						s.append(chan.positive?"+":"-").append(chan.channel);
					}
				}
				
				LOGGER.info(s);
			}
			
			v8Sensor = Util.getNumber(properties.getProperty("v8.sensor"), -1);
			
			if(v8Sensor > -1)
			{
				StringBuffer s = new StringBuffer();
				
				s.append("v8 sensor ").append(v8Sensor);
				
				String c = properties.getProperty("v8.channels");
				
				if(c != null)
				{
					v8Channels = Channel.parse(c);
					
					s.append(", channels ");
					
					for(Channel chan : v8Channels.values())
					{
						s.append(chan.positive?"+":"-").append(chan.channel);
					}
				}
				
				LOGGER.info(s);
			}
			
			v9Sensor = Util.getNumber(properties.getProperty("v9.sensor"), -1);
			
			if(v9Sensor > -1)
			{
				StringBuffer s = new StringBuffer();
				
				s.append("v9 sensor ").append(v9Sensor);
				
				String c = properties.getProperty("v9.channels");
				
				if(c != null)
				{
					v9Channels = Channel.parse(c);
					
					s.append(", channels ");
					
					for(Channel chan : v9Channels.values())
					{
						s.append(chan.positive?"+":"-").append(chan.channel);
					}
				}
				
				LOGGER.info(s);
			}
			
			v10Sensor = Util.getNumber(properties.getProperty("v10.sensor"), -1);
			
			if(v10Sensor > -1)
			{
				StringBuffer s = new StringBuffer();
				
				s.append("v10 sensor ").append(v10Sensor);
				
				String c = properties.getProperty("v10.channels");
				
				if(c != null)
				{
					v10Channels = Channel.parse(c);
					
					s.append(", channels ");
					
					for(Channel chan : v10Channels.values())
					{
						s.append(chan.positive?"+":"-").append(chan.channel);
					}
				}
				
				LOGGER.info(s);
			}
			
			v11Sensor = Util.getNumber(properties.getProperty("v11.sensor"), -1);
			
			if(v11Sensor > -1)
			{
				StringBuffer s = new StringBuffer();
				
				s.append("v11 sensor ").append(v11Sensor);
				
				String c = properties.getProperty("v11.channels");
				
				if(c != null)
				{
					v11Channels = Channel.parse(c);
					
					s.append(", channels ");
					
					for(Channel chan : v11Channels.values())
					{
						s.append(chan.positive?"+":"-").append(chan.channel);
					}
				}
				
				LOGGER.info(s);
			}
			
			v12Sensor = Util.getNumber(properties.getProperty("v12.sensor"), -1);
			
			if(v12Sensor > -1)
			{
				StringBuffer s = new StringBuffer();
				
				s.append("v12 sensor ").append(v12Sensor);
				
				String c = properties.getProperty("v12.channels");
				
				if(c != null)
				{
					v12Channels = Channel.parse(c);
					
					s.append(", channels ");
					
					for(Channel chan : v12Channels.values())
					{
						s.append(chan.positive?"+":"-").append(chan.channel);
					}
				}
				
				LOGGER.info(s);
			}
		}
		catch(Exception e)
		{
			throw new Exception("Could not load configuration in currentcost.ini'", e);
		}
		
		if(portName.length() == 0 || "auto".equalsIgnoreCase(portName))
		{
			Set<CommPortIdentifier> ports = getAvailableSerialPorts();
			
			for(CommPortIdentifier port : ports)
			{
				if(!port.isCurrentlyOwned())
				{
					SerialPortDataPeek peek = new SerialPortDataPeek(portDetectWait);
					peek.peek(port, rate);
					
					if(peek.isMatch())
					{
						LOGGER.info("Found expected data on port: " + port.getName());
						
						portIdentifier = port;
						
						break;
					}
				}
			}
			
			if(portIdentifier == null)
			{
				for(CommPortIdentifier port : ports)
				{
					if(!port.isCurrentlyOwned())
					{
						portIdentifier = port;
					
						LOGGER.warn("No valid data found on any port, using " + portIdentifier.getName());
						
						break;
					}
				}
			}
		}
		else
		{
			try 
			{
				portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
			} 
			catch (NoSuchPortException e) 
			{
				throw new Exception("The port " + portName + " does not exist");
			}
		}

		if(portIdentifier == null)
		{
			throw new Exception("No free serial ports detected");
		}
		else if(portIdentifier.isCurrentlyOwned())
		{
			throw new Exception("The port " + portIdentifier.getName() + " is currently in use");
		}
		else
		{
			open();
		}
		
		CurrentCostFileWriter w = new CurrentCostFileWriter();
		w.start();
		
		CurrentCostHeartBeat h = new CurrentCostHeartBeat();
		h.start();
	}
	
	public void close()
	{
		if(commPort != null)
		{
			LOGGER.info("Closing Serial Port: " + commPort.getName());
			
			commPort.close();
		}
	}
	
	public void open()
	throws Exception
	{
		commPort = portIdentifier.open(this.getClass().getName(), 5000);

		if(commPort instanceof SerialPort)
		{
			LOGGER.info("Opened serial port: " + commPort.getName());
			
			SerialPort serialPort = (SerialPort) commPort;
			serialPort.setSerialPortParams(rate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			InputStream in = serialPort.getInputStream();                
			serialPort.addEventListener(new CurrentCostSerialReader(in));
			serialPort.notifyOnDataAvailable(true);
		}
	}
	
	public static Set<CommPortIdentifier> getAvailableSerialPorts() 
	{
        Set<CommPortIdentifier> h = new HashSet<CommPortIdentifier>();
        Enumeration thePorts = CommPortIdentifier.getPortIdentifiers();
        
        while(thePorts.hasMoreElements()) 
        {
            CommPortIdentifier com = (CommPortIdentifier) thePorts.nextElement();
            
            switch (com.getPortType()) {
            
            case CommPortIdentifier.PORT_SERIAL:
                
            	try 
            	{
                    CommPort thePort = com.open("CommUtil", 5000);
                    thePort.close();
                    h.add(com);
                } 
            	catch(PortInUseException e) 
            	{
                } 
            	catch (Exception e) 
            	{
                }
            }
        }
        
        LOGGER.info("Serial ports available: " + h.size());
        
        return h;
    }
	
	public void shutdown()
	{
		close();
	}

	@Override
	public int getDefaultDirection() 
	{
		return IMPORT;
	}
	
	public synchronized int getLastUpdate()
	{
		if(lastSerialActivity > 0)
		{
			return (int)Math.round((System.currentTimeMillis() - lastSerialActivity)/1000.0);
		}
		
		return 0;
	}
	
	public synchronized void update()
	{
		lastSerialActivity = System.currentTimeMillis();
	}
	
	private class CurrentCostSerialReader implements SerialPortEventListener 
	{
		private InputStream in;
		private byte[] buffer = new byte[1024];
		
		public CurrentCostSerialReader(InputStream in)
		{
			this.in = in;
		}

		public void serialEvent(SerialPortEvent arg0) 
		{
			int data;

			try
			{
				int len = 0;

				while((data = in.read()) > -1)
				{
					if(data == '\n' ) 
					{
						break;
					}

					buffer[len++] = (byte) data;
				}

				String d = new String(buffer,0,len);

				if(d.trim().startsWith("<msg>") && d.trim().endsWith("</msg>"))
				{
					processMessage(d.trim());
				}
			}
			catch ( IOException e )
			{
				e.printStackTrace();
			}             
		}

		private void processMessage(String msg)
		{
			Date time = new Date();
			
			update();
			
			if(LOGGER.isDebugEnabled())
			{				
				LOGGER.debug("Received: " + msg);
			}
			
			// real time message and correct sensor
			if(msg.indexOf("<hist>") == -1)
			{
				String sSensor = Util.getElement(msg, "sensor");
				
				int sensor = -1;
				
				try
				{
					sensor = Integer.parseInt(sSensor);
				}
				catch(Exception e)
				{
				}
				
				if(sensor == -1)
				{
					if(msg.indexOf("<watts>") > -1)
					{
						// assume classic format
						sensor = 0;
					}
					else
					{
						return;
					}
				}
				
				for(Sensor sensorObj: sensors.values())
				{
					Output output = null;
					
					// handle extended parameters on the first sensor
					if(sensorObj.id == 0)
					{
						int v7 = -1;
						int v8 = -1;
						int v9 = -1;
						int v10 = -1;
						int v11 = -1;
						int v12 = -1;
						
						if(v7Sensor == sensor)
							v7 = processExtendedValue(msg, v7Channels);

						if(v8Sensor == sensor)
							v8 = processExtendedValue(msg, v8Channels);
						
						if(v9Sensor == sensor)
							v9 = processExtendedValue(msg, v9Channels);
						
						if(v10Sensor == sensor)
							v10 = processExtendedValue(msg, v10Channels);
						
						if(v11Sensor == sensor)
							v11 = processExtendedValue(msg, v11Channels);
					
						if(v12Sensor == sensor)
							v12 = processExtendedValue(msg, v12Channels);
						
					
						if(v7 > -1 || v8 > -1 || v9 > -1 || v10 > -1 || v11 > -1 || v12 > -1)
						{
							output = new Output();
							output.time = new Date(time.getTime());
							
							if(v7 > -1)
								output.v7 = v7;
							if(v8 > -1)
								output.v8 = v8;
							if(v9 > -1)
								output.v9 = v9;
							if(v10 > -1)
								output.v10 = v10;
							if(v11 > -1)
								output.v11 = v11;
							if(v12 > -1)
								output.v12 = v12;
						}
					}
							
					if(sensor == sensorObj.sensor)
					{
						int total = 0;
						double temperature = Constant.DEFAULT_TEMPERATURE;
						
						// specific channels
						if(sensorObj.channelSet.size() > 0)
						{
							boolean found = true;
							
							for(int c : sensorObj.channelSet)
							{
								String channel = Util.getElement(msg, "ch"+ (c));
								
								if(channel != null)
								{
									String watts = Util.getElement(channel, "watts");
									
									try
									{
										int iwatt = Integer.parseInt(watts);
										total += iwatt;
										
									}
									catch(Exception e)
									{
										
									}
								}
								else
								{
									found = false;
								}
							}
							
							// all channel data must be present
							if(!found)
							{
								continue;
							}
						}
						else
						{
							// all channels
							for(int c = 1; c <= 10; c++)
							{
								String channel = Util.getElement(msg, "ch"+ (c));
								
								if(channel != null)
								{
									String watts = Util.getElement(channel, "watts");
									
									try
									{
										int iwatt = Integer.parseInt(watts);
										total += iwatt;
									}
									catch(Exception e)
									{
										
									}
								}
							}
						}
						
						// ignore temperature if not 'C' or 'F'
						if("F".equalsIgnoreCase(temperatureUnit) || "C".equalsIgnoreCase(temperatureUnit))
						{
							temperature = Util.getDouble(Util.getElement(msg, "tmpr"), Constant.DEFAULT_TEMPERATURE);
							
							if(temperature == Constant.DEFAULT_TEMPERATURE)
							{
								temperature = Util.getDouble(Util.getElement(msg, "tmprF"), Constant.DEFAULT_TEMPERATURE);
							}
							
							if(temperature != Constant.DEFAULT_TEMPERATURE && "F".equalsIgnoreCase(temperatureUnit))
							{
								temperature = Util.getCelsius(temperature);
							}
						}
						
						total *= sensorObj.calibration;
						
						// apply the rule
						if(rule != null && interpreter != null)
						{
							Calendar c = Calendar.getInstance();
							
							try
							{
								interpreter.set("power", total);
								interpreter.set("hour", c.get(Calendar.HOUR_OF_DAY));
								interpreter.set("minute", c.get(Calendar.MINUTE));
								interpreter.set("in", sensorObj.direction == ADataLogReader.IMPORT ? true : false);
								interpreter.set("out", sensorObj.direction == ADataLogReader.EXPORT ? true : false);
								
								interpreter.eval(rule);
								
								Object o = interpreter.get("power");
								
								if(o instanceof Integer)
								{
									total = (Integer)o;
								}
								else if(o instanceof Double)
								{
									total = (int)Math.round(((Double)o).doubleValue());
								}
							}
							catch(Exception e)
							{
								LOGGER.error("Rule Error [" + rule + "]");
							}
						}
						
						if(output == null)
						{
							output = new Output();
							output.time = new Date(time.getTime());
						}
						
						output.power = total;
						output.temperature = temperature;
					}
					
					// save output
					if(output != null)
					{
						synchronized(sensorObj.readings)
						{						
							if(sensorObj.lastTime != null)
							{
								Calendar lastCal = Calendar.getInstance();
								lastCal.setTime(sensorObj.lastTime);
								
								Calendar currentCal = Calendar.getInstance();
								currentCal.setTime(time);
								
								if(lastCal.get(Calendar.DAY_OF_YEAR) != currentCal.get(Calendar.DAY_OF_YEAR))
								{
									// new day
									sensorObj.readings.clear();
								}
							}

							sensorObj.add(output);

							if(LOGGER.isDebugEnabled())
							{
								LOGGER.debug("Add[" + output.toString() +"]");
							}
						}

						sensorObj.lastTime = time;
					}
				}
			}
		}
	}

	private class CurrentCostHeartBeat extends Thread
	{	
		@Override
		public void run()
		{ 
			while(true)
			{
				int idle = getLastUpdate();
				
				if(idle > portIdleRecycle)
				{
					LOGGER.warn("Port " + commPort.getName() + " idle for more than " + portIdleRecycle + " seconds");
				
					close();
					
					update();
					
					try
					{
						open();
					}
					catch(Exception e)
					{
						LOGGER.error("Could not read data", e);
					}
				}
				
				try
				{
					Thread.sleep(portIdleRecycle * 1000);
				}
				catch(Exception e)
				{
					
				}
			}

		}
	}
	
	private class CurrentCostFileWriter extends Thread
	{				
		@Override
		public void run()
		{ 
			try
			{
				Thread.sleep(6000);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			while(true)
			{
				try
				{
					for(Sensor sensor : sensors.values())
					{
						sensor.processReadings();
					}
				}
				catch(Exception e)
				{
					LOGGER.error("Could not read data", e);
				}

				try
				{
					Thread.sleep(60000);
				}
				catch(Exception e)
				{

				}
			}
		}
	}
	
	public void setStatus(Date statusDateTime, int energyImport, int energyExport)
	{
		this.statusDateTime = statusDateTime;
		this.energyImport = energyImport;
		this.energyExport = energyExport;
	}

	public int processExtendedValue(String msg, Map<String, Channel> ochan) 
	{
		int v = 0;
		
		if(ochan == null || ochan.size() == 0)
		{
			for(int c = 1; c <= 10; c++)
			{
				String channel = Util.getElement(msg, "ch"+ (c));
				
				if(channel != null)
				{
					String watts = Util.getElement(channel, "watts");
					
					int iwatt = Util.getInt(watts, -1);
					
					if(iwatt > -1)
					{
						v += iwatt;
					}
				}
			}
		}
		else
		{
			for(Channel chan : ochan.values())
			{
				String channel = Util.getElement(msg, "ch"+ (chan.channel));
				
				if(channel != null)
				{
					String watts = Util.getElement(channel, "watts");
					
					int iwatt = Util.getInt(watts, -1);
					
					if(iwatt > -1)
					{
						if(chan.positive)
						{
							v += iwatt;
						}
						else
						{
							v -= iwatt;
						}
					}
				}
				else
				{					
					return -1;
				}
				
				
			}
			
			if(v < 0)
				v = 0;
		}
		
		return v;
	}

	public void setSensorId(int i) 
	{
		sensorId = i;
	}
	


	
}
