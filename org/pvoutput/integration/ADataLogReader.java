/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.pvoutput.integration.util.Constant;
import org.pvoutput.integration.util.WebClient;


public abstract class ADataLogReader
{
	private static Logger LOGGER = Logger.getLogger(ADataLogReader.class);

	public static int EXPORT = 0;
	public static int IMPORT = 1;

	protected static File logfile;
	protected String graphSubtitle = "";
	protected SimpleDateFormat logFileTimeFormat;
	protected SimpleDateFormat logFileDateFormat;
	protected Calendar serviceEndTime = null;
	protected Date date;
	protected String filePattern;
	protected File dir;
	protected String format;
	protected Map<Date,Output> data = new TreeMap<Date,Output>();
	protected Map<Date,Output> outputs = new TreeMap<Date,Output>();
	protected Properties properties = new Properties();
	protected int direction = EXPORT;
	protected int systemSize = -1;
	protected int interval = 10;
	
	protected String labelDate;
	protected String labelTime;
	protected String labelEnergy;
	protected String labelPower;
	protected String labelTemperature;
	protected String labelVoltage;
	
	protected String labelV7;
	protected String labelV8;
	protected String labelV9;
	protected String labelV10;
	protected String labelV11;
	protected String labelV12;
	
	protected boolean checkNonZeroEnergyStart;
	protected boolean checkDecreasingEnergy;
	
	protected int indexEnergy = -1;
	protected int indexDate = -1;
	protected int indexPower = -1;
	protected int indexTime = -1;	
	protected int indexTemperature = -1;
	protected int indexVoltage = -1;
	
	protected int indexV7;
	protected int indexV8;
	protected int indexV9;
	protected int indexV10;
	protected int indexV11;
	protected int indexV12;
	
	protected Map<String,Double> lastEnergy = new HashMap<String,Double>();
	protected Map<String,Boolean> zeroEnergy = new HashMap<String,Boolean>();
	protected Map<String,Double> baseEnergy = new HashMap<String,Double>();
	protected Map<String,Double> lastLogEnergy = new HashMap<String,Double>();
	protected Map<String,Integer> increments = new HashMap<String,Integer>();
	
	public abstract void setLabels();
	public abstract void get() throws Exception;
	public abstract void read() throws Exception;
	public abstract String getLogDateFormat();
	public abstract String getLogTimeFormat();
	public abstract File getLogFile();
	public abstract String getLogFilename();
	public abstract void startup() throws Exception;
	public abstract void shutdown() throws Exception;
	
	protected void initReader()
	{
		outputs.clear();
		data.clear();
		baseEnergy.clear();
		lastEnergy.clear();
		lastLogEnergy.clear();
		increments.clear();
		zeroEnergy.clear();
		
		indexEnergy = -1;
		indexDate = -1;
		indexPower = -1;
		indexTime = -1;
		
		indexV7 = -1;
		indexV8 = -1;
		indexV9 = -1;
		indexV10 = -1;
		indexV11 = -1;
		indexV12 = -1;
	}
	
	protected void initLabels(int n) 
	{
		if(properties != null)
		{
			String s = properties.getProperty("label.power");

			if(s != null && s.length() > 0)
			{
				labelPower = s;
				
				LOGGER.info("Label Power " + n + ": '" + s + "'");
			}

			s = properties.getProperty("label.energy");

			if(s != null && s.length() > 0)
			{
				labelEnergy = s;
				
				LOGGER.info("Label Energy " + n + ": '" + s + "'");
			}

			s = properties.getProperty("label.date");

			if(s != null && s.length() > 0)
			{
				labelDate = s;
				
				LOGGER.info("Label Date " + n + ": '" + s + "'");
			}

			s = properties.getProperty("label.time");

			if(s != null && s.length() > 0)
			{
				labelTime = s;
				
				LOGGER.info("Label Time " + n + ": '" + s + "'");
			}
			
			s = properties.getProperty("label.temperature");

			if(s != null && s.length() > 0)
			{
				labelTemperature = s;
				
				LOGGER.info("Label Temperature " + n + ": '" + s + "'");
			}
			
			s = properties.getProperty("label.voltage");

			if(s != null && s.length() > 0)
			{
				labelVoltage = s;
				
				LOGGER.info("Label Voltage " + n + ": '" + s + "'");
			}
			
			s = properties.getProperty("label.v7");

			if(s != null && s.length() > 0)
			{
				labelV7 = s;
				
				LOGGER.info("Label v7 " + n + ": '" + s + "'");
			}
			
			s = properties.getProperty("label.v8");

			if(s != null && s.length() > 0)
			{
				labelV8 = s;
				
				LOGGER.info("Label v8 " + n + ": '" + s + "'");
			}
			
			s = properties.getProperty("label.v9");

			if(s != null && s.length() > 0)
			{
				labelV9 = s;
				
				LOGGER.info("Label v9 " + n + ": '" + s + "'");
			}
			
			s = properties.getProperty("label.v10");

			if(s != null && s.length() > 0)
			{
				labelV10 = s;
				
				LOGGER.info("Label v10 " + n + ": '" + s + "'");
			}
			
			s = properties.getProperty("label.v11");

			if(s != null && s.length() > 0)
			{
				labelV11 = s;
				
				LOGGER.info("Label v11 " + n + ": '" + s + "'");
			}
			
			s = properties.getProperty("label.v12");

			if(s != null && s.length() > 0)
			{
				labelV12 = s;
				
				LOGGER.info("Label v12 " + n + ": '" + s + "'");
			}
		}

		setLabels();
	}

	protected boolean validateLabels() throws Exception
	{
		if(indexEnergy == -1)
		{
			LOGGER.error("Could not find '" + labelEnergy + "' label");

			return false;
		}

		if(indexPower == -1)
		{
			LOGGER.error("Could not find '" + labelPower + "' label");

			return false;
		}

		if(indexTime == -1)
		{
			LOGGER.error("Could not find '" + labelTime + "' label");

			return false;
		}

		if(indexDate == -1)
		{
			LOGGER.error("ERROR: Could not find '" + labelDate + "' label");

			return false;
		}

		return true;
	}

	public Date getDate()
	{
		return date;
	}

	public String getSubtitle()
	{
		return graphSubtitle;
	}

	protected Map<Date,Output> getOutputs()
	{
		return outputs;
	}

	protected Map<Date,Output> getData()
	{
		return data;
	}

	protected File getDirectory()
	{
		return dir;
	}

	protected void setDirectory(File dir)
	{
		this.dir = dir;
	}

	protected String getFormat()
	{
		return format;
	}

	protected void setServiceEndTime(Calendar c)
	{
		serviceEndTime = c;
	}

	protected void setFilePattern(String s)
	{
		this.filePattern = s;
	}

	protected void setFormat(String format)
	{
		this.format = format;
	}

	public String getLogFilename(String pattern) 
	{
		if(filePattern != null && filePattern.length() > 0)
		{
			pattern = filePattern.replaceAll(".*\\{(.*)}.*", "$1");

			if(pattern != null && pattern.length() > 0)
			{
				SimpleDateFormat df = new SimpleDateFormat(pattern);

				String file = filePattern.replaceAll("\\{" + pattern + "\\}", df.format(new java.util.Date()));

				return file;
			}
			else
			{
				return filePattern;
			}
		}	

		return null;
	}

	protected File getLogFile(String pattern)
	{
		if(filePattern != null && filePattern.length() > 0)
		{
			pattern = filePattern.replaceAll(".*\\{(.*)}.*", "$1");

			if(pattern != null && pattern.length() > 0)
			{
				String file = filePattern;

				SimpleDateFormat df = null;

				try
				{
					df = new SimpleDateFormat(pattern);

					file = filePattern.replaceAll("\\{" + pattern + "\\}", df.format(new java.util.Date()));
				}
				catch(Exception e)
				{	
				}

				// check if its a remote file
				if(file.startsWith("http://"))
				{
					String url = file.substring(7);

					String query = null;
					int port = 80;
					String host = null;

					int portIndex = url.indexOf(':');
					int queryIndex = url.indexOf('/');

					if(portIndex > -1)
					{
						host = url.substring(0, portIndex);

						if(queryIndex > -1)
						{
							port = Util.getNumber(url.substring(portIndex+1, queryIndex));
						}
						else
						{
							port = Util.getNumber(url.substring(portIndex+1));
						}

						if(queryIndex > -1)
						{
							query = url.substring(queryIndex);
						}
					}
					else
					{
						if(queryIndex > -1)
						{
							host = url.substring(0, queryIndex);

							query = url.substring(queryIndex);
						}
						else
						{
							host = url;
						}
					}

					int offset = query.lastIndexOf('/');

					File f = null;

					if(offset > -1)
					{
						f = new File(dir, query.substring(offset));
					}
					else
					{
						f = new File(dir, query);
					}

					getHttp(host, port, query, f);
					
					return f;
				}
				else if(file.startsWith("ftp://"))
				{
					String url = file.substring(6);

					String remote = null;
					int port = 21;
					String host = null;
					String username = "anonymous";
					String password = "";
							
					int loginIndex = url.indexOf('@');
					
					if(loginIndex > 0)
					{
						String[] userPassPart = url.substring(0, loginIndex).split(":");
						
						if(userPassPart != null)
						{
							if(userPassPart.length == 2)
							{
								username = userPassPart[0];
								password = userPassPart[1];
							}
							else if(userPassPart.length == 1)
							{
								username = userPassPart[0];
							}
						}
						
						url = url.substring(loginIndex+1);
					}
					
					int portIndex = url.indexOf(':');
					int queryIndex = url.indexOf('/');

					if(portIndex > -1)
					{
						host = url.substring(0, portIndex);

						if(queryIndex > -1)
						{
							port = Util.getNumber(url.substring(portIndex+1, queryIndex));
						}
						else
						{
							port = Util.getNumber(url.substring(portIndex+1));
						}

						if(queryIndex > -1)
						{
							remote = url.substring(queryIndex);
						}
					}
					else
					{
						if(queryIndex > -1)
						{
							host = url.substring(0, queryIndex);

							remote = url.substring(queryIndex);
						}
						else
						{
							host = url;
						}
					}

					int offset = remote.lastIndexOf('/');

					File f = null;

					if(offset > -1)
					{
						f = new File(dir, remote.substring(offset));
					}
					else
					{
						f = new File(dir, remote);
					}

					LOGGER.info(remote + " to " + f.getAbsolutePath());
					
					getFtp(host, port, username, password, remote, f);
					
					return f;
				}

				return new File(dir, file);
			}
			else
			{
				return new File(dir, filePattern);
			}
		}	

		return null;
	}
	

	/**
	 * Get log file from an external ftp source
	 * @param hostname
	 * @param port
	 * @param dir
	 * @param filename
	 */
	private boolean getFtp(String hostname, int port, String username, String password, String remote, File local)
	{
		final FTPClient ftp = new FTPClient();
		
		boolean success = false;
		
		try
		{
			int reply;
			
			ftp.connect(hostname, port);
			
			reply = ftp.getReplyCode();

            if(!FTPReply.isPositiveCompletion(reply))
            {
                ftp.disconnect();
                
                LOGGER.error("FTP server refused connection.");
                
                return success;
            }
		}
		catch(Exception e)
		{
			LOGGER.error("Error communicating with FTP server", e);
		
			if(ftp.isConnected())
            {
                try
                {
                    ftp.disconnect();
                }
                catch (IOException f)
                {
                }
            }
			
			return success;
		}
		
		// connected and continue
		try
        {
            if (!ftp.login(username, password))
            {
            	LOGGER.error("Failed to login as '" + username + "'");
            	
                ftp.logout();
                
                return success;
            }
            else
            {
            	LOGGER.info("Logged in as " + username);
            }

            LOGGER.info("Remote system is " + ftp.getSystemType());

            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            ftp.enterLocalPassiveMode();
            
            // download file
            OutputStream output = null;

            try
            {
                output = new FileOutputStream(local);

                if(ftp.retrieveFile(remote, output))
                {
                	LOGGER.error("Downloaded " + remote + " to " + local);
                	
                	success = true;
                }
                else
                {
                	LOGGER.error("Failed to Download " + remote + " to " + local);
                }
            }
            catch(Exception e)
            {
            	LOGGER.error("Could not download file", e);
            }
            finally
            {
            	if(output != null)
            	{
            		try
            		{
            			output.close();
            		}
            		catch(Exception e)
            		{
            			
            		}
            	}
            }

            ftp.noop();

            ftp.logout();
        }
        catch (FTPConnectionClosedException e)
        {
            LOGGER.error("Server closed connection", e);
        }
        catch (IOException e)
        {
        	LOGGER.error("FTP error", e);
        }
        finally
        {
            if (ftp.isConnected())
            {
                try
                {
                    ftp.disconnect();
                }
                catch (IOException f)
                {
                    // do nothing
                }
            }
        }
		
		return success;
	}
	
	
	/**
	 * Get log file from an external http source
	 * @param hostname
	 * @param port
	 * @param query
	 * @param f
	 */
	private void getHttp(String hostname, int port, String query, File f) 
	{		
		org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
		HttpGet httpget = WebClient.getHttpGet(hostname, port, query, false);
		
		try
		{
			HttpContext context = new BasicHttpContext();
			HttpResponse response1 = httpclient.execute(httpget, context);
			int rc = response1.getStatusLine().getStatusCode();
			
			HttpEntity entity = response1.getEntity();
			String s = null;
			
			if(entity != null)
				s = EntityUtils.toString(entity);

			if(rc == 200)
			{
				// remove existing file
				if(f.exists())
				{
					f.delete();
				}

				FileOutputStream out = null;

				try
				{
					out = new FileOutputStream(f);
					writeFile(out, new ByteArrayInputStream(s.getBytes()));
				}
				catch(Exception e)
				{
					LOGGER.error("Could not write data", e);
				}
				finally
				{
					if(out != null)
					{
						try
						{
							out.close();
						}
						catch(Exception e)
						{

						}
					}
				}
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Processing error", e);
		}
	}

	
	public void writeFile(OutputStream out, InputStream in) 
	throws IOException 
	{
		try 
		{
			int l;
			byte[] buffer = new byte[1024];

			while ((l = in.read(buffer)) != -1) 
			{
				out.write(buffer, 0, l);
			}
		} 
		finally 
		{
			try
			{
				in.close();
			}
			catch(Exception e)
			{

			}
		}
	}

	public void setLogfile(File f)
	{
		logfile = f;
	}

	protected void add(String[] line, Object sTime, String sEnergy, String sPower, String sTemp, String sVolt, String inverterId)
	{
		add(line, sTime, sEnergy, sPower, sTemp, sVolt, inverterId, '.');
	}
	
	protected void add(String[] line
			, Object sTime
			, String sEnergy
			, String sPower
			, String sTemp
			, String sVolt
			, String inverterId
			, char decimal)
	{
		Date time = null;
		int power = 0;
		int energy = 0;

		if(LOGGER.isDebugEnabled())
		{
			LOGGER.debug("Adding " + sTime + ", pout=" + sPower + ", eout=" + sEnergy);
		}
		
		try
		{
			if(sTime instanceof Date)
			{
				time = (Date)sTime;

				Calendar c = Calendar.getInstance();
				c.setTime(time);
			}
			else if(sTime instanceof String)
			{
				// time will be set to today's date
				time = logFileTimeFormat.parse((String)sTime);
				Calendar today = Calendar.getInstance();
				int year = today.get(Calendar.YEAR);
				int day = today.get(Calendar.DAY_OF_YEAR);

				Calendar c = Calendar.getInstance();
				c.setTime(time);
				c.set(Calendar.YEAR, year);
				c.set(Calendar.DAY_OF_YEAR, day);

				time = c.getTime();
			}
			else
			{
				return;
			}

			if(time.getTime() > System.currentTimeMillis())
			{
				LOGGER.warn("Future time: " + time);

				return;
			}

			power = (int)(Math.round(Double.parseDouble(sPower)));
			energy = (int)(Math.round(Double.parseDouble(sEnergy)));
		}
		catch(Exception e)
		{
			return;
		}

		Date roundedInterval = Util.getRoundedTime(time, interval);
		
		double last = getLastLogEnergy(inverterId);
		
		setLastLogEnergy(inverterId, energy);
		
		if(energy < last && last > -1)
		{
			if(getIncrement(inverterId) == 0)
			{
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.warn("Cleared entries before " + time + " (" + data.size() + ")");
				}
				
				data.clear();
				outputs.clear();
				lastEnergy.clear();
				baseEnergy.clear();
			}
		}
		else if(energy > last && last > -1)
		{
			increment(inverterId);
		}
		
		if(systemSize > 0 && getDirection() == EXPORT)
		{
			Calendar c2 = Calendar.getInstance();
			c2.setTime(roundedInterval);
			int hour = c2.get(Calendar.HOUR_OF_DAY);
			int minute = c2.get(Calendar.MINUTE);

			int t = hour * 60 + minute;

			double gss = energy/(systemSize+0.0);

			if(!isValidEnergyEfficiency(gss, t))
			{
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.warn("Energy [" + energy + "] too high at [" + time + "] eff[" + gss + "] size[" + systemSize + "]");
				}
				
				return;
			}
		}

		if(energy == 0)
		{
			if(lastEnergy.containsKey(inverterId) && lastEnergy.get(inverterId) > 0)
			{
				baseEnergy.put(inverterId, lastEnergy.get(inverterId));
			}
		}
		
		if(power == 0 && energy == 0)
		{
			// mark zero energy
			zeroEnergy.put(inverterId, true);
			
			return;
		}

		if(energy > 0)
		{			
			setLastEnergy(inverterId, energy);
			
			if(checkNonZeroEnergyStart)
			{
				// handle previous days energy
				if(!zeroEnergy.containsKey(inverterId) && getIncrement(inverterId) == 0)
				{
					LOGGER.warn("Log started with non-zero energy [" + energy + "] at " + time);
					
					return;
				}
			}
		}
		
		double _baseEnergy = baseEnergy.containsKey(inverterId) ? baseEnergy.get(inverterId) : 0;
		
		if(_baseEnergy > 0)
		{
			if(energy == 0)
			{
				energy += _baseEnergy;
			}
			else if(energy > 0)
			{
				if(energy / (_baseEnergy+0.0) < 0.10)
				{
					// when the daily energy value is reset is should be less than the 10% of the base energy
					energy += _baseEnergy;
				}
				else if(energy < _baseEnergy)
				{
					energy = (int)_baseEnergy;
				}
			}
		}

		Date roundedTime = Util.getRoundedTime(time, 1);

		boolean newOutput = true;

		if(data.containsKey(roundedTime))
		{
			Output existingOutput = data.get(roundedTime);

			if(!existingOutput.inverters.contains(inverterId))
			{
				existingOutput.energy += energy;
				existingOutput.power += power;
				existingOutput.inverters.add(inverterId);

				newOutput = false;
			}
		}

		double temperature = Util.getDouble(sTemp, Constant.DEFAULT_TEMPERATURE);
		double voltage = Util.getDouble(sVolt, Constant.DEFAULT_VOLTAGE);
				
		if(newOutput)
		{
			Output output = new Output();
			output.energy = energy;
			output.power = power;			
			output.time = roundedTime;
			output.inverters.add(inverterId);

			if(Util.validTemperature(temperature))
			{
				output.temperature = temperature;
			}
			
			if(Util.validVoltage(voltage))
			{
				output.voltage = voltage;
			}
			
			if(indexV7 > -1 && line.length >= indexV7)
			{
				output.v7 = Util.getDouble(decimal != '.' ? line[indexV7].replace(decimal, '.') : line[indexV7], Double.NaN);
			}
			
			if(indexV8 > -1 && line.length >= indexV8)
			{
				output.v8 = Util.getDouble(decimal != '.' ? line[indexV8].replace(decimal, '.') : line[indexV8], Double.NaN);
			}
			
			if(indexV9 > -1 && line.length >= indexV9)
			{
				output.v9 = Util.getDouble(decimal != '.' ? line[indexV9].replace(decimal, '.') : line[indexV9], Double.NaN);
			}
			
			if(indexV10 > -1 && line.length >= indexV10)
			{
				output.v10 = Util.getDouble(decimal != '.' ? line[indexV10].replace(decimal, '.') : line[indexV10], Double.NaN);
			}
			
			if(indexV11 > -1 && line.length >= indexV11)
			{
				output.v11 = Util.getDouble(decimal != '.' ? line[indexV11].replace(decimal, '.') : line[indexV11], Double.NaN);
			}
			
			if(indexV12 > -1 && line.length >= indexV12)
			{
				output.v12 = Util.getDouble(decimal != '.' ? line[indexV12].replace(decimal, '.') : line[indexV12], Double.NaN);
			}
									
			data.put(roundedTime, output);
		}

		newOutput = true;

		if(outputs.containsKey(roundedInterval))
		{
			Output existingOutput = outputs.get(roundedInterval);

			if(!existingOutput.inverters.contains(inverterId))
			{
				existingOutput.energy += energy;
				existingOutput.power += power;
				existingOutput.inverters.add(inverterId);

				newOutput = false;
			}
		}

		if(newOutput)
		{
			Output output = new Output();
			output.energy = energy;
			output.power = power;
			output.time = roundedInterval;
			output.inverters.add(inverterId);

			if(Util.validTemperature(temperature))
			{
				output.temperature = temperature;
			}
			
			if(Util.validVoltage(voltage))
			{
				output.voltage = voltage;
			}
			
			if(indexV7 > -1 && line.length >= indexV7)
			{
				output.v7 = Util.getDouble(decimal != '.' ? line[indexV7].replace(decimal, '.') : line[indexV7], Double.NaN);
			}
			
			if(indexV8 > -1 && line.length >= indexV8)
			{
				output.v8 = Util.getDouble(decimal != '.' ? line[indexV8].replace(decimal, '.') : line[indexV8], Double.NaN);
			}
			
			if(indexV9 > -1 && line.length >= indexV9)
			{
				output.v9 = Util.getDouble(decimal != '.' ? line[indexV9].replace(decimal, '.') : line[indexV9], Double.NaN);
			}
			
			if(indexV10 > -1 && line.length >= indexV10)
			{
				output.v10 = Util.getDouble(decimal != '.' ? line[indexV10].replace(decimal, '.') : line[indexV10], Double.NaN);
			}
			
			if(indexV11 > -1 && line.length >= indexV11)
			{
				output.v11 = Util.getDouble(decimal != '.' ? line[indexV11].replace(decimal, '.') : line[indexV11], Double.NaN);
			}
			
			if(indexV12 > -1 && line.length >= indexV12)
			{
				output.v12 = Util.getDouble(decimal != '.' ? line[indexV12].replace(decimal, '.') : line[indexV12], Double.NaN);
			}
			
			outputs.put(roundedInterval, output);
		}
	}

	protected boolean isValidEnergyEfficiency(double gss, int t) 
	{
		// 6:00 to 10:00am
		if(t <= 240 && gss >= 0.25 || t <= 300 && gss >= 0.5)
		{
			return false;
		}
		else if(t <= 360 && gss >= 1.0     //  6:00am
				|| t <= 390 && gss >= 1.25 //  6:30am
				|| t <= 420 && gss >= 1.5  //  7:00am
				|| t <= 450 && gss >= 1.75 //  7:30am
				|| t <= 480 && gss >= 2.0  //  8:00am
				|| t <= 510 && gss >= 2.5  //  8:30am
				|| t <= 540 && gss >= 3.0  //  9:00am
				|| t <= 570 && gss >= 3.5  //  9:30am
				|| t <= 600 && gss >= 4.0) // 10:00am
		{
			return false;
		}
		else if(gss > 12)
		{
			return false;
		}
		
		return true;
	}
	
	private int getIncrement(String inverterId)
	{
		if(increments.containsKey(inverterId))
		{
			return increments.get(inverterId);
		}
		
		return 0;
	}
	
	private void increment(String inverterId) 
	{
		int value = 1;
		
		if(increments.containsKey(inverterId))
		{
			value = increments.get(inverterId);
			value++;
		}
		
		increments.put(inverterId, value);		
	}

	private void setLastLogEnergy(String inverterId, double value)
	{
		lastLogEnergy.put(inverterId, value);
	}
	
	private double getLastLogEnergy(String inverterId)
	{
		if(lastLogEnergy.containsKey(inverterId))
		{
			return lastLogEnergy.get(inverterId);
		}
		
		return -1;
	}
	
	private void setLastEnergy(String inverterId, double value)
	{
		lastEnergy.put(inverterId, value);
	}

	protected boolean hasLogStarted(Date dTime, int startHour, int startMinutes)
	{
		try
		{
			Calendar c = Calendar.getInstance();
			c.setTime(dTime);

			int totalStartMinutes = startHour * 60 + startMinutes;

			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minutes = c.get(Calendar.MINUTE);

			int totalMinutes = hour * 60 + minutes;

			if(totalMinutes >= totalStartMinutes)
			{
				return true;
			}
		}
		catch(Exception e)
		{
		}

		return false;	
	}

	public int getDefaultDirection()
	{
		return EXPORT;
	}

	public int getDirection()
	{
		return direction;
	}

	public void setDirection(int i)
	{
		direction = i;
	}

	public void setDirection(String s) 
	{
		if(s == null)
		{
			direction = getDefaultDirection();
		}
		else
		{
			if("import".equalsIgnoreCase(s) || "i".equalsIgnoreCase(s) || "in".equalsIgnoreCase(s))
			{
				direction = IMPORT;
			}
			else
			{
				direction = EXPORT;
			}
		}
	}

	public void setSystemSize(int i) 
	{
		systemSize = i;
	}

	public void setInterval(int i)
	{
		interval = i;
	}

	public String getDelimiter() 
	{
		return ",";
	}
	
	public void setDataFormats() 
	{
		logFileTimeFormat = new SimpleDateFormat(getLogTimeFormat());
		logFileDateFormat = new SimpleDateFormat(getLogDateFormat());	
	}
		
}
