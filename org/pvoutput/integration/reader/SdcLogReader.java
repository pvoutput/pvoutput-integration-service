/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.Util;


public class SdcLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(SdcLogReader.class);

	private SimpleDateFormat dataDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	private SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyMMdd");
	private String timestampLabel = "TimeStamp";
	private String powerLabel = "Pac";
	private String energyLabel = "E-Total";
	private String temperatureLabel = "Tkk";
	private String voltageLabel = "Uac";
	private String delimiter = ";";
	private double startEnergy = 0;
	
	public void startup()
	throws Exception
	{
		super.startup();
				
		// allow logs to start with non-zero energy data
		checkNonZeroEnergyStart = false;
		checkDecreasingEnergy = true;

		if(properties.containsKey("data-time-format"))
		{
			String s = (String)properties.get("data-time-format");
			
			dataDateTimeFormat = new SimpleDateFormat(s);
		}
		
		if(properties.containsKey("file-date-format"))
		{
			String s = (String)properties.get("file-date-format");
			
			fileDateFormat = new SimpleDateFormat(s);
		}
		
		if(properties.containsKey("column.timestamp"))
		{
			timestampLabel = (String)properties.get("column.timestamp");
		}
		
		if(properties.containsKey("column.energy"))
		{
			energyLabel = (String)properties.get("column.energy");
		}
		
		if(properties.containsKey("column.power"))
		{
			powerLabel = (String)properties.get("column.power");
		}
		
		if(properties.containsKey("column.temperature"))
		{
			temperatureLabel = (String)properties.get("column.temperature");
		}
		
		if(properties.containsKey("column.voltage"))
		{
			voltageLabel = (String)properties.get("column.voltage");
		}
		
		LOGGER.info("File Date Format: " + fileDateFormat.toPattern());
		LOGGER.info("Timestamp Format: " + dataDateTimeFormat.toPattern());
	}
	
	@Override
	public void get()
	{
		int added = 0;
		
		Calendar c = Calendar.getInstance();
		
		int currentDay = c.get(Calendar.DAY_OF_MONTH);
		int currentYear = c.get(Calendar.YEAR);
		int currentMonth = c.get(Calendar.MONTH);
		
		// scan directory
		String filePrefix = fileDateFormat.format(c.getTime());
		String fileSuffix = ".suo";
		
		File[] files = dir.listFiles();
		
		Arrays.sort(files, new Comparator<File>()
		{
			// newest first
		    public int compare(File f1, File f2)
		    {
		        return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
		    } 
		});

		File targetFile = null;
		
		for(File f : files)
		{
			if(f.getName().startsWith(filePrefix) && f.getName().endsWith(fileSuffix))
			{
				targetFile = f;
				
				break;
			}
		}
				
		if(targetFile != null)
		{
			Calendar c1 = Calendar.getInstance();
			int removed = 0;
			
			// clear out previous day's entries
			Iterator<Date> i = logEntries.keySet().iterator();
					
			while(i.hasNext())
			{
				Date d = i.next();
				c1.setTime(d);
				
				int day = c1.get(Calendar.DAY_OF_MONTH);
				int year = c1.get(Calendar.YEAR);
				int month = c1.get(Calendar.MONTH);
				
				if(day != currentDay || month != currentMonth || year != currentYear)
				{
					removed++;
					
					i.remove();
				}
			}
			
			if(removed > 0)
			{
				LOGGER.info("Removed " + removed + " old entries");	
			}
			
			if(logEntries.size() == 0)
			{
				startEnergy = 0;
			}
			else
			{
				Iterator<Output> iValues = logEntries.values().iterator();
				
				while(iValues.hasNext())
				{
					Output d = iValues.next();
					
					startEnergy = d.energy_lifetime;
					
					break;
				}
			}
			
			BufferedReader br = null;
			
			try
			{
				br = new BufferedReader(new FileReader(targetFile));
				
				String line = null;
				int columns = 0;
				int timestampIndex = 0;
				int powerIndex = 0;
				int energyIndex = 0;
				int n = 0;
				
				while((line = br.readLine()) != null)
				{
					if(line.indexOf(timestampLabel) > -1
							&& line.indexOf(powerLabel) > -1
							&& line.indexOf(energyLabel) > -1)
					{
						if(!LOGGER.isDebugEnabled())
						{
							LOGGER.debug("Found Header: " + line);
						}
						
						String[] header = line.split(delimiter);
						
						if(header != null)
						{
							for(int col = 0; col < header.length; col++)
							{
								if(timestampLabel.equals(header[col]))
								{
									timestampIndex = col;
								}
								
								if(powerLabel.equals(header[col]))
								{
									powerIndex = col;
								}
								
								if(energyLabel.equals(header[col]))
								{
									energyIndex = col;
								}
								
								if(temperatureLabel.equals(header[col]))
								{
									indexTemperature = col;
								}
								
								if(voltageLabel.equals(header[col]))
								{
									indexVoltage = col;
								}
							}
							
							if(timestampIndex > -1 && powerIndex > -1 && energyIndex > -1)
							{
								columns = header.length;
							}
							else
							{
								LOGGER.warn("Missing labels '" + timestampLabel + "', '" + energyLabel + "' or '" + powerLabel + "'");
							}
						}
					}
					else
					{
						if(columns > 0)
						{
							String[] values = line.split(delimiter);
							
							if(values != null && values.length == columns)
							{
								String timestamp = values[timestampIndex];
								
								Date currentDate = null;
								
								if(timestamp != null && timestamp.length() > 0)
								{	
									try
									{
										currentDate = dataDateTimeFormat.parse(timestamp);
									}
									catch(ParseException e)
									{
										if(n > 0)
										{
											LOGGER.warn("Ignoring invalid line: Time [" + timestamp + "] does not match [" + dataDateTimeFormat.toPattern() + "]");
										}
									}
								}
								
								if(currentDate == null)
								{
									continue;
								}
								
								c.setTime(currentDate);
								c.set(Calendar.SECOND, 0);
								c.set(Calendar.MILLISECOND, 0);
								
								int day = c.get(Calendar.DAY_OF_MONTH);
								int year = c.get(Calendar.YEAR);
								int month = c.get(Calendar.MONTH);
								
								if(day == currentDay && month == currentMonth && year == currentYear)
								{
									String energy = values[energyIndex];
									String power = values[powerIndex];
									String voltage = null;
									String temperature = null;
									
									if(indexTemperature > 0)
									{
										temperature = values[indexTemperature];
									}
									
									if(indexVoltage > 0)
									{
										voltage = values[indexVoltage];
									}
									
									Output o = null;
									
									if(logEntries.containsKey(c.getTime()))
									{
										o = logEntries.get(c.getTime());
									}
									else
									{
										o = new Output();
										o.time = c.getTime();
										o.power = 0;
										o.energy = 0;
										
										if(voltage != null)
										{
											o.voltage = Util.getDouble(voltage, -1);
										}
										
										if(temperature != null)
										{
											o.temperature = Util.getDouble(temperature, -1);
										}
										
										logEntries.put(c.getTime(), o);
									}
									
									try
									{
										double dEnergy = Double.parseDouble(energy)*1000;
										o.power = (int)Math.round(Double.parseDouble(power));
										
										if(startEnergy == 0)
										{
											startEnergy = dEnergy;
										}
										
										o.energy_lifetime = dEnergy;
										o.energy = dEnergy - startEnergy;
									}
									catch(Exception e)
									{
										LOGGER.warn("Could not read energy/power values at " + line);
										
										continue;
									}
									
									added++;
								}
								else
								{
									LOGGER.warn(dataDateTimeFormat.format(c.getTime()) + " does not match year=" + currentYear + ", month=" + currentMonth + ", day=" + currentDay);
								}
							}
							
							n++;
						}
					}
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Could not read file " + targetFile.getAbsolutePath(), e);
			}
			finally
			{
				if(br != null)
				{
					try 
					{
						br.close();
					} 
					catch (IOException e) 
					{
					}
				}
			}	
		}
		else
		{
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.warn("Log file " + filePrefix + "* not found");
			}
		}
		
		if(added > 0)
		{
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.info("Added " + added + " entries");
			}
			
			c = Calendar.getInstance();
			
			addLog(c);
		}
	}

	@Override
	public String getLogFilename()
	{
        StringBuffer s = new StringBuffer();
        
        s.append("SDC").append("-").append(logfileDateFormat.format(new Date())).append(".log");
        
        return s.toString();
	}
	
	@Override
    public File getLogFile()
    {
        return new File(dir, "SDC" + "-" + logfileDateFormat.format(new Date()) + ".log");
    }

}
