/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.TreeMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.pvoutput.integration.console.AdminConsole;
import org.pvoutput.integration.reader.AHttpLogReader;
import org.pvoutput.integration.reader.CurrentcostLogReader;
import org.pvoutput.integration.util.Constant;
import org.pvoutput.integration.util.ExcelReader;
import org.pvoutput.integration.util.WeatherLogger;
import org.pvoutput.integration.util.WebClient;

public class Controller implements Runnable
{
	private static Logger LOGGER = Logger.getLogger(Controller.class);
	
	private String systemName = "None";
	private int systemPostcode = -1;
	private int systemSize = -1;
	private String graphSubtitle = "";
	private String hostname;
	private String query;
	private String apikey;
	private String sid;
	private int retriesSoft = 30;
	private int retriesHard = 40;
	private String format;
	private boolean dryrun = false;
	private boolean uploadTemperature = true;
	private boolean uploadVoltage = true;
	private int port = 80;
	private Calendar serviceEndTime;
	private String[] dirs;
	private SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	private SimpleDateFormat serviceDateFormat = new SimpleDateFormat("yyyyMMdd");
	private SimpleDateFormat dataDirFormat = new SimpleDateFormat("yyyyMM");
	private SimpleDateFormat serviceTimeFormat = new SimpleDateFormat("HH:mm");
	private SimpleDateFormat serviceDateTimeFormat = new SimpleDateFormat("yyyyMMdd HH:mm");
	private SimpleDateFormat localTimeFormat = new SimpleDateFormat("H:mma");
	private SimpleDateFormat localDateFormat = new SimpleDateFormat("dd/MM/yy");
	private Output lastOutputSent = new Output();
	private Map<String,ADataLogReader> readers = new HashMap<String,ADataLogReader>();
	private Map<Date,Output> outputs1min = new TreeMap<Date,Output>();
	private Map<Date,Output> outputsInterval = new TreeMap<Date,Output>(Collections.reverseOrder());
	private Map<Date,Output> outputsQueue = new TreeMap<Date,Output>();
	
	private static final long POLL = 60000;
	private int exportReaders = 0;
	private int importReaders = 0;
	private int energyImport = -1;
	private int energyExport = -1;
	private Date statusDateTime = null;
	private int interval = 10;
	private boolean donator = false;
	private int maxHistoryDays = 14;
	private boolean localGraph = false;
	
	private NumberFormat nf2 = NumberFormat.getInstance();
	private NumberFormat nf = NumberFormat.getInstance();
	
	private ExcelReader excelReader;
	private WeatherLogger weatherLogger;
	private File installDir = null;
	
	public Controller()
	{
		PropertyConfigurator.configure("../conf/log.properties");

		LOGGER.info("");
		LOGGER.info("*** Starting PVOutput Integration Service v" + Main.VERSION);	
	}
	
	public void startup()
	throws StartupException, Exception
	{
		nf2.setMaximumFractionDigits(3);
		nf2.setMinimumFractionDigits(3);
		
		nf.setMaximumFractionDigits(3);
		nf.setMinimumFractionDigits(3);
		nf.setGroupingUsed(false);

		String direction = null;
		String dir = null;
		String file = null;
		String url = null;

		String installDirName = Controller.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		
		installDir = new File(installDirName.replaceAll("%20", " ")).getParentFile().getParentFile();
		
		LOGGER.info("Install Directory: " + installDir.getAbsolutePath());
		
		// load pvoutput.ini
		File fconfig = new File("../conf/pvoutput.ini");

		Properties config = new Properties();
		
		if(fconfig.exists())
		{
			LOGGER.info("Loaded Config: " + fconfig.getName());
			
			FileInputStream in = null;
			
			try
			{
				in = new FileInputStream(fconfig);
				config.load(in);
				
				format = config.getProperty("format");
				direction = config.getProperty("direction");
				dir = config.getProperty("dir");
				file = config.getProperty("file");
				url = config.getProperty("service-url");
				dryrun = "true".equalsIgnoreCase(config.getProperty("service-dryrun"));
				apikey = config.getProperty("key");
				sid = config.getProperty("sid");
				retriesSoft = Util.getNumber(config.getProperty("max-retries-soft"), 30);
				retriesHard = Util.getNumber(config.getProperty("max-retries-hard"), 40);
				uploadTemperature = Util.getBoolean(config.getProperty("upload-temperature", "true"));
				uploadVoltage = Util.getBoolean(config.getProperty("upload-voltage", "true"));
				localGraph = Util.getBoolean(config.getProperty("local-graph", "false"));
				
				if(retriesSoft < 0 || retriesSoft > 1440)
				{
					retriesSoft = 30;
				}
				
				if(retriesHard < 0 || retriesHard > 1440)
				{
					retriesHard = 40;
				}
				
				startAdminConsole(config);
				
				parseURL(url);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				
				throw new StartupException("Could not read properties file: " + fconfig.getAbsolutePath());
			}
			finally
			{
				Util.close(in);
			}
		}
		else
		{
			throw new StartupException("Missing configuration file: " + fconfig.getAbsolutePath());
		}
		
		String[] formats = null;
		String[] directions = null;
		String[] files = null;

		if(file != null)
		{
			files = file.split(",");
		}
		
		if(direction != null)
		{
			directions = direction.split(",");
		}
		
		if(format != null && format.length() > 0)
		{
			formats = format.split(",");
		}
		else
		{
			throw new StartupException("Missing 'format' config in pvoutput.ini");
		}

		if(dir != null && dir.length() > 0)
		{
			dirs = dir.split(",");
		}
		else
		{
			throw new StartupException("Missing 'dir' config in pvoutput.ini");
		}
		
		if(formats == null || dirs == null)
		{
			throw new StartupException("Missing 'dir', 'format' config in pvoutput.ini");
		}
		else if(formats.length != dirs.length)
		{
			throw new StartupException("Number of directories must match number of formats");
		}
		else if(directions != null && dirs.length != directions.length)
		{
			throw new StartupException("Number of directories must match number of directions");
		}
		else
		{
			String sEndTime = config.getProperty("endtime");
			serviceEndTime = Calendar.getInstance();

			try
			{
				serviceEndTime.setTimeInMillis(serviceTimeFormat.parse(sEndTime).getTime());
			}
			catch(Exception e)
			{
				serviceEndTime.set(Calendar.HOUR_OF_DAY, 18);
				serviceEndTime.set(Calendar.MINUTE, 0);
			}

			serviceEndTime.set(Calendar.SECOND, 0);
			serviceEndTime.set(Calendar.MILLISECOND, 0);

			if(hostname == null || query == null || port == -1)
			{
				throw new StartupException("Missing 'service-url' config in pvoutput.ini");
			}

			if(apikey == null)
			{
				throw new StartupException("Missing 'key' config in pvoutput.ini");
			}
			else
			{
				apikey = apikey.trim();
				
				if(apikey.equalsIgnoreCase("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx") || apikey.length() < 40)
				{
					throw new StartupException("Invalid 'key' configuration in pvoutput.ini.");
				}
			}

			if(sid == null)
			{
				throw new StartupException("Missing 'sid' config in pvoutput.ini");
			}
			else
			{
				sid = sid.trim();
				
				if(sid.equals("0"))
				{
					throw new StartupException("Invalid 'sid' configuration in pvoutput.ini.");
				}
			}
			
			// set the system size
			int errors = 0;
			
			Util.sleep(2000);
			
			while(!setSystem())
			{
				Util.sleep(POLL);
				
				errors++;
				
				if(errors > retriesHard)
				{
					throw new StartupException("Stopping service after " + retriesHard + " errors");
				}
			}
			
			errors = 0;
			
			Util.sleep(2000);
			
			// set existing energy
			while(!setStatus())
			{
				Util.sleep(POLL);
				
				errors++;
				
				if(errors > retriesHard)
				{
					throw new StartupException("Stopping service after " + retriesHard + " errors");
				}
			}
			
			errors = 0;
			
			LOGGER.info("--------------------------");
			LOGGER.info("System Name: " + systemName);
			LOGGER.info("System Size: " + systemSize);
			LOGGER.info("Timezone: " + TimeZone.getDefault().getID());
			LOGGER.info("Donation Mode: " + donator);
			LOGGER.info("Max History: " + maxHistoryDays);
			LOGGER.info("Status Interval: " + interval + "-min");
			LOGGER.info("Status Date Time: " + statusDateTime);
			LOGGER.info("Upload Temperature: " + uploadTemperature);
			LOGGER.info("Upload Voltage: " + uploadVoltage);
			LOGGER.info("Energy Generation: " + energyExport);
			LOGGER.info("Energy Consumption: " + energyImport);
			LOGGER.info("Create Local Graph: " + localGraph);
			LOGGER.info("Retry Limit: " + retriesSoft + " to " + retriesHard);
			LOGGER.info("--------------------------");
			
			CurrentcostLogReader ccreader = null;
			AHttpLogReader consumptionReader = null;
			
			for(int i = 0; i < formats.length; i++)
			{
				String format = formats[i].toLowerCase().trim();
				
				String className = "org.pvoutput.integration.reader." + format.substring(0, 1).toUpperCase() + format.substring(1) + "LogReader";

				Object o = null;
				
				try
				{
					o = Class.forName(className).newInstance();
				}
				catch(Exception e)
				{
					LOGGER.error("Unable to create class '" + className + "'", e);
					
					continue;
				}
				
				if("currentcost".equals(format) && ccreader != null)
				{
					LOGGER.warn("Current Cost format can only be used once");
					
					continue;
				}
				
				if(o instanceof ADataLogReader)
				{
					File dirfile = new File(dirs[i].replaceAll("\\\\", "/"));
					
					LOGGER.info("Loaded Reader " + (i+1) + ": " + format);
					LOGGER.info("Log Directory " + (i+1) + ": [" + dirfile.getAbsolutePath() + "]");
					
					ADataLogReader reader = (ADataLogReader)o;
					
					String id = formats[i] + String.valueOf(i+1);

					// read properties
					File propFile = new File("../conf/" + format + ".ini");

					if(propFile.exists())
					{
						LOGGER.info("Loading Config " + (i+1) + ": " + propFile.getName());
						
						FileInputStream in = null;
						
						try
						{
							in = new FileInputStream(propFile);
							reader.properties.load(in);
						}
						catch(Exception e)
						{
							LOGGER.error("Could not read properties file: " + propFile.getAbsolutePath());
						}
						finally
						{
							Util.close(in);
						}
					}
					else
					{
						throw new StartupException("Missing Configuration File " + (i+1) + " '" + propFile.getName() + "'");
					}
					
					reader.setSystemSize(systemSize);
					reader.setInterval(interval);
					reader.setDirectory(dirfile);
					reader.startup();
					reader.initLabels(i+1);
					reader.setFormat(format);
					reader.setServiceEndTime(serviceEndTime);
					reader.setDataFormats();
					reader.setDirection(directions != null ? directions[i] : null);
					
					boolean meterReader = false;
		
					if(o instanceof CurrentcostLogReader)
					{
						// call before startup()
						ccreader = (CurrentcostLogReader)o;
						ccreader.setStatus(statusDateTime, energyImport, energyExport);
						
						meterReader = true;
					}
					else if(o instanceof AHttpLogReader)
					{
						consumptionReader = (AHttpLogReader)o;
						consumptionReader.setStatus(statusDateTime, energyImport, energyExport);
						consumptionReader.setDirectory(dirfile);
						
						if(consumptionReader.getSensors() != null && consumptionReader.getSensors().size() > 0)
						{
							LOGGER.info("Found " + consumptionReader.getSensors().size() + " Sensors");
							
							meterReader = true;
						}
					}
					
					if(!meterReader)
					{
						LOGGER.info("Added Reader: " + id);
						
						readers.put(id, reader);
						
						if(reader.getDirection() == ADataLogReader.IMPORT)
						{
							importReaders++;
						}
						else if(reader.getDirection() == ADataLogReader.EXPORT)
						{
							exportReaders++;
						}
						
						LOGGER.info("Log File " + (i+1) + ": " + ((ADataLogReader)o).getLogFilename());
						LOGGER.info("Direction " + (i+1) + ": " + (reader.getDirection() == ADataLogReader.IMPORT?"Import":"Export"));
						
						if(files != null && files.length > i)
						{
							if(files[i] != null && files[i].length() > 0)
							{
								reader.setFilePattern(files[i]);
							
								LOGGER.info("File Pattern " + (i+1) + ": " + files[i]);
							}
						}
					}
				}
				else
				{
					throw new StartupException("Invalid Reader: " + className);
				}
			}
			
			/*
			 * Handle multiple current cost sensors
			 */
			if(ccreader != null)
			{
				Map<Integer,Sensor> sensors = ccreader.getSensors();
				
				for(Sensor sensor: sensors.values())
				{
					String id = "currentcost" + sensor.id;
					
					CurrentcostLogReader reader = null;
					
					LOGGER.info("Added Reader: " + id);
					
					if(!readers.containsKey(id))
					{
						reader = new CurrentcostLogReader();
						reader.initLabels(readers.size()+1);
						reader.setSystemSize(systemSize);
						reader.setInterval(interval);
						reader.setDirectory(ccreader.getDirectory());
						reader.setFormat(ccreader.getFormat());
						reader.setServiceEndTime(serviceEndTime);
						
						readers.put(id, reader);
					}
					else
					{
						reader = (CurrentcostLogReader)readers.get(id);
					}
					
					reader.setDirection(sensor.direction);
					reader.setSensorId(sensor.id);
					
					LOGGER.info("Log File: " + id + " [" + reader.getLogFilename() + "]");
					
					if(reader.getDirection() == ADataLogReader.IMPORT)
					{
						importReaders++;

						LOGGER.info("Import Sensor: " + sensor.sensor + ", id: " + sensor.id);
					}
					else if(reader.getDirection() == ADataLogReader.EXPORT)
					{
						exportReaders++;

						LOGGER.info("Export Sensor: " + sensor.sensor + ", id: " + sensor.id);
					}
					
				}
			}
			else if(consumptionReader != null)
			{
				Map<Integer,Sensor> sensors = consumptionReader.getSensors();
				
				for(Sensor sensor: sensors.values())
				{
					String id = consumptionReader.getName() + sensor.id;
					
					LOGGER.info("Added Reader: " + id);
					
					AHttpLogReader reader = null;
					
					if(!readers.containsKey(id))
					{
						reader = (AHttpLogReader)consumptionReader.copy();
						reader.initLabels(readers.size()+1);
						reader.setSystemSize(systemSize);
						reader.setInterval(interval);
						reader.setDirectory(consumptionReader.getDirectory());
						reader.setFormat(consumptionReader.getFormat());
						reader.setServiceEndTime(serviceEndTime);
						
						readers.put(id, reader);
					}
					else
					{
						reader = (AHttpLogReader)readers.get(id);
					}
					
					reader.setDirection(sensor.direction);
					reader.setSensorId(sensor.id);
					
					LOGGER.info("Log File: " + id + " [" + reader.getLogFilename() + "]");
					
					if(reader.getDirection() == ADataLogReader.IMPORT)
					{
						importReaders++;

						LOGGER.info("Import Sensor: " + sensor.sensor + ", id: " + sensor.id);
					}
					else if(reader.getDirection() == ADataLogReader.EXPORT)
					{
						exportReaders++;

						LOGGER.info("Export Sensor: " + sensor.sensor + ", id: " + sensor.id);
					}
				}
			}
			
			initExcel(new File("../conf/excel.ini"));
			initWeather(new File("../conf/weather.ini"));
			
			LOGGER.info("Service: [http://" + hostname + ":" + port + query + "] Test: " + dryrun + ", End: " + serviceTimeFormat.format(serviceEndTime.getTime()));
			
			if(readers.size() != importReaders + exportReaders)
			{
				LOGGER.warn("Loaded " + readers.size() + " Readers (Import: " + importReaders + ", Export: " + exportReaders + ")");
			}
			else
			{
				LOGGER.info("Loaded " + readers.size() + " Readers (Import: " + importReaders + ", Export: " + exportReaders + ")");
			}
		}
		
		deserialise();
	}

	private void startAdminConsole(Properties config) 
	{
		if(config.containsKey("admin-port"))
		{
			int port = Util.getNumber(config.getProperty("admin-port"), 8888);

			try
			{
				new AdminConsole(port, installDir);
			}
			catch(Exception e)
			{
				LOGGER.error("Could not start admin console on port: " + port, e);
			}
		}
	}

	private void initWeather(File propFile)
	{
		Properties config = new Properties();
		
		if(propFile.exists())
		{			
			FileInputStream in = null;
			
			try
			{
				in = new FileInputStream(propFile);
				config.load(in);
			}
			catch(Exception e)
			{
				LOGGER.error("Could not read properties file: " + propFile.getAbsolutePath());
			
				return;
			}
			finally
			{
				Util.close(in);
			}
			
			String enabled = config.getProperty("enabled");
			
			
			LOGGER.info("Weather Enabled: " + enabled);
			
			if("true".equalsIgnoreCase(enabled))
			{			
				String state = Util.getState(systemPostcode);
				
				weatherLogger = new WeatherLogger(systemPostcode + "%20" + state + "%20AUSTRALIA", sid, apikey, hostname, port, dryrun);
			
				LOGGER.info("Weather WOEID: " + config.getProperty("woeid"));
				
				long woeid = Util.getLong(config.getProperty("woeid"));
				
				if(woeid > 0)
				{
					weatherLogger.setWoeid(woeid);
				}
			}
		}
		else
		{
			LOGGER.warn("Weather configuration not found: " + propFile.getAbsoluteFile());
		}
	}
	
	/**
	 * Load excel daily configuration file
	 * @param propFile
	 */
	private void initExcel(File propFile) 
	{
		Properties config = new Properties();
		
		if(propFile.exists())
		{			
			FileInputStream in = null;
			
			try
			{
				in = new FileInputStream(propFile);
				config.load(in);
			}
			catch(Exception e)
			{
				LOGGER.error("Could not read properties file: " + propFile.getAbsolutePath());
			
				return;
			}
			finally
			{
				Util.close(in);
			}
			
			// parse daily properties
			if(config.getProperty("daily-file") != null)
			{
				File f = new File(config.getProperty("daily-file"));
					
				excelReader = new ExcelReader(sid, apikey, hostname, port, dryrun);
				excelReader.setInputFile(f);
				excelReader.startup(config);
				
				if(f.exists())
				{
					LOGGER.info(excelReader.toString());
				}
			}
		}
	}

	private void parseURL(String url) 
	{
		if(url.startsWith("http://"))
		{
			url = url.substring(7);
		}
				
		int portIndex = url.indexOf(':');
		int queryIndex = url.indexOf('/');
		
		if(portIndex > -1)
		{
			hostname = url.substring(0, portIndex);
			
			if(queryIndex > -1)
			{
				port = Util.getNumber(url.substring(portIndex+1, queryIndex));
			}
			else
			{
				port = Util.getNumber(url.substring(portIndex+1));
			}
			
			if(queryIndex > -1)
			{
				query = url.substring(queryIndex);
			}
		}
		else
		{
			if(queryIndex > -1)
			{
				hostname = url.substring(0, queryIndex);
				
				query = url.substring(queryIndex);
			}
			else
			{
				hostname = url;
			}
		}
		
		if(query != null)
		{
			query = query.trim();
		}
		
		query = "/service/r2/addbatchstatus.jsp";
	}

	public void run()
	{
		try
		{
			startup();
		}
		catch(StartupException e)
		{
			LOGGER.error("Startup Error: " + e.getMessage());
			
			shutdown();
			
			System.exit(e.getCode());
		}
		catch(Exception e)
		{
			LOGGER.error("Startup Error", e);
			
			return;
		}
		
		int errors = 0;
		
		Util.sleep(2000);
		
		LOGGER.info("Startup Complete: Waiting for data...");
				
		while(true)
		{
			try
			{
				if(excelReader != null)
				{
					if(excelReader.modified())
					{
						excelReader.read();
						excelReader.addoutputs();
					}
				}
				
				process();
				
				if(weatherLogger != null)
				{
					if(hasLogEnded(new Date()))
					{
						weatherLogger.process();
					}
				}
				
				Thread.sleep(POLL);
				
				errors = 0;
			}
			catch (InterruptedException e)
			{
				break;
			}
			catch (Exception e)
			{
				LOGGER.error("Processing Error", e);
			
				if(++errors > retriesHard)
				{
					LOGGER.info("Stopping service after " + retriesHard + " errors");
					
					break;
				}
			}
		}
	}

	private void process()
	throws Exception
	{
		outputs1min.clear();
		outputsInterval.clear();

		long now = System.currentTimeMillis();
		
		for(ADataLogReader reader : readers.values())
		{
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Log directory: '" + reader.getDirectory() + "', Type: " + reader.getClass().getName());
			}
			
			if(!reader.getDirectory().exists())
			{
				LOGGER.warn("Log directory '" + reader.getDirectory().getAbsolutePath() + "' does not exist");
				
				continue;
			}
			
			// get data from external sources
			try
			{
				reader.get();
			}
			catch(Exception e)
			{
				continue;
			}
			
			File logfile = reader.getLogFile();
			
			reader.setLogfile(logfile);

			if(logfile.exists())
			{
				try
				{
					reader.read();
				}
				catch(Exception e)
				{
					LOGGER.error("Error Reading File", e);
					
					throw new InterruptedException();
				}
				
				// aggregate reader outputs				
				if(reader.getSubtitle() != null)
				{
					graphSubtitle = reader.getSubtitle();
				}
	
				// aggregate 1 minute outputs
				Map<Date,Output> data = reader.getData();
	
				for(Map.Entry<Date,Output> entry: data.entrySet())
				{
					if(outputs1min.containsKey(entry.getKey()))
					{
						Output output = outputs1min.get(entry.getKey());
						Output current = entry.getValue();
						
						if(reader.getDirection() == ADataLogReader.EXPORT)
						{
							output.energy += current.energy;
							output.power += current.power;
							
							if(current.temperature != Constant.DEFAULT_TEMPERATURE)
							{
								output.temperature = current.temperature;
							}
							
							if(current.voltage != Constant.DEFAULT_VOLTAGE)
							{
								output.voltage = current.voltage;
							}
							
							output.exports++;
						}
						else
						{
							output.energyImport += current.energy;
							output.powerImport += current.power;
							
							if(current.temperature != Constant.DEFAULT_TEMPERATURE)
							{
								output.temperature = current.temperature;
							}
							
							if(current.voltage != Constant.DEFAULT_VOLTAGE)
							{
								output.voltage = current.voltage;
							}
							
							output.imports++;
						}
					}
					else
					{
						Output output = new Output();
						Output current = entry.getValue();
						
						if(reader.getDirection() == ADataLogReader.EXPORT)
						{
							output.energy = current.energy;
							output.power = current.power;
							
							if(current.temperature != Constant.DEFAULT_TEMPERATURE)
							{
								output.temperature = current.temperature;
							}
							
							if(current.voltage != Constant.DEFAULT_VOLTAGE)
							{
								output.voltage = current.voltage;
							}
							
							output.exports = 1;
						}
						else
						{
							output.energyImport = current.energy;
							output.powerImport = current.power;
							
							if(current.temperature != Constant.DEFAULT_TEMPERATURE)
							{
								output.temperature = current.temperature;
							}
							
							if(current.voltage != Constant.DEFAULT_VOLTAGE)
							{
								output.voltage = current.voltage;
							}
							
							output.imports = 1;
						}
						
						output.time = current.time;

						if(Util.validDateTime(entry.getKey(), maxHistoryDays))
						{
							outputs1min.put(entry.getKey(), output);
						}
					}
				}
	
				// aggregate 5 or 10 minute outputs
				Map<Date,Output> outputs = reader.getOutputs();
	
				for(Map.Entry<Date,Output> entry: outputs.entrySet())
				{
					Date date = entry.getKey();
					
					int days = Util.getDaysDiff(date.getTime(), now);
					
					if(days <= maxHistoryDays)
					{
						if(outputsInterval.containsKey(date))
						{
							Output output = outputsInterval.get(date);
							Output current = entry.getValue();
							
							if(reader.getDirection() == ADataLogReader.EXPORT)
							{
								output.energy += current.energy;
								output.power += current.power;
								
								if(current.temperature != Constant.DEFAULT_TEMPERATURE)
								{
									output.temperature = current.temperature;
								}
								
								if(current.voltage != Constant.DEFAULT_VOLTAGE)
								{
									output.voltage = current.voltage;
								}
								
								if(!Double.isNaN(current.v7))
								{
									output.v7 = current.v7;
								}
								
								if(!Double.isNaN(current.v8))
								{
									output.v8 = current.v8;
								}
								
								if(!Double.isNaN(current.v9))
								{
									output.v9 = current.v9;
								}
								
								if(!Double.isNaN(current.v10))
								{
									output.v10 = current.v10;
								}
								
								if(!Double.isNaN(current.v11))
								{
									output.v11 = current.v11;
								}
								
								if(!Double.isNaN(current.v12))
								{
									output.v12 = current.v12;
								}
								
								output.exports++;
							}
							else
							{							
								output.energyImport += current.energy;
								output.powerImport += current.power;
								
								if(current.temperature != Constant.DEFAULT_TEMPERATURE)
								{
									output.temperature = current.temperature;
								}
								
								if(current.voltage != Constant.DEFAULT_VOLTAGE)
								{
									output.voltage = current.voltage;
								}
								
								if(!Double.isNaN(current.v7))
								{
									output.v7 = current.v7;
								}
								
								if(!Double.isNaN(current.v8))
								{
									output.v8 = current.v8;
								}
								
								if(!Double.isNaN(current.v9))
								{
									output.v9 = current.v9;
								}
								
								if(!Double.isNaN(current.v10))
								{
									output.v10 = current.v10;
								}
								
								if(!Double.isNaN(current.v11))
								{
									output.v11 = current.v11;
								}
								
								if(!Double.isNaN(current.v12))
								{
									output.v12 = current.v12;
								}
								
								output.imports++;
							}
						}
						else
						{
							Output output = new Output();
							Output current = entry.getValue();
							
							if(reader.getDirection() == ADataLogReader.EXPORT)
							{
								output.energy = current.energy;
								output.power = current.power;
								output.temperature = current.temperature;
								
								if(current.voltage != Constant.DEFAULT_VOLTAGE)
								{
									output.voltage = current.voltage;
								}
								
								if(!Double.isNaN(current.v7))
								{
									output.v7 = current.v7;
								}
								
								if(!Double.isNaN(current.v8))
								{
									output.v8 = current.v8;
								}
								
								if(!Double.isNaN(current.v9))
								{
									output.v9 = current.v9;
								}
								
								if(!Double.isNaN(current.v10))
								{
									output.v10 = current.v10;
								}
								
								if(!Double.isNaN(current.v11))
								{
									output.v11 = current.v11;
								}
								
								if(!Double.isNaN(current.v12))
								{
									output.v12 = current.v12;
								}
								
								output.exports = 1;
							}
							else
							{
								output.energyImport = current.energy;
								output.powerImport = current.power;
								output.temperature = current.temperature;
								
								if(current.voltage != Constant.DEFAULT_VOLTAGE)
								{
									output.voltage = current.voltage;
								}
								
								if(!Double.isNaN(current.v7))
								{
									output.v7 = current.v7;
								}
								
								if(!Double.isNaN(current.v8))
								{
									output.v8 = current.v8;
								}
								
								if(!Double.isNaN(current.v9))
								{
									output.v9 = current.v9;
								}
								
								if(!Double.isNaN(current.v10))
								{
									output.v10 = current.v10;
								}
								
								if(!Double.isNaN(current.v11))
								{
									output.v11 = current.v11;
								}
								
								if(!Double.isNaN(current.v12))
								{
									output.v12 = current.v12;
								}
								
								output.imports = 1;		
							}
		
							output.time = current.time;
	
							if(Util.validDateTime(date, maxHistoryDays))
							{
								outputsInterval.put(date, output);
							}
						}
					}
					else
					{	
						LOGGER.warn("Date " + date + " too far in the past : " + date);
					}
				}
			}
			else
			{
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.debug("Log file " + logfile.getAbsolutePath() + " does not exist [" + reader.getFormat() + "]");
				}
			}
		}

		/*
		 * Graph outputs to local file
		 */
		if(localGraph)
		{
			graph();
		}
		
		/*
		 * Send outputs
		 */
		for(Output output: outputsInterval.values())
		{
			if(importReaders > 1 && importReaders != output.imports && output.imports > 0)
			{
				continue;
			}
			
			if(exportReaders > 1 && exportReaders != output.exports && output.exports > 0)
			{
				continue;
			}
			
			if(exportReaders == output.exports && exportReaders > 0 || importReaders == output.imports)
			{
				if(!outputsQueue.containsKey(output.time))
				{
					if(LOGGER.isDebugEnabled())
					{
						LOGGER.debug("Queued " + serviceDateTimeFormat.format(output.time) + " export[" + output.exports + "/" + exportReaders + "] import[" + output.imports + "/" + importReaders + "]");
					}
					
					outputsQueue.put(output.time, output);
				}
			}
			
			if(outputsQueue.containsKey(output.time))
			{
				Output sentOutput = outputsQueue.get(output.time);
				
				if(sentOutput.sent)
				{				
					if(sentOutput.imports != output.imports || sentOutput.exports != output.exports)
					{
						LOGGER.warn("Requeued " + serviceDateTimeFormat.format(output.time) + " export[" + output.exports + "/" + exportReaders + "] import[" + output.imports + "/" + importReaders + "]  old export[" + sentOutput.exports + "/" + exportReaders + "] old import[" + sentOutput.imports + "/" + importReaders + "]");

						outputsQueue.put(output.time, output);
					}
				}		
			}
		}
		
		Iterator<Output> i = outputsQueue.values().iterator();
		
		// batch up the requests
		int batchsize = 0;
		StringBuffer batchRequest = new StringBuffer();
		
		while(i.hasNext())
		{
			Output output = i.next();
			
			if(!output.sent)
			{
				boolean send = true;
				
				if(importReaders == 0)
				{
					if(output.energy == 0 && output.power == 0)
					{
						// before 6:00am
						if(!hasLogStarted(output.time, 6, 0))
						{
							send = false;
						}
					}
					
					if(lastOutputSent.energy == output.energy && lastOutputSent.power == 0 && output.power == 0)
					{
						if(hasLogEnded(output.time))
						{
							send = false;
						}
					}
				}
				
				if(send)
				{
					batchsize++;

					batchRequest.append(";").append(serviceDateFormat.format(output.time))
					.append(",").append(serviceTimeFormat.format(output.time));
					
					if(output.exports > 0)
					{
						batchRequest.append(",").append(String.valueOf(output.energy))
						.append(",").append(String.valueOf(output.power));
					}
					else
					{
						batchRequest.append(",-1,-1");
					}
					
					if(output.imports > 0)
					{
						batchRequest.append(",").append(String.valueOf(output.energyImport))
						.append(",").append(String.valueOf(output.powerImport));
					}
					else
					{
						batchRequest.append(",-1,-1");
					}
					
					if(Util.validTemperature(output.temperature) && uploadTemperature)
					{
						batchRequest.append(",").append(output.temperature);
					}
					else
					{
						batchRequest.append(",").append(String.valueOf(Constant.DEFAULT_TEMPERATURE));
					}
					
					if(Util.validVoltage(output.voltage) && uploadVoltage)
					{
						batchRequest.append(",").append(output.voltage);
					}
					else
					{
						batchRequest.append(",").append(String.valueOf(Constant.DEFAULT_VOLTAGE));
					}
					
					if(donator)
					{
						if(!Double.isNaN(output.v7) 
								|| !Double.isNaN(output.v8)
								|| !Double.isNaN(output.v9)
								|| !Double.isNaN(output.v10)
								|| !Double.isNaN(output.v11)
								|| !Double.isNaN(output.v12))
						{
							batchRequest.append(",").append(Double.isNaN(output.v7) ? "" : nf.format(output.v7));
							batchRequest.append(",").append(Double.isNaN(output.v8) ? "" : nf.format(output.v8));
							batchRequest.append(",").append(Double.isNaN(output.v9) ? "" : nf.format(output.v9));
							batchRequest.append(",").append(Double.isNaN(output.v10) ? "" : nf.format(output.v10));
							batchRequest.append(",").append(Double.isNaN(output.v11) ? "" : nf.format(output.v11));
							batchRequest.append(",").append(Double.isNaN(output.v12) ? "" : nf.format(output.v12));
						}
					}
				
					if(batchsize >= Constant.MAX_BATCH_SIZE)
					{
						break;
					}
				}
			}
			else
			{
				int days = Util.getDaysDiff(output.time.getTime(), now);

				if(days > maxHistoryDays)
				{
					i.remove();
				}
			}
		}
		
		if(batchRequest.length() > 0)
		{
			batchRequest.deleteCharAt(0);
			
			ResponseData response = sendbatch(batchRequest.toString());

			if(response.status == StatusCode.SUCCESS)
			{
				if(response.response != null)
				{
					String[] results = response.response.split(";");
					
					for(String result: results)
					{
						String[] s = result.split(",");
						
						if(s != null && s.length >= 3)
						{
							try
							{
								Date datetime = serviceDateTimeFormat.parse(s[0] + " " + s[1]);
								Output output = outputsQueue.get(datetime);
								
								if(output != null)
								{
									if("1".equals(s[2]) || "2".equals(s[2]))
									{
										output.sent = true;
										
										lastOutputSent.time = output.time;
										lastOutputSent.energy = output.energy;
										lastOutputSent.time = output.time;
										lastOutputSent.power = output.power;
										lastOutputSent.energyImport = output.energyImport;
										lastOutputSent.powerImport = output.powerImport;
										
										if(LOGGER.isDebugEnabled())
										{
											LOGGER.debug("OK " + serviceDateTimeFormat.format(datetime));
										}
									}
									else
									{
										output.errors++;
									}
								}
								else
								{
									if(LOGGER.isDebugEnabled())
									{
										LOGGER.warn("Not found: " + serviceDateTimeFormat.format(datetime));
									}
								}
							}
							catch(Exception e)
							{
							}
						}
					}
				}		
			}
			else if(response.status == StatusCode.ERROR_SECURITY)
			{
				LOGGER.warn("Security error - skipping output");
				
				// set all statuses to sent
				if(response.response != null)
				{
					String[] results = response.response.split(";");
					
					for(String result: results)
					{
						String[] s = result.split(",");
						
						if(s != null && s.length >= 3)
						{
							try
							{
								Date datetime = serviceDateTimeFormat.parse(s[0] + " " + s[1]);
								Output output = outputsQueue.get(datetime);
								
								if(output != null)
								{
									output.sent = true;
								}
							}
							catch(Exception e)
							{
							}
						}
					}
				}
			}
			else if(response.status == StatusCode.ERROR_CONNECTION)
			{
				// keep retrying on connection errors
			}
			else
			{
				// increment all status error counts
				if(response.response != null)
				{
					String[] results = response.response.split(";");
					
					for(String result: results)
					{
						String[] s = result.split(",");
						
						if(s != null && s.length > 4)
						{
							try
							{
								Date datetime = serviceDateTimeFormat.parse(s[0] + " " + s[1]);
								Output output = outputsQueue.get(datetime);
								
								if(output != null)
								{
									if("3".equals(s[4]))
									{
										LOGGER.info("Unrecoverable error [" + output.time + "] - skipping");
										
										output.sent = true;
										
										continue;
									}
									
									output.errors++;
									
									if(output.errors >= retriesSoft)
									{
										LOGGER.warn("Excessive errors [" + output.time + "] retries: " + output.errors + "/" + retriesSoft);
										
										output.sent = true;
									}
								}
							}
							catch(Exception e)
							{
							}
						}
					}
				}
			}
			
			serialise();
		}
	}

	private void deserialise()
	{
		BufferedReader br = null;
		
		File file = new File("../logs/status.log");
		
		if(file.exists())
		{
			try
			{
				br = new BufferedReader(new FileReader(file));
				
				String line;
				
				while((line = br.readLine()) != null)
				{
					Date outputdate = null;
				
					String[] values = line.split(",");
					
					try
					{
						outputdate = isoDateFormat.parse(values[0]);
						
						Calendar outputc = Calendar.getInstance();
						outputc.setTime(outputdate);
						
						Output output = new Output();
						output.sent = true;
						output.time = outputdate;
						
						if(values.length == 3)
						{
							try
							{
								output.exports = Integer.parseInt(values[1]);
							}
							catch(Exception e)
							{
								
							}
							
							try
							{
								output.imports = Integer.parseInt(values[2]);
							}
							catch(Exception e)
							{
								
							}
						}
						else
						{
							output.exports = 1;
							output.imports = 0;
						}
						
						outputsQueue.put(output.time, output);
					}
					catch(Exception e)
					{
						
					}
				}
				
				LOGGER.info("Records restored from " + file.getName() + " : " + outputsQueue.size() );
			}
			catch(Exception e)
			{
				LOGGER.error("Error", e);
			}
			finally
			{
				if(br != null) try{br.close();}catch(Exception e){}
			}
		}
	}
	
	private void serialise()
	{
		BufferedWriter bw = null;
		
		try
		{
			bw = new BufferedWriter(new FileWriter("../logs/status.log"));
			
			for(Output output: outputsQueue.values())
			{
				if(output.sent)
				{
					bw.write(isoDateFormat.format(output.time));
					bw.write(",");
					bw.write(String.valueOf(output.exports));
					bw.write(",");
					bw.write(String.valueOf(output.imports));
					bw.write("\n");
				}
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Error", e);
		}
		finally
		{
			if(bw != null) try{bw.close();}catch(Exception e){}
		}
	}
	
	/**
	 * Graph the outputs to a local html file
	 * 1 minute intervals
	 */
	private void graph()
	{
		StringBuffer seriesTime = new StringBuffer();
		StringBuffer seriesEnergy = new StringBuffer();
		StringBuffer seriesPower = new StringBuffer();
		StringBuffer seriesTemperature = new StringBuffer();
		StringBuffer seriesVoltage = new StringBuffer();
		StringBuffer tbody = null;
		
		Calendar c = Calendar.getInstance();
		
		int today = c.get(Calendar.DAY_OF_YEAR);
		
		for(Output output: outputs1min.values())
		{
			c.setTime(output.time);
			
			int day = c.get(Calendar.DAY_OF_YEAR);
			
			if(day == today)
			{
				seriesTime.append(",'").append(serviceTimeFormat.format(output.time)).append("'");
				seriesEnergy.append(",").append(nf.format(output.energy/1000.0));
				seriesPower.append(",").append(output.power);
				
				if(output.temperature == -1000)
				{
					seriesTemperature.append(",null");
				}
				else
				{
					seriesTemperature.append(",").append(output.temperature);
				}
				
				if(output.voltage == -1)
				{
					seriesVoltage.append(",null");
				}
				else
				{
					seriesVoltage.append(",").append(output.voltage);
				}
				
				tbody = new StringBuffer();
				
				tbody.append("<tr>")
				.append("<td align='right' style='padding-right: 15px'>")
				.append("<a href='http://pvoutput.org/intraday.jsp?sid=").append(sid).append("&dt=").append(serviceDateFormat.format(output.time)).append("'>")
				.append(localDateFormat.format(output.time))
				.append("</a></td>")
				.append("<td align='right' style='padding-right: 15px'>")
				.append(localTimeFormat.format(output.time))
				.append("</td>")
				.append("<td align='right' style='padding-right: 35px'>")
				.append(nf2.format(output.energy/1000.0)).append("kWh")
				.append("</td>")
				.append("<td align='right' style='padding-right: 70px'>")
				.append(output.power).append("W")
				.append("</td>")
				.append("</tr>");
			}
		}

		if(seriesTime.length() > 0)
		{
			File monthDir = new File(installDir, "/data/" + dataDirFormat.format(new Date()));
			
			if(!monthDir.exists())
			{
				monthDir.mkdirs();
			}
			
			File targetFile = new File(monthDir, "daily-graph-" + serviceDateFormat.format(new Date()) + ".html");
			File currentFile = new File(installDir, "/data/latest/daily-graph.html");
			
			BufferedWriter bw = null;
			BufferedReader br = null;
			
			try
			{
				seriesTime.deleteCharAt(0);
				seriesEnergy.deleteCharAt(0);
				seriesPower.deleteCharAt(0);
				
				if(seriesTemperature.length() > 0)
				{
					seriesTemperature.deleteCharAt(0);
				}
				
				if(seriesVoltage.length() > 0)
				{
					seriesVoltage.deleteCharAt(0);
				}
				
				String baseFileName = "/data/templates/daily-graph.html";
				
				File baseFile = new File(installDir, baseFileName);

				if(baseFile.exists())
				{
					br = new BufferedReader(new FileReader(baseFile));
					bw = new BufferedWriter(new FileWriter(targetFile));
	
					String line;
	
					while((line = br.readLine())!=null)
					{
						try
						{
							if(line.contains("[systemName]"))
							{
								systemName = systemName.replaceAll("'", "\\\\\\\\'");
		
								line = line.replaceAll("\\[systemName\\]", systemName);
							}
							else if(line.contains("[dateTime]"))
							{
								line = line.replaceAll("\\[dateTime\\]", graphSubtitle);
							}
							else if(line.contains("[seriesTime]"))
							{
								line = line.replaceAll("\\[seriesTime\\]", seriesTime.toString());
							}
							else if(line.contains("[seriesEnergy]"))
							{
								line = line.replaceAll("\\[seriesEnergy\\]", seriesEnergy.toString());
							}
							else if(line.contains("[seriesPower]"))
							{
								line = line.replaceAll("\\[seriesPower\\]", seriesPower.toString());
							}
							else if(line.contains("[seriesTemperature]"))
							{
								line = line.replaceAll("\\[seriesTemperature\\]", seriesTemperature.toString());
							}
							else if(line.contains("[seriesVoltage]"))
							{
								line = line.replaceAll("\\[seriesVoltage\\]", seriesVoltage.toString());
							}
							else if(line.contains("[tbody]"))
							{
								line = line.replaceAll("\\[tbody\\]", tbody.toString());
							}
						}
						catch(Exception e)
						{
						}

						bw.write(line);
						bw.write("\n");
					}
				}
				else
				{
					LOGGER.error("Missing file '" + baseFile.getAbsolutePath() + "'");
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Processing error", e);
			}
			finally
			{
				if(br != null) try{br.close();}catch(Exception e){}
				if(bw != null) try{bw.close();}catch(Exception e){}
			}
			
			if(targetFile.exists())
			{
				if(currentFile.exists())
				{
					currentFile.delete();
				}
				
				Util.copyFile(targetFile, currentFile);
			}
		}
	}
	
	private ResponseData sendbatch(String request)
	{
		ResponseData response = new ResponseData();
		
		if(dryrun)
		{
			LOGGER.info(">>> http://" + hostname + ":" + port + query);
			
			StringBuffer s = new StringBuffer();
			
			response.status = StatusCode.SUCCESS;
			
			String[] lines = request.split(";");
			
			if(lines != null)
			{
				for(int i = 0; i < lines.length; i++)
				{
					LOGGER.info(">>> " + lines[i]);
					
					String[] fields = lines[i].split(",");
					
					if(fields != null && fields.length >= 4)
					{
						s.append(";").append(fields[0])
						.append(",").append(fields[1])
						.append(",").append(fields[2])
						.append(",").append(fields[3])
						.append(",1");
					}
				}
			}
			
			if(s.length() > 0)
			{
				s.deleteCharAt(0);
				
				response.response = s.toString();
				
				LOGGER.info("<<< OK 200: " + response.response);
			}

			LOGGER.info("<<< [Dryrun]");
			
			return response;
		}
		
		response.status = StatusCode.UNKNOWN;
		
		String[] lines = request.split(";");
		
		if(lines != null)
		{
			for(int i = 0; i < lines.length; i++)
			{
				LOGGER.info(">>> " + lines[i]);
			}
		}
		
		int rc = -1;

		String q = query + "?data=" + request;
		
		org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
		HttpGet httpget = WebClient.getPVOutputGet(hostname, port, q, sid, apikey, format);
		
		try
		{
			HttpContext context = new BasicHttpContext();
			HttpResponse response1 = httpclient.execute(httpget, context);
			rc = response1.getStatusLine().getStatusCode();
			
			HttpEntity entity = response1.getEntity();
			String s = null;
			
			if(entity != null)
				s = EntityUtils.toString(entity);
	
			if(s != null && s.trim().length() > 0)
			{
				if(rc == 200)
				{
					LOGGER.info("<<< " + s.trim());
				}
				else
				{
					LOGGER.warn("<<< [" + rc + "] " + s.trim());
				}
				
				response.response = s.trim();
				response.message = response.response;
			}
			else
			{
				LOGGER.error("<<< Empty response [" + rc + "]");
			}
			
			if(rc == 200)
			{
				response.status = StatusCode.SUCCESS;
			}
			else if(rc == 401)
			{
				response.status = StatusCode.ERROR_SECURITY;
			}
			else if(rc == 400 || rc == 403)
			{
				response.status = StatusCode.ERROR_DATA;
			}
		}
		catch(UnknownHostException e)
		{
			response.status = StatusCode.ERROR_CONNECTION;
			LOGGER.error("Unknown Host: " + e.getMessage());
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
		catch(IOException e)
		{
			response.status = StatusCode.ERROR_CONNECTION;
			LOGGER.error("Connection Error: " + e.getMessage());
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
		catch(Exception e)
		{			
			response.status = StatusCode.ERROR_UNKNOWN;
			LOGGER.error("Unknown error", e);
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
		
		if(response.status != StatusCode.SUCCESS)
		{		
			// simulate the error response
			StringBuffer s = new StringBuffer();
			
			int energy = -1;
			
			if(response.status == StatusCode.ERROR_DATA 
					&& response.message.indexOf("lower than previously recorded value") > 0)
			{
				int start = response.message.indexOf('[');
				int end = response.message.indexOf(']');
				
				if(end > start)
				{
					energy = Util.getInt(response.message.substring(start+1, end));
				}
			}
			
			lines = request.split(";");
			
			if(lines != null)
			{
				for(int i = 0; i < lines.length; i++)
				{
					String[] fields = lines[i].split(",");
					
					if(fields != null && fields.length >= 4)
					{
						s.append(";").append(fields[0])
						.append(",").append(fields[1])
						.append(",").append(fields[2])
						.append(",").append(fields[3]);
						
						// mark as unrecoverable
						if(energy > -1 && energy == (int)Util.getDouble(fields[2]))
						{
							s.append(",3");
						}
						else
						{
							s.append(",0");
						}
					}
				}
			}
			
			if(s.length() > 0)
			{
				s.deleteCharAt(0);
				
				response.response = s.toString();
				
				LOGGER.info("<<< [" + rc + "] " + response.response);
			}
		}
		
		return response;
	}
		
	protected boolean hasLogEnded(Date dTime)
    {
        try
        {
            Calendar c = Calendar.getInstance();
            c.setTime(dTime);

            int endHour = serviceEndTime.get(Calendar.HOUR_OF_DAY);
            int endMinutes = serviceEndTime.get(Calendar.MINUTE);

            int totalEndMinutes = endHour * 60 + endMinutes;

            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minutes = c.get(Calendar.MINUTE);

            int totalMinutes = hour * 60 + minutes;

            if(totalMinutes >= totalEndMinutes)
            {
                return true;
            }
        }
        catch(Exception e)
        {
        }

        return false;
    }
	
	protected boolean hasLogStarted(Date dTime, int startHour, int startMinutes)
    {
    	try
        {
            Calendar c = Calendar.getInstance();
            c.setTime(dTime);

            int totalStartMinutes = startHour * 60 + startMinutes;

            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minutes = c.get(Calendar.MINUTE);

            int totalMinutes = hour * 60 + minutes;

            if(totalMinutes >= totalStartMinutes)
            {
                return true;
            }
        }
        catch(Exception e)
        {
        }

        return false;	
    }
	
	private boolean setStatus() 
    {
		boolean success = false;
		
		String query = "/service/r1/getstatus.jsp?d=" + serviceDateFormat.format(new Date());

		org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
		HttpGet httpget = WebClient.getPVOutputGet(hostname, port, query, sid, apikey, format);
		
		try
		{
			HttpContext context = new BasicHttpContext();
			HttpResponse response1 = httpclient.execute(httpget, context);
			int rc = response1.getStatusLine().getStatusCode();
			
			HttpEntity entity = response1.getEntity();
			String s = null;
			
			if(entity != null)
				s = EntityUtils.toString(entity);
		
			if(s != null && s.trim().length() > 0)
			{
				if(rc == 200)
				{
					String[] f = s.split(",");
					
					LOGGER.info("<<< " + s.trim());

					energyExport = Util.getInt(f[2]);
					energyImport = Util.getInt(f[4]);
					
					try
					{
						statusDateTime = serviceDateTimeFormat.parse(f[0] + " " + f[1]);
					}
					catch(Exception e)
					{
						
					}
					
					success = true;
				}
				else
				{
					if(rc == 400)
					{
						// no status	
						success = true;
					}
					else
					{
						LOGGER.info("<<< [" + rc + "] " + s.trim());
					}
				}
			}
		}
		catch(Exception e)
		{			
			LOGGER.error("GetStatus Error", e);
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
		
		return success;
	}
		
	private boolean setSystem() 
    throws StartupException
    {
		boolean success = false;
		
		String query = "/service/r2/getsystem.jsp?donations=1";

		org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
		HttpGet httpget = WebClient.getPVOutputGet(hostname, port, query, sid, apikey, format);
		
		try
		{
			HttpContext context = new BasicHttpContext();
			HttpResponse response1 = httpclient.execute(httpget, context);
			int rc = response1.getStatusLine().getStatusCode();
			
			HttpEntity entity = response1.getEntity();
			String s = null;
			
			if(entity != null)
				s = EntityUtils.toString(entity);
		
			if(s != null && s.trim().length() > 0)
			{
				if(rc == 200)
				{
					String[] d = s.split(";");
					
					LOGGER.info("<<< " + s.trim());
					
					if(d != null && d.length > 0)
					{
						String[] f = d[0].split(",");
						
						systemName = f[0];
						systemSize = Util.getNumber(f[1]);
						systemPostcode = Util.getNumber(f[2]);
						interval = Util.getNumber(f[15]);
						
						donator = Util.getNumber(d[2], 0) > 0;
						
						if(donator)
						{
							maxHistoryDays = 90;
						}
						
						success = true;
					}
				}
				else if(rc == 401)
				{
					throw new StartupException("PVOutput API Key (" + apikey + ") or System Id (" + sid + ") is invalid");
				}
				else
				{
					LOGGER.error("<<< [" + rc + "] " + s.trim());
				}
			}
		}
		catch(StartupException e)
		{
			throw e;
		}
		catch(Exception e)
		{			
			LOGGER.error("GetSystem Error", e);
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
		
		return success;
	}

	public void shutdown() 
	{
		if(readers != null)
		{
			for(ADataLogReader reader : readers.values())
			{
				try
				{
					reader.shutdown();
				}
				catch(Exception e)
				{
					
				}
			}
		}
		
		LOGGER.info("*** Stopped PVOutput Integration Service v" + Main.VERSION);
	}
	
	public static void main(String[] args)
	{
    	Controller controller = new Controller();
    	Thread application = new Thread(controller);
        
        application.setDaemon(false);
        application.start();
	}
	
}
