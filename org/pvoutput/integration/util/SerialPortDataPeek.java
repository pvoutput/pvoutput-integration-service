/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.util;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

public class SerialPortDataPeek 
{
	private static Logger LOGGER = Logger.getLogger(SerialPortDataPeek.class);
	
	private CommPort commPort;
	private boolean matched;
	private int wait = 15;
	
	public SerialPortDataPeek(int wait)
	{
		this.wait = wait;
	}
	
	public void peek(CommPortIdentifier portIdentifier, int rate)
	throws Exception
	{
		commPort = portIdentifier.open(this.getClass().getName(), 5000);

		if(commPort instanceof SerialPort)
		{
			LOGGER.info("Auto scanning port: " + portIdentifier.getName());
			
			SerialPort serialPort = (SerialPort) commPort;
			serialPort.setSerialPortParams(rate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			InputStream in = serialPort.getInputStream();                
			serialPort.addEventListener(new SerialPortDataReceiver(in));
			serialPort.notifyOnDataAvailable(true);
			
			try
			{
				Thread.sleep(wait * 1000);
			}
			catch(Exception e)
			{
			}
		}
		
		shutdown();
	}
	
	public boolean isMatch()
	{
		return matched;
	}
	
	public void shutdown()
	{
		if(commPort != null)
		{
			LOGGER.debug("Closing Serial Port: " + commPort.getName());
			
			commPort.close();
		}
	}
	
	private class SerialPortDataReceiver implements SerialPortEventListener 
	{
		private InputStream in;
		private byte[] buffer = new byte[1024];
		
		public SerialPortDataReceiver(InputStream in)
		{
			this.in = in;
		}

		public void serialEvent(SerialPortEvent arg0) 
		{
			int data;

			try
			{
				int len = 0;

				while((data = in.read()) > -1)
				{
					if(data == '\n' ) 
					{
						break;
					}

					buffer[len++] = (byte) data;
				}

				String d = new String(buffer,0,len);

				if(d.trim().startsWith("<msg>") && d.trim().endsWith("</msg>"))
				{
					matched = true;
				}
			}
			catch ( IOException e )
			{
				e.printStackTrace();
			}             
		}
	}
}
