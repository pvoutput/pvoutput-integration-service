/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.pvoutput.integration.DailyOutput;
import org.pvoutput.integration.StatusCode;
import org.pvoutput.integration.Util;

public class WeatherLogger 
{
	private static Logger LOGGER = Logger.getLogger(WeatherLogger.class);
	
	private String woeid;
	private String place;
	private Map<Date,String> data = new LinkedHashMap<Date,String>();
	private SimpleDateFormat serviceDateFormat = new SimpleDateFormat("yyyyMMdd");
	private WeatherConditions conditions = new WeatherConditions();
	
	private boolean dryrun = false;
	private String hostname;
	private int port;
	private String sid;
	private String apikey;
	
	public WeatherLogger(String place, String sid, String apikey, String hostname, int port, boolean dryrun)
	{
		this.place = place;
		this.sid = sid;
		this.apikey = apikey;
		this.hostname = hostname;
		this.port = port;
		this.dryrun = dryrun;
	}
	
	public void setWoeid(long woeid)
	{
		this.woeid = String.valueOf(woeid);
	}
	
	private boolean setWoeid() 
    {
		String hostname = "query.yahooapis.com";
		int port = 80;
		
		boolean success = false;
		
		String query = "/v1/public/yql?q=select%20*%20from%20geo.places%20where%20text%20%3D%27" + place + "%27&format=xml";

		org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
		
		HttpGet httpget = WebClient.getHttpGet(hostname, port, query, false);

		try
		{
			HttpContext context = new BasicHttpContext();
			HttpResponse response = httpclient.execute(httpget, context);
			int rc = response.getStatusLine().getStatusCode();
			
			HttpEntity entity = response.getEntity();
			String s = null;
			
			if(entity != null)
				s = EntityUtils.toString(entity);

			if(s != null && s.trim().length() > 0)
			{
				if(rc == 200)
				{
					// woeid
					woeid = Util.getElement(s, "woeid");
					
					LOGGER.info("<<< WOEID: " + woeid);
					
					success = true;
				}
				else
				{
					LOGGER.error("<<< " + s.trim());
				}
			}
			else
			{
				LOGGER.error("<<< [" + rc + "]");
			}
		}
		catch(Exception e)
		{			
			LOGGER.error("GetWOEID Error", e);
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
		
		return success;
	}
		
	private String getWeather() 
    {
		if(woeid == null)
		{
			setWoeid();
		}
		
		if(woeid == null)
		{
			return null;
		}
		
		String hostname = "weather.yahooapis.com";
		int port = 80;
		
		String weathertext = null;
		
		String query = "/forecastrss?w=" + woeid + "&u=c";

		org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
		HttpGet httpget = WebClient.getHttpGet(hostname, port, query, false);
		
		try
		{
			HttpContext context = new BasicHttpContext();
			HttpResponse response = httpclient.execute(httpget, context);
			int rc = response.getStatusLine().getStatusCode();
			
			HttpEntity entity = response.getEntity();
			String s = null;
			
			if(entity != null)
				s = EntityUtils.toString(entity);

			if(s != null && s.trim().length() > 0)
			{
				if(rc == 200)
				{
					weathertext = s;
				}
				else
				{
					LOGGER.error("<<< " + s.trim());
				}
			}
			else
			{
				LOGGER.error("<<< [" + rc + "]");
			}
		}
		catch(Exception e)
		{			
			LOGGER.error("GetWeather Error", e);
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
		
		return weathertext;
	}
	
	public void process()
	{
		deserialise();
		
		if(!isWeatherSet(new Date()))
		{
			String weather = getWeather();
			setWeather(weather);
		}
	}
	
	private boolean isWeatherSet(Date d)
	{
		try
		{
			d = serviceDateFormat.parse(serviceDateFormat.format(d));
		}
		catch(Exception e)
		{
			
		}
		
		return data.containsKey(d);
	}
	
	
	private void setWeather(String s)
	{
		if(s != null)
		{
			int conditionCode = Util.getNumber(Util.getAttribute(s, "yweather:forecast", "code"));
			
			if(conditionCode > -1)
			{
				String conditionText = Util.getAttribute(s, "yweather:forecast", "text");
				
				if(conditionText == null)
					conditionText = "unknown";
				
				conditionText = conditionText.toLowerCase();
				
				int tempMin = Util.getNumber(Util.getAttribute(s, "yweather:forecast", "low"));
				int tempMax = Util.getNumber(Util.getAttribute(s, "yweather:forecast", "high"));
	
				LOGGER.info("<<< Weather Code: " + conditionCode + " -> " + conditionText + " -> " + conditions.getCondition(conditionCode) + ", temperature: " + tempMin + " to " + tempMax + ", woeid: " + woeid);
				
				data.put(new Date(), conditionCode + "," + conditionText + "," + conditions.getCondition(conditionCode) + "," + tempMin + "," + tempMax);
				
				DailyOutput output = new DailyOutput();
				output.condition = conditions.getCondition(conditionCode);
				output.tempLow = tempMin;
				output.tempHigh = tempMax;
				output.time = new Date();
				
				addOutput(output);
			}
			else
			{
				data.put(new Date(), "");
				
				LOGGER.warn("Invalid Weather Response, check your WOEID (" + woeid + ")");
				
				LOGGER.warn("<<< " + s);
			}
			
			serialise();
		}
	}
	
	private void serialise()
	{
		BufferedWriter bw = null;
		
		try
		{
			bw = new BufferedWriter(new FileWriter("../logs/weather.log"));
			
			for(Map.Entry<Date, String> entry: data.entrySet())
			{
				bw.write(serviceDateFormat.format(entry.getKey()));
				bw.write(",");
				bw.write(entry.getValue());
				bw.write("\n");
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Processing error", e);
		}
		finally
		{
			if(bw != null) try{bw.close();}catch(Exception e){}
		}
	}
	
	private void deserialise()
	{
		BufferedReader br = null;
		
		File file = new File("../logs/weather.log");
		
		if(file.exists())
		{
			try
			{
				br = new BufferedReader(new FileReader(file));
				
				String line;
				
				while((line = br.readLine()) != null)
				{
					int offset = line.indexOf(",");
					
					if(offset > 0)
					{
						try
						{
							Date date = serviceDateFormat.parse(line.substring(0, offset));
							
							String text = line.substring(offset+1);
							
							if(text != null && text.length() > 0)
							{
								data.put(date, text);
							}
						}
						catch(Exception e)
						{
							
						}
					}
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Processing error", e);
			}
			finally
			{
				if(br != null) try{br.close();}catch(Exception e){}
			}
		}
	}
	
	private StatusCode addOutput(DailyOutput output)
	{
		StringBuffer query = new StringBuffer("/service/r1/addoutput.jsp");
		
		query.append("?d=").append(serviceDateFormat.format(output.time));
		
		if(output.condition != null && output.condition.length() > 0)
		{	
			query.append("&cd=").append(org.eclipse.jetty.util.UrlEncoded.encodeString(output.condition));
		}
		
		if(output.tempLow >= -100 && output.tempLow <= 100)
		{
			query.append("&tm=").append(String.valueOf(output.tempLow));
		}
		
		if(output.tempHigh >= -100 && output.tempHigh <= 100)
		{
			query.append("&tx=").append(String.valueOf(output.tempHigh));
		}		
		
		if(dryrun)
		{
			LOGGER.info("http://" + hostname + ":" + port + query);
			
			return StatusCode.SUCCESS;
		}
		
		StatusCode status = StatusCode.UNKNOWN;
		
		org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
		HttpGet httpget = WebClient.getPVOutputGet(hostname, port, query.toString(), sid, apikey, null);
		
		try
		{
			HttpContext context = new BasicHttpContext();
			HttpResponse response = httpclient.execute(httpget, context);
			int rc = response.getStatusLine().getStatusCode();
			
			HttpEntity entity = response.getEntity();
			String s = null;
			
			if(entity != null)
				s = EntityUtils.toString(entity);
		
			if(s != null && s.trim().length() > 0)
			{
				if(rc == 200)
				{
					LOGGER.info("<<< " + s.trim());
				}
				else
				{
					LOGGER.error("<<< [" + rc + "] " + s.trim());
				}
			}
			
			if(rc == 200)
			{
				status = StatusCode.SUCCESS;
			}
			else if(rc == 403)
			{
				status = StatusCode.ERROR_SECURITY;
			}
			else if(rc == 400)
			{
				status = StatusCode.ERROR_DATA;
			}
		}
		catch(IOException e)
		{
			status = StatusCode.ERROR_CONNECTION;
			
			LOGGER.error("Connection: " + e.getMessage());
		}
		catch(Exception e)
		{
			status = StatusCode.ERROR_UNKNOWN;
			
			LOGGER.error("AddOutput Error", e);
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
		
		return status;
	}
}
