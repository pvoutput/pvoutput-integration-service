package org.pvoutput.integration;

public class DailyOutput extends Output 
{
	public double exported;
	public String condition;
	public int tempLow = Integer.MIN_VALUE;
	public int tempHigh = Integer.MIN_VALUE;
	public String comments;
	
	public double importPeak = -1;
	public double importOffPeak = -1;
	public double importShoulder = -1;
	public double importHighShoulder = -1;
	
}
