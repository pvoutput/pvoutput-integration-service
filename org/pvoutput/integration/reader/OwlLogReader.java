/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Output;


public class OwlLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(OwlLogReader.class);
	
	private Connection conn = null;
	private String sName = null;
		 
	public void startup()
	throws Exception
	{
		super.startup();
		
		Class.forName("org.sqlite.JDBC");
		
		if(properties.containsKey("database"))
		{
			String dbfile = (String)properties.get("database");
			
			File f = new File(dbfile);
			
			if(f.exists())
			{
				try
				{
					conn = DriverManager.getConnection("jdbc:sqlite:" + dbfile);
					
					LOGGER.info("Connected to database: " + f.getAbsolutePath());
				}
				catch(SQLException e)
				{
					throw new Exception("Could not connect to the database: " + f.getAbsolutePath() + " [" + e.getMessage() + "]");
				}
			}
			else
			{
				throw new Exception("The 'database' file does not exist: " + f.getAbsolutePath());
			}
		}
		else
		{
			throw new Exception("The 'database' value not set in Owl.ini");
		}

		if(properties.containsKey("name"))
		{
			sName = (String)properties.get("name");
		}
		else
		{
			throw new Exception("The 'name' value not set in Owl.ini");
		}

	}
	
	@Override
	public void get()
	{
		logEntries.clear();
		
		int added = 0;
		
		Calendar c = Calendar.getInstance();
		
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
			
		PreparedStatement stmt = null;
		ResultSet rs = null;
		double total = 0;
			            
		try
		{
            StringBuffer sql = new StringBuffer();
            sql.append("SELECT h.ch1_kw_avg, h.hour, h.min from energy_history h, energy_sensor s ")
            .append(" WHERE h.addr = s.addr and s.name='").append(sName)
            .append("' AND h.year = '").append(c.get(Calendar.YEAR))
            .append("' and h.month = '").append(c.get(Calendar.MONTH)+1)
            .append("' and h.day = '").append(c.get(Calendar.DAY_OF_MONTH))
            .append("'")
            .append(" ORDER BY h.hour, h.min");
	            
		    stmt = conn.prepareStatement(sql.toString());
		    
            if(LOGGER.isDebugEnabled())
            {
	           	LOGGER.debug("Query sName: " + sName);
	           	LOGGER.debug("Query: " + sql.toString());
	        }
			    
            rs = stmt.executeQuery();

	        while(rs.next())
	        {
		    	String sData = rs.getString("ch1_kw_avg");
		    	String sHour = rs.getString("hour");
		    	String sMin = rs.getString("min");
		    	
		        c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sHour));
		        c.set(Calendar.MINUTE, Integer.parseInt(sMin));
		        
		        Date TimeOfReading = c.getTime();
		        
		        if (TimeOfReading.getTime() < System.currentTimeMillis())
		        {    	
			        /* 
			         * Despite its name, the database field "ch1_kw_avg" contains the watt-hour-per-min average for that minute, and not kilowatt-hours. 
			         * So multiply "sData" by 60 (mins in hour) to get instantaneous power in the required units. 
			         * Sum over time to get the energy usage. 
			         */
	                double watthour_avg = Double.parseDouble(sData);
	                int power = 0;
	             
		            power = (int) (watthour_avg*60);
	                total += watthour_avg;
						
					Output o = null;
						
					if(logEntries.containsKey(TimeOfReading))
					{
						o = logEntries.get(TimeOfReading);
					}
					else
					{
						o = new Output();
						o.time = TimeOfReading;
						o.power = 0;
						o.energy = 0;
		
						logEntries.put(TimeOfReading, o);
					}
	
					o.power += power;
					o.energy += Math.round(total);
					
					added++;
		        }
		        else
		        {
		    		if(LOGGER.isDebugEnabled())
		    		{
		    			LOGGER.debug("OWL: database entry in the future - discarded for now: " + TimeOfReading);
		    		}
		        }
            }
		}
        catch(Exception e)
        {
            LOGGER.error("Could not read database", e);
        }
        finally
        {
            if(rs!=null)
            {
                try
                {
                    rs.close();
                }
                catch(Exception e)
                {

                }
            }
	
            if(stmt!=null)
            {
                try
                {
                    stmt.close();
                }
                catch(Exception e)
                {

                }
            }
        }
	        
		if(LOGGER.isDebugEnabled())
		{
			LOGGER.debug("Added " + added + " records");
		}

		
		if(added > 0)
		{
			addLog(c);
		}
	}

	@Override
	public String getLogFilename()
	{
        StringBuffer s = new StringBuffer();
        
        s.append("OW").append("-").append(logfileDateFormat.format(new Date())).append(".log");
        
        return s.toString();
	}
	
	@Override
    public File getLogFile()
    {
        return new File(dir, "OW" + "-" + logfileDateFormat.format(new Date()) + ".log");
    }

}
