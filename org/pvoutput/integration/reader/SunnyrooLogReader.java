/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Output;


public class SunnyrooLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(SunnyrooLogReader.class);
	
	private String[] datasources = null;
	private String DATE_COLUMN = "DateAndTime";
	private String POWER_COLUMN = "Output_power";
	private String tableName = "Machine1SpeedN";
	private String username = "";
	private String password = "";
	
	public void startup()
	throws Exception
	{
		super.startup();
		
		Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		
		if(properties.containsKey("datasources"))
		{
			String sSerials = (String)properties.get("datasources");
			
			datasources = sSerials.split(",");
		}
		else
		{
			datasources = new String[]{"sunnyroo"};
			
			LOGGER.error("The 'datasources' value not set in sunnyroo.ini");
		}
		
		if(properties.containsKey("table"))
		{
			tableName = (String)properties.get("table");
		}
	
		LOGGER.info("Table Name: " + tableName);
		
		if(properties.containsKey("column.power"))
		{
			POWER_COLUMN = (String)properties.get("column.power");
		}
		
		LOGGER.info("Power Column: " + POWER_COLUMN);
		
		int n = 1;
		
		for(String filename : datasources)
		{
			LOGGER.info("Datasource " + n + ": " + filename);
			
			n++;
		}
	}
    
	@Override
	public void get()
	{
		logEntries.clear();
		
		int added = 0;
		
		Calendar c = Calendar.getInstance();
		
		for(String filename : datasources)
		{
			Set<Date> fileEntries = new HashSet<Date>();
			
			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rs = null;
			double total = 0;
			String url = "jdbc:odbc:" + filename;
			
			try
			{
				conn = DriverManager.getConnection(url, username, password);
				StringBuffer sql = new StringBuffer();
				sql.append("SELECT ").append(DATE_COLUMN).append(",").append(POWER_COLUMN).append(" FROM ").append(tableName).append(" WHERE DateAndTime >=? and DateAndTime <?");
				sql.append(" ORDER BY ").append(DATE_COLUMN);
				
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.debug(sql.toString());
				}
				
				stmt = conn.prepareStatement(sql.toString());
	            Calendar ts = Calendar.getInstance();	
	            ts.set(Calendar.HOUR_OF_DAY, 0);
	            ts.set(Calendar.MINUTE, 0);
	            ts.set(Calendar.SECOND, 0);
	            ts.set(Calendar.MILLISECOND, 0);
	            stmt.setTimestamp(1, new java.sql.Timestamp(ts.getTimeInMillis()));
	            
	            ts.add(Calendar.DAY_OF_YEAR, 1);
	            stmt.setTimestamp(2, new java.sql.Timestamp(ts.getTimeInMillis()));
	            
	            rs = stmt.executeQuery();
	
	            java.util.Date lastDate = null;
	
	            while(rs.next())
	            {
	                int power = (int)Math.round(rs.getDouble(2)*1000);
	                double energy = 0;
	                long elapse = -1;
	                
	                java.util.Date currentDate = rs.getTimestamp(1);
		                
	                if(lastDate != null)
	                {
	                    elapse = currentDate.getTime() - lastDate.getTime();
	                }
	
	                if(elapse > 0)
	                {
	                    energy = power/((60000/(elapse+0.0))*60000)*1000;
	                    total += energy;
	                }
	                
	                c.setTimeInMillis(currentDate.getTime());
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);
					
					if(!fileEntries.contains(c.getTime()))
					{
						fileEntries.add(c.getTime());
					}
					else
					{
						continue;
					}
					
					Output o = null;
					
					if(logEntries.containsKey(c.getTime()))
					{
						o = logEntries.get(c.getTime());
					}
					else
					{
						o = new Output();
						o.time = c.getTime();
						o.power = 0;
						o.energy = 0;
		
						logEntries.put(c.getTime(), o);
					}

					o.power += power;
					o.energy += Math.round(total);
					
					added++;

	                lastDate = currentDate;
	            }
			}
	        catch(Exception e)
	        {
	            LOGGER.error("Could not read database '" + filename + "'", e);
	        }
	        finally
	        {
	            if(rs!=null)
	            {
	                try
	                {
	                    rs.close();
	                }
	                catch(Exception e)
	                {
	
	                }
	            }
	
	            if(stmt!=null)
	            {
	                try
	                {
	                    stmt.close();
	                }
	                catch(Exception e)
	                {
	
	                }
	            }
	
	            if(conn != null)
	            {
	                try
	                {
	                    conn.close();
	                }
	                catch(Exception e)
	                {
	
	                }
	            }
	        }
	        
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Added " + added + " records from " + filename);
			}
		}
		
		if(added > 0)
		{
			addLog(c);
		}
	}

	@Override
	public String getLogFilename()
	{
        StringBuffer s = new StringBuffer();
        
        s.append("SR").append("-").append(logfileDateFormat.format(new Date())).append(".log");
        
        return s.toString();
	}
	
	@Override
    public File getLogFile()
    {
        return new File(dir, "SR" + "-" + logfileDateFormat.format(new Date()) + ".log");
    }

}
