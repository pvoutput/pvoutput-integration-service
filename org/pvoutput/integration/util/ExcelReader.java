/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.pvoutput.integration.DailyOutput;
import org.pvoutput.integration.StatusCode;

public class ExcelReader 
{
	private static Logger LOGGER = Logger.getLogger(ExcelReader.class);
	
	private Map<Date,DailyOutput> outputs = new HashMap<Date, DailyOutput>();
	private SimpleDateFormat serviceDateFormat = new SimpleDateFormat("yyyyMMdd");
	
	private int sheetIndex = 0;
	private int dateIndex = 0;
	private int energyIndex = 1;
	
	private int exportIndex = -1;
	private int commentIndex = -1;
	private int conditionIndex = -1;
	private int importPeakIndex = -1;
	private int importOffPeakIndex = -1;
	private int importShoulderIndex = -1;
	private int importHighShoulderIndex = -1;
	private int tempHighIndex = -1;
	private int tempLowIndex = -1;
	
	private File inputWorkbook;
	private Date dateFrom;
	private Date dateTo;
	private String[] missingDates;
	private long lastModified = -1;
	
	private boolean dryrun = false;
	private String hostname;
	private int port;
	private String sid;
	private String apikey;
		
	public ExcelReader(String sid, String apikey, String hostname, int port, boolean dryrun)
	{
		this.sid = sid;
		this.apikey = apikey;
		this.hostname = hostname;
		this.port = port;
		this.dryrun = dryrun;
	}
	
	public boolean modified()
	{
		if(inputWorkbook != null && inputWorkbook.exists())
		{
			return inputWorkbook.lastModified() != lastModified;
		}
		
		return false;
	}
	
	public void setSheetIndex(int i)
	{
		sheetIndex = i;
	}
	
	public void setDateIndex(int i)
	{
		dateIndex = i;
	}
	
	public void setEnergyIndex(int i)
	{
		energyIndex = i;
	}
	
	public void setTempHighIndex(int i)
	{
		tempHighIndex = i;
	}
	
	public void setTempLowIndex(int i)
	{
		tempLowIndex = i;
	}
	
	public void setCommentIndex(int i)
	{
		commentIndex = i;
	}
	
	public void setConditionIndex(int i)
	{
		conditionIndex = i;
	}
	
	public void setExportIndex(int i)
	{
		exportIndex = i;
	}
	
	public void setImportPeakIndex(int i)
	{
		importPeakIndex = i;
	}
	
	public void setImportOffPeakIndex(int i)
	{
		importOffPeakIndex = i;
	}
	
	public void setImportShoulderIndex(int i)
	{
		importShoulderIndex = i;
	}
	
	public void setImportHighShoulderIndex(int i)
	{
		importHighShoulderIndex = i;
	}
	
	public void setInputFile(File f) 
	{
		inputWorkbook = f;
	}

	public void read() 
	throws IOException 
	{
		if(inputWorkbook == null || !inputWorkbook.exists())
		{
			return;
		}
		
		lastModified = inputWorkbook.lastModified();
		
		Workbook w = null;
		Calendar today = Calendar.getInstance();
		
 		try 
		{
 			outputs.clear();
 			
			w = Workbook.getWorkbook(inputWorkbook);

			Sheet sheet = w.getSheet(sheetIndex);
			
			int rows = sheet.getRows();
			int cols = sheet.getColumns();
			
			LOGGER.info("Found " + rows + " rows, " + cols + " cols in " + inputWorkbook.getAbsolutePath());
			
			for (int i = 0; i < rows; i++) 
			{
				Date outputDate = null;
				double energy = -1;
				
				// handle date
				Cell cell = sheet.getCell(dateIndex, i);
				
				if(cell != null)
				{
					CellType type = cell.getType();
										
					if(type == CellType.DATE_FORMULA || type == CellType.DATE)
					{
						String msFormat = cell.getCellFormat().getFormat().getFormatString();
						String data = cell.getContents();
						
						msFormat = msFormat.replaceAll("\\\\-", "-")
						.replaceAll(";@", "")
						.replaceAll("m", "M")
						.replaceAll("\\\\ ", " ")
						.replaceAll("\\\\,", ",");
		
						if(msFormat.startsWith("[$-C09]"))
						{
							msFormat = msFormat.substring(7);
						}
						if(msFormat.startsWith("[$-F800]"))
						{
							msFormat = msFormat.substring(8);
						}
						
						if(msFormat.startsWith("ddd\\"))
						{
							msFormat = msFormat.substring(5);
							data = data.substring(4);
						}
						else if(msFormat.startsWith("dddd\\,"))
						{
							msFormat = msFormat.substring(7);
							data = data.substring(6);
						}
				
						SimpleDateFormat df = new SimpleDateFormat(msFormat);
						
						try
						{
							outputDate = df.parse(data);
						}
						catch(Exception e)
						{
							LOGGER.error("Failed to parse date [" + data + "] format[" + msFormat + "]");
						}
					}
					
					// handle energy
					if(outputDate != null)
					{
						if(outputDate.before(today.getTime()))
						{							
							Cell c = sheet.getCell(energyIndex, i);
							
							if(c != null)
							{
								type = c.getType();
								
								if(type == CellType.NUMBER || type == CellType.NUMBER_FORMULA)
								{
									try
									{
										energy = Double.parseDouble(c.getContents());
									}
									catch(Exception e)
									{
									}
								}
							}	
						}
					}
				}
				else
				{
					continue;
				}
				
				if(outputDate != null && energy > -1)
				{				
					if(dateTo == null)
					{
						dateTo = outputDate;
					}
					
					if(dateFrom == null)
					{
						dateFrom = outputDate;
					}
					
					if(outputDate.after(dateTo))
					{
						dateTo = outputDate;
					}
					else if(outputDate.before(dateFrom))
					{
						dateFrom = outputDate;
					}
					
					DailyOutput o = new DailyOutput();
					o.energy = energy * 1000;
					o.time = outputDate;
					
					if(exportIndex == energyIndex)
					{
						o.exported = o.energy;
					}
					else if(exportIndex > -1)
					{
						Cell c = sheet.getCell(exportIndex, i);
						
						CellType type = c.getType();
						
						if(type == CellType.NUMBER || type == CellType.NUMBER_FORMULA)
						{
							try
							{
								energy = Double.parseDouble(c.getContents());
							}
							catch(Exception e)
							{
								
							}
						}
					}
					
					if(commentIndex > -1)
					{
						Cell c = sheet.getCell(commentIndex, i);
						
						if(c != null)
						{
							o.comments = c.getContents();
						}
					}
					
					if(conditionIndex > -1)
					{
						Cell c = sheet.getCell(conditionIndex, i);
						
						if(c != null)
						{
							o.condition = c.getContents();
						}
					}
					
					if(importPeakIndex > -1)
					{
						Cell c = sheet.getCell(importPeakIndex, i);
						
						if(c != null)
						{
							CellType type = c.getType();
							
							if(type == CellType.NUMBER || type == CellType.NUMBER_FORMULA)
							{
								try
								{
									o.importPeak = Double.parseDouble(c.getContents());
								}
								catch(Exception e)
								{
									
								}
							}
						}
					}
					
					if(importOffPeakIndex > -1)
					{
						Cell c = sheet.getCell(importOffPeakIndex, i);
						
						if(c != null)
						{
							CellType type = c.getType();
							
							if(type == CellType.NUMBER || type == CellType.NUMBER_FORMULA)
							{
								try
								{
									o.importOffPeak = Double.parseDouble(c.getContents());
								}
								catch(Exception e)
								{
									
								}
							}
						}
					}
					
					if(importShoulderIndex > -1)
					{
						Cell c = sheet.getCell(importShoulderIndex, i);
						
						if(c != null)
						{
							CellType type = c.getType();
							
							if(type == CellType.NUMBER || type == CellType.NUMBER_FORMULA)
							{
								try
								{
									o.importShoulder = Double.parseDouble(c.getContents());
								}
								catch(Exception e)
								{
									
								}
							}
						}
					}
					
					if(importHighShoulderIndex > -1)
					{
						Cell c = sheet.getCell(importHighShoulderIndex, i);
						
						if(c != null)
						{
							CellType type = c.getType();
							
							if(type == CellType.NUMBER || type == CellType.NUMBER_FORMULA)
							{
								try
								{
									o.importHighShoulder = Double.parseDouble(c.getContents());
								}
								catch(Exception e)
								{
									
								}
							}
						}
					}
					
					if(tempLowIndex > -1)
					{
						Cell c = sheet.getCell(tempLowIndex, i);
						
						if(c != null)
						{
							CellType type = c.getType();
							
							if(type == CellType.NUMBER || type == CellType.NUMBER_FORMULA)
							{
								try
								{
									o.tempLow = Integer.parseInt(c.getContents());
								}
								catch(Exception e)
								{
								}
							}
						}
					}
					
					if(tempHighIndex > -1)
					{
						Cell c = sheet.getCell(tempHighIndex, i);
						
						if(c != null)
						{
							CellType type = c.getType();
							
							if(type == CellType.NUMBER || type == CellType.NUMBER_FORMULA)
							{
								try
								{
									o.tempHigh = Integer.parseInt(c.getContents());
								}
								catch(Exception e)
								{
								}
							}
						}
					}
					
					if(importHighShoulderIndex > -1)
					{
						Cell c = sheet.getCell(importHighShoulderIndex, i);
						
						if(c != null)
						{
							CellType type = c.getType();
							
							if(type == CellType.NUMBER || type == CellType.NUMBER_FORMULA)
							{
								try
								{
									o.importHighShoulder = Double.parseDouble(c.getContents());
								}
								catch(Exception e)
								{
									
								}
							}
						}
					}
					
					outputs.put(outputDate, o);
				}
			}

			LOGGER.info("Found " + outputs.size() + " output records");
		} 
		catch (BiffException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(w != null)
				{
					w.close();
				}
			}
			catch(Exception e)
			{
				
			}
		}
	}

	public void addoutputs()
	{
		setMissing();
		
		if(missingDates != null)
		{	
			int sent = 0;
			
			for(String sdate: missingDates)
			{
				Date date = null;
				
				try
				{
					date = serviceDateFormat.parse(sdate);
				}
				catch(Exception e)
				{
					
				}
				
				if(date != null)
				{
					if(outputs.containsKey(date))
					{
						DailyOutput output = outputs.get(date);
						
						StatusCode status = addOutput(output);
						
						if(status == StatusCode.SUCCESS)
						{
							output.sent = true;
							
							sent++;
						
							if(sent >= 30)
							{
								lastModified = -1;
								
								break;	
							}
						}
						
						try
						{
							Thread.sleep(60000);
						}
						catch(Exception e)
						{
						}
					}
				}
			}
		}
	}
	
	private StatusCode addOutput(DailyOutput output)
	{
		StringBuffer query = new StringBuffer("/service/r2/addoutput.jsp");
		
		String sDate = serviceDateFormat.format(output.time);
		
		query.append("?").append("d=").append(sDate);
		query.append("&").append("g=").append(String.valueOf(output.energy));
		
		//List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);  
		//nameValuePairs.add(new BasicNameValuePair("d", sDate));
		//nameValuePairs.add(new BasicNameValuePair("g", String.valueOf(output.energy)));
		
		StringBuffer b = new StringBuffer();
		
		if(output.exported > -1)
		{
			b.append(" e[" + output.exported + "]");
			
			query.append("&").append("e=").append(String.valueOf(output.exported));
			
			//nameValuePairs.add(new BasicNameValuePair("e", String.valueOf(output.exported)));
		}
		
		if(output.comments != null && output.comments.length() > 0)
		{
			b.append(" cm[" + output.comments + "]");
			
			query.append("&").append("cm=").append(output.comments);
			
			//nameValuePairs.add(new BasicNameValuePair("cm", output.comments));
		}
		
		if(output.condition != null && output.condition.length() > 0)
		{
			b.append(" cd[" + output.condition + "]");
			
			query.append("&").append("cd=").append(output.condition);
			//nameValuePairs.add(new BasicNameValuePair("cd", output.condition));
		}
		
		if(output.tempLow > -40 && output.tempLow < 60)
		{
			b.append(" tm[" + output.tempLow + "]");
			
			query.append("&").append("tm=").append(String.valueOf(output.tempLow));
			
			//nameValuePairs.add(new BasicNameValuePair("tm", String.valueOf(output.tempLow)));
		}
		
		if(output.tempHigh > -40 && output.tempHigh < 60)
		{
			b.append(" tx[" + output.tempHigh + "]");
			
			query.append("&").append("tx=").append(String.valueOf(output.tempHigh));
			
			//nameValuePairs.add(new BasicNameValuePair("tx", String.valueOf(output.tempHigh)));
		}
		
		if(output.importPeak > -1)
		{
			b.append(" ip[" + output.importPeak + "]");
			
			query.append("&").append("ip=").append(String.valueOf(output.importPeak));
			
			//nameValuePairs.add(new BasicNameValuePair("ip", String.valueOf(output.importPeak)));
		}
		
		if(output.importOffPeak > -1)
		{
			b.append(" io[" + output.importOffPeak + "]");
			
			query.append("&").append("io=").append(String.valueOf(output.importOffPeak));
			//nameValuePairs.add(new BasicNameValuePair("io", String.valueOf(output.importOffPeak)));
		}
		
		if(output.importShoulder > -1)
		{
			b.append(" is[" + output.importShoulder + "]");
			
			query.append("&").append("is=").append(String.valueOf(output.importShoulder));
			
			//nameValuePairs.add(new BasicNameValuePair("is", String.valueOf(output.importShoulder)));
		}
		
		if(output.importHighShoulder > -1)
		{
			b.append(" ih[" + output.importHighShoulder + "]");
			
			query.append("&").append("ih=").append(String.valueOf(output.importHighShoulder));
			
			//nameValuePairs.add(new BasicNameValuePair("ih", String.valueOf(output.importHighShoulder)));
		}
				
		//LOGGER.info(">>> d[" + sDate + "] g[" + output.energy + "]" + b);
		
		if(dryrun)
		{
			return StatusCode.SUCCESS;
		}
		
		StatusCode status = StatusCode.UNKNOWN;
			
		org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
		HttpGet httpget = WebClient.getPVOutputGet(hostname, port, query.toString(), sid, apikey, null);
		
		try
		{	
			HttpContext context = new BasicHttpContext();
			HttpResponse response = httpclient.execute(httpget, context);
			int rc = response.getStatusLine().getStatusCode();
			
			HttpEntity entity = response.getEntity();
			String s = null;
			
			if(entity != null)
				s = EntityUtils.toString(entity);
	
			if(s != null && s.trim().length() > 0)
			{
				if(rc == 200)
				{
					LOGGER.info("<<< [" + rc + "] " + s);
				}
				else
				{
					LOGGER.warn("<<< [" + rc + "] " + s);
				}
			}
			else
			{
				LOGGER.warn("<<< [" + rc + "]");
			}
			
			if(rc == 200)
			{
				status = StatusCode.SUCCESS;
			}
			else if(rc == 403)
			{
				status = StatusCode.ERROR_SECURITY;
			}
			else if(rc == 400)
			{
				status = StatusCode.ERROR_DATA;
			}
		}
		catch(IOException e)
		{
			status = StatusCode.ERROR_CONNECTION;
			
			LOGGER.error("Connection: " + e.getMessage());
		}
		catch(Exception e)
		{			
			status = StatusCode.ERROR_UNKNOWN;
			
			LOGGER.error("Unknown: ", e);
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
		
		return status;
	}

	private void setMissing() 
    {
		if(dateFrom == null || dateTo == null)
		{
			return;
		}
		
		String query = "/service/r1/getmissing.jsp";
				
		query += "?df=" + serviceDateFormat.format(dateFrom) + "&dt=" + serviceDateFormat.format(dateTo);
		
		org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
		HttpGet httpget = WebClient.getPVOutputGet(hostname, port, query, sid, apikey, null);
		
		try
		{	
			HttpContext context = new BasicHttpContext();
			HttpResponse response = httpclient.execute(httpget, context);
			int rc = response.getStatusLine().getStatusCode();
			
			HttpEntity entity = response.getEntity();
			String s = null;
			
			if(entity != null)
				s = EntityUtils.toString(entity);
			
			if(rc == 200)
			{
				if(s != null)
				{
					if(s.length() > 0)
					{
						missingDates = s.split(",");
						
						LOGGER.info("<<< Found " + missingDates.length + " missing outputs [" + s + "]");
					}
					else
					{
						missingDates = null;
						
						LOGGER.info("<<< No missing outputs");
					}
				}
			}
			else
			{
				LOGGER.warn("<<< [" + rc + "] " + s);
			}
		}
		catch(Exception e)
		{			
			LOGGER.error("GetMissing Error", e);
			
			if(httpget != null)
			{
				httpget.abort();
			}
		}
	}
	
	public void write(OutputStream out, InputStream in) 
    throws IOException 
    {
        try 
        {
            int l;
            byte[] buffer = new byte[1024];
            
            while ((l = in.read(buffer)) != -1) 
            {
                out.write(buffer, 0, l);
            }
        } 
        finally 
        {
        	try
        	{
        		in.close();
        	}
        	catch(Exception e)
        	{
        		
        	}
        }
    }
	
	public String toString()
	{
		StringBuffer b = new StringBuffer();
		
		b.append("Spreadsheet: " + inputWorkbook.getAbsolutePath() 
		+ ", found:" + (inputWorkbook.exists()?"yes":"no") 
		+ ", worksheet:" + sheetIndex 
		+ ", date:" + dateIndex 
		+ ", energy:" + energyIndex
		);
		
		if(exportIndex > -1)
			b.append(", export:" + exportIndex);
		
		if(commentIndex > -1)
			b.append(", comment:" + commentIndex);
		
		if(tempLowIndex > -1)	
			b.append(", temp-low:" + tempLowIndex);
		
		if(tempHighIndex > -1)
			b.append(", temp-high:" + tempHighIndex);
		
		if(conditionIndex > -1)
			b.append(", condition:" + conditionIndex);
		
		if(importPeakIndex > -1)
			b.append(", import-peak:" + importPeakIndex);
		
		if(importOffPeakIndex > -1)	
			b.append(", import-offpeak:" + importOffPeakIndex);
		
		if(importShoulderIndex > -1)
			b.append(", import-shoulder:" + importShoulderIndex);
		
		if(importHighShoulderIndex > -1)
			b.append(", import-high-shoulder:" + importShoulderIndex);
		
		
		return b.toString();
	}

	public void startup(Properties config) 
	{
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-worksheet"));
			setSheetIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-date"));
			setDateIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-energy"));
			setEnergyIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-export"));
			setExportIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-comment"));
			setCommentIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-temp-min"));
			setTempLowIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-temp-max"));
			setTempHighIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-condition"));
			setConditionIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-import-peak"));
			setImportPeakIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-import-offpeak"));
			setImportOffPeakIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-import-shoulder"));
			setImportShoulderIndex(i);
		}
		catch(Exception e)
		{	
		}
		
		try
		{
			int i = Integer.parseInt(config.getProperty("daily-column-import-high-shoulder"));
			setImportHighShoulderIndex(i);
		}
		catch(Exception e)
		{	
		}
	}
}

