The PVOutput Integration Service allows PV logging (Generation) and energy monitoring (Consumption) data to be collected and uploaded to pvoutput.org

The following monitoring log file formats are supported -

- aurora
- xantrex
- solarlog
- growatt
- suntellite
- currentcost
- flukso
- sunnyroo
- ted5000
- enphase
- enasolar
- csv
- powerwall
