/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Output;


public class DanfossLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(DanfossLogReader.class);
		
	private SimpleDateFormat dataDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyMMdd");
	private String timestampLabel = "TIMESTAMP";
	private String powerLabel = "P_AC";
	private String energyLabel = "E_DAY";
	private String delimiter = ";";
	private boolean deleteFile = false;
	private String renamePrefix = "done-";
	
	private String username = null;
	
	public void startup()
	throws Exception
	{
		super.startup();
				
		// allow logs to start with non-zero energy data
		checkNonZeroEnergyStart = false;
		checkDecreasingEnergy = true;
		
		if(properties.containsKey("username"))
		{
			username = (String)properties.get("username");
		}
		else
		{
			LOGGER.error("The 'username' value not set in danfoss.ini");
		}
		
		if(properties.containsKey("data-time-format"))
		{
			String s = (String)properties.get("data-time-format");
			
			dataDateTimeFormat = new SimpleDateFormat(s);
		}
		
		if(properties.containsKey("file-date-format"))
		{
			String s = (String)properties.get("file-date-format");
			
			fileDateFormat = new SimpleDateFormat(s);
		}
		
		if(properties.containsKey("column.timestamp"))
		{
			timestampLabel = (String)properties.get("column.timestamp");
		}
		
		if(properties.containsKey("column.energy"))
		{
			energyLabel = (String)properties.get("column.energy");
		}
		
		if(properties.containsKey("column.power"))
		{
			powerLabel = (String)properties.get("column.power");
		}
		
		if(properties.containsKey("delete-file"))
		{
			String s = (String)properties.get("delete-file");
			
			if(s.equalsIgnoreCase("y") || s.equalsIgnoreCase("true") || s.equalsIgnoreCase("yes"))
			{
				deleteFile = true;
			}
		}
		
		if(properties.containsKey("rename-prefix"))
		{
			String s = (String)properties.get("rename-prefix");
			
			if(s.length() > 0)
			{
				renamePrefix = s;
			}
		}


		LOGGER.info("Username Prefix: " + username);
		LOGGER.info("Rename Prefix: " + renamePrefix);
		
		LOGGER.info("File Date Format: " + fileDateFormat.toPattern());
		LOGGER.info("Timestamp Format: " + dataDateTimeFormat.toPattern());

	}
	
	@Override
	public void get()
	{
		logEntries.clear();
		
		int added = 0;
		
		Calendar c = Calendar.getInstance();
		
		int currentDay = c.get(Calendar.DAY_OF_MONTH);
		int currentYear = c.get(Calendar.YEAR);
		int currentMonth = c.get(Calendar.MONTH);
		
		// scan directory
		String filePrefix = username + "-" + fileDateFormat.format(c.getTime());
		
		File[] files = dir.listFiles();
		
		File targetFile = null;
		
		for(File f : files)
		{
			if(f.getName().startsWith(filePrefix))
			{
				targetFile = f;
				
				break;
			}
		}
		
		if(targetFile != null)
		{
			BufferedReader br = null;
			
			try
			{
				br = new BufferedReader(new FileReader(targetFile));
				
				String line = null;
				int columns = 0;
				int timestampIndex = 0;
				int powerIndex = 0;
				int energyIndex = 0;
				
				while((line = br.readLine()) != null)
				{
					if(line.indexOf(timestampLabel) > -1
							&& line.indexOf(powerLabel) > -1
							&& line.indexOf(energyLabel) > -1)
					{
						if(LOGGER.isDebugEnabled())
						{
							LOGGER.debug("Found Header: " + line);
						}
						
						String[] header = line.split(delimiter);
						
						if(header != null)
						{
							for(int col = 0; col < header.length; col++)
							{
								if(timestampLabel.equals(header[col]))
								{
									timestampIndex = col;
								}
								
								if(powerLabel.equals(header[col]))
								{
									powerIndex = col;
								}
								
								if(energyLabel.equals(header[col]))
								{
									energyIndex = col;
								}
							}
							
							if(timestampIndex > -1 && powerIndex > -1 && energyIndex > -1)
							{
								columns = header.length;
							}
							else
							{
								LOGGER.warn("Missing labels '" + timestampLabel + "', '" + energyLabel + "' or '" + powerLabel + "'");
							}
						}
					}
					else
					{
						if(columns > 0)
						{
							String[] values = line.split(delimiter);
						
							if(values != null && values.length >= columns)
							{
								String energy = values[energyIndex];
								String power = values[powerIndex];
								String timestamp = values[timestampIndex];
								
								Date currentDate = null;
								
								try
								{
									currentDate = dataDateTimeFormat.parse(timestamp);
								}
								catch(ParseException e)
								{
									throw new Exception("Time [" + timestamp + "] does not match [" + dataDateTimeFormat.toPattern() + "]");
								}
								
								c.setTime(currentDate);
								c.set(Calendar.SECOND, 0);
								c.set(Calendar.MILLISECOND, 0);
								
								int day = c.get(Calendar.DAY_OF_MONTH);
								int year = c.get(Calendar.YEAR);
								int month = c.get(Calendar.MONTH);
								
								if(day == currentDay && month == currentMonth && year == currentYear)
								{
									Output o = null;
									
									if(logEntries.containsKey(c.getTime()))
									{
										o = logEntries.get(c.getTime());
									}
									else
									{
										o = new Output();
										o.time = c.getTime();
										o.power = 0;
										o.energy = 0;
										
										logEntries.put(c.getTime(), o);
									}
									
									try
									{
										o.power = Integer.parseInt(power);
										o.energy = Double.parseDouble(energy)*1000;
									}
									catch(Exception e)
									{
										LOGGER.warn("Could not read energy/power values at " + line);
										
										continue;
									}
									
									added++;
								}
								else
								{
									LOGGER.warn(dataDateTimeFormat.format(c.getTime()) + " does not match year=" + currentYear + ", month=" + currentMonth + ", day=" + currentDay);
								}
							}
						}
					}
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Could not read file " + targetFile.getAbsolutePath(), e);
			}
			finally
			{
				if(br != null)
				{
					try 
					{
						br.close();
					} 
					catch (IOException e) 
					{
					}
				}
			}
			
			if(deleteFile)
			{
				if(targetFile.delete())
				{
					if(LOGGER.isDebugEnabled())
					{
						LOGGER.info("Removed file: " + targetFile.getAbsolutePath());
					}
				}
			}
			else
			{
				File newTargetFile = new File(targetFile.getParentFile(), renamePrefix + targetFile.getName());
				
				if(targetFile.renameTo(newTargetFile))
				{
					if(LOGGER.isDebugEnabled())
					{
						LOGGER.info("Renamed file: " + targetFile.getAbsolutePath());
					}
				}
			}
		}
		else
		{
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.warn("Log file " + filePrefix + "* not found");
			}
		}
		
		if(added > 0)
		{
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.info("Added " + added + " entries");
			}
			
			c = Calendar.getInstance();
			
			addLog(c);
		}
	}

	@Override
	public String getLogFilename()
	{
        StringBuffer s = new StringBuffer();
        
        s.append("DF").append("-").append(logfileDateFormat.format(new Date())).append(".log");
        
        return s.toString();
	}
	
	@Override
    public File getLogFile()
    {
        return new File(dir, "DF" + "-" + logfileDateFormat.format(new Date()) + ".log");
    }

}
