/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.pvoutput.integration.ADataLogReader;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.Sensor;
import org.pvoutput.integration.Util;
import org.pvoutput.integration.util.WebClient;


public abstract class AHttpLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(AHttpLogReader.class);
	
	protected SimpleDateFormat tf2 = new SimpleDateFormat("HH:mm:ss");
	protected double energyExport;
	protected double energyImport;
	protected String[] urls = null;
	protected String rule;
	protected bsh.Interpreter interpreter;
	protected int poll = 10;
	protected int sensorId = 0;
	protected HashMap<Integer,Sensor> sensors = null;
	protected Map<String,String> httpHeaders = new HashMap<String,String>();
	
	public void startup()
	throws Exception
	{
		super.startup();
				
		sensors = new HashMap<Integer,Sensor>();
		
		if(properties.containsKey("url"))
		{
			String s = (String)properties.get("url");
			
			urls = s.split(",");
			
			for(int i = 0; i< urls.length; i++)
			{
				LOGGER.info("URL " + (i+1) + ": " + urls[i]);
			}
		}
		else
		{			
			LOGGER.error("The 'url' value not set in " + getName() + ".ini");
		}
		
		if(properties.containsKey("poll"))
		{
			poll = Util.getNumber((String)properties.get("poll"));
			
			if(poll < 1)
			{
				poll = 10;
			}
			
			LOGGER.info("Poll Frequency: " + poll + " seconds");
		}
		else
		{			
			LOGGER.error("The 'poll' value not set in " + getName() + ".ini");
		}
		
		if(properties.containsKey("rule"))
		{
			rule = properties.getProperty("rule");
			
			if(rule != null && rule.trim().length() > 0)
			{
				interpreter = new bsh.Interpreter();
				
				LOGGER.info("Rule: " + rule);
			}
		}
		
		int sensorsIn = 0;
		int sensorsOut = 0;
		
		try
		{
			String[] aChannels = properties.getProperty("MTU", "").split(",");
			String[] aDirections = properties.getProperty("direction", "in").split(",");
			
			for(int i = 0; i < aDirections.length; i++)
			{
				Sensor sensor = new Sensor();
				sensor.setDirectory(dir);
				sensor.setPrefix(getLogFilePrefix());
				sensor.sensor = i;
				sensor.id = i;
				
				if(aChannels != null)
				{
					sensor.setChannels(aChannels[i]);
				}
				
				if(aDirections != null && aDirections.length > i)
				{
					if("in".equalsIgnoreCase(aDirections[i]))
					{
						sensor.direction = ADataLogReader.IMPORT;
						sensorsIn++;
					}
					else if("out".equalsIgnoreCase(aDirections[i]))
					{
						sensor.direction = ADataLogReader.EXPORT;
						sensorsOut++;
					}
				}
				
				sensors.put(sensor.id, sensor);
				
				LOGGER.info(sensor.toString());
				
				sensor.init(sensorsOut, sensorsIn, energyExport, energyImport);
			}
		}
		catch(Exception e)
		{
			throw new Exception("Could not load configuration in " + getName(), e);
		}
		
		/*
		 * Entries read from local file
		 * Assume the file data is correct
		 */
		if(logEntries.size() > 0)
		{
			statusDateTime = null;
		}
	}
	
	abstract String getLogFilePrefix();
	
	public abstract String getName();
	
	public abstract AHttpLogReader copy();
	
	@Override
	public String getLogFilename()
	{
        return getLogFilePrefix() + sensorId + "-" + logfileDateFormat.format(new Date()) + ".log";
	}
	
	@Override
    public File getLogFile()
    {
        return new File(dir, getLogFilePrefix() + sensorId + "-" + logfileDateFormat.format(new Date()) + ".log");
    }
	
	abstract class SimpleLogFileWriter extends ASimpleLogFileWriter
	{		
		protected int save(Sensor sensor, Output output, Date time)
		{
			int n = 0;
			
			if(!logEntries.containsKey(output.time))
			{
				if(Util.isSameDay(output.time, new Date()))
				{
					if(statusDateTime != null)
					{
						if(!output.time.after(statusDateTime))
						{
							return 0;
						}
					}
					
					logEntries.put(output.time, output);
					
					n++;
				}
			}
			
			return n;
		}
		
		protected void process()
		{
			// calculate energy generated
			Calendar c = Calendar.getInstance();
			int today = c.get(Calendar.DAY_OF_YEAR);
			
			int lastDay = -1;
			double baseEnergy = 0;
			double lastTotal = 0;
			
			if(statusDateTime != null && Util.isSameDay(c.getTime(), statusDateTime))
			{
				lastTotal = getDirection() == EXPORT ? energyExport : energyImport;
				baseEnergy = lastTotal;
			}
			
			BufferedWriter bw = null;
			
			try
			{
				bw = new BufferedWriter(new FileWriter(getLogFile()));
				bw.write(HEADER);
				bw.newLine();
				
				Iterator<Output> i = logEntries.values().iterator();
				
				while(i.hasNext())
				{
					Output output = i.next();

					c.setTime(output.time);
					
					int currentDay = c.get(Calendar.DAY_OF_YEAR);
					
					// remove yesterday's entries
					if(currentDay != today)
					{
						i.remove();
						
						continue;
					}
					
					if(!output.useEnergy)
					{
						if(currentDay != lastDay && baseEnergy == 0)
						{
							output.energy = 0;
						}
						else
						{
							output.energy = lastTotal + output.energy_change;
						}
					}
					
					bw.write(getDataLine(output));
					
					bw.newLine();
					
					lastTotal = output.energy;
					lastDay = currentDay;
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Log Write Error", e);
			}
			finally
			{
				if(bw != null)
				{
					try
					{
						bw.close();
					}
					catch(Exception e)
					{
						
					}
				}
			}
		}
	}
	
	abstract class ASimpleLogFileWriter extends Thread
	{				
		protected void process()
		{
			for(Sensor sensor : sensors.values())
			{
				sensor.process();
			}
		}
		
		@Override
		public void run()
		{ 
			while(true)
			{
				try
				{
					if(get() > 0)
					{
						process();
					}
				}
				catch(Exception e)
				{
					LOGGER.error("Could not read data", e);
				}

				try
				{
					Thread.sleep(poll * 1000);
				}
				catch(Exception e)
				{

				}
			}
		}
		
		abstract List<Output> parse(Sensor sensor, String s);
		
		protected int save(Sensor sensor, Output output, Date time)
		{
			synchronized(sensor.readings)
			{						
				if(sensor.lastTime != null)
				{
					Calendar lastCal = Calendar.getInstance();
					lastCal.setTime(sensor.lastTime);
					
					Calendar currentCal = Calendar.getInstance();
					currentCal.setTime(time);
					
					if(lastCal.get(Calendar.DAY_OF_YEAR) != currentCal.get(Calendar.DAY_OF_YEAR))
					{
						// new day
						sensor.readings.clear();
					}
				}
				
				sensor.readings.put(time, output);
				
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.debug("Added " + output.power + "W " + tf2.format(time) + " (" + sensor.readings.size() + ")");
				}
			}

			sensor.lastTime = time;
			
			return 1;
		}
		
		private int get()
		{
			StringBuffer data = new StringBuffer();
		
			for(String url: urls)
			{
				parseURL(url);
			
				org.apache.http.client.HttpClient httpclient = WebClient.getWebClient();
				
				HttpGet httpget = WebClient.getHttpGet(getHttpHost(), getHttpPort(), getHttpQuery(), isSecure());
				
				try
				{
					for(Map.Entry<String,String> entry: httpHeaders.entrySet())
					{
						httpget.setHeader(entry.getKey(), entry.getValue());
					}
					
					HttpContext context = new BasicHttpContext();
					HttpResponse response = httpclient.execute(httpget, context);
					int rc = response.getStatusLine().getStatusCode();
					
					HttpEntity entity = response.getEntity();
					String s = null;
					
					if(entity != null)
						s = EntityUtils.toString(entity);

					if(s != null && s.trim().length() > 0)
					{
						if(rc == 200)
						{
							if(LOGGER.isDebugEnabled())
							{
								LOGGER.debug("<<< " + s);
							}
							
							// add to the data buffer if successful
							data.append(s);
						}
						else
						{
							LOGGER.error("<<< [" + rc + "] " + s.trim());
						}
					}
					else
					{
						LOGGER.error("<<< [" + rc + "]");
					}
				}
				catch(Exception e)
				{
					LOGGER.error("Http Error", e);
					
					if(httpget != null)
					{
						httpget.abort();
					}
				}
			}
		
			int n = 0;
			
			if(data.length() > 0)
			{
				Date time = new Date();
				
				for(Sensor sensor : sensors.values())
				{
					List<Output> outputs = parse(sensor, data.toString());
					
					if(outputs == null || outputs.size() == 0)
						continue;
					
					for(Output output : outputs)
					{
						if(output.time == null)
						{
							output.time = new Date(time.getTime());
						}
						
						// apply the rule
						if(rule != null && interpreter != null)
						{
							Calendar c = Calendar.getInstance();
							
							try
							{
								interpreter.set("power", output.power);
								interpreter.set("hour", c.get(Calendar.HOUR_OF_DAY));
								interpreter.set("minute", c.get(Calendar.MINUTE));
								interpreter.set("in", sensor.direction == ADataLogReader.IMPORT ? true : false);
								interpreter.set("out", sensor.direction == ADataLogReader.EXPORT ? true : false);
								
								interpreter.eval(rule);
								
								Object o = interpreter.get("power");
								
								if(o instanceof Integer)
								{
									output.power = (Integer)o;
								}
								else if(o instanceof Double)
								{
									output.power = (int)Math.round(((Double)o).doubleValue());
								}
							}
							catch(Exception e)
							{
								LOGGER.error("Rule Error [" + rule + "]");
							}
						}
						
						n += save(sensor, output, time);
					}
				}
			}
				
			return n;
		}	
	}
	
	public void setStatus(Date statusDateTime, int energyImport, int energyExport)
	{
		this.statusDateTime = statusDateTime;
		this.energyImport = energyImport;
		this.energyExport = energyExport;
	}
	
	public HashMap<Integer,Sensor> getSensors()
	{
		return sensors;
	}
	
	public void setSensorId(int i) 
	{
		sensorId = i;
	}
		
}
