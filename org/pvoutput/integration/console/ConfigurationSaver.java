/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.console;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Util;

public class ConfigurationSaver 
{
	private static Logger LOGGER = Logger.getLogger(ConfigurationSaver.class);
	
	private File file;
	
	public ConfigurationSaver(File file)
	{
		this.file = file;
	}
	
	public boolean save(Properties p)
	{
		int changes = 0;
		
		Properties prop = (Properties)p.clone();
		
		if(file.exists())
		{
			//SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd-HHmmss");
			
			File fileTmp = new File(file.getParentFile(), file.getName() + ".tmp");
			//File fileBak = new File(file.getParentFile(), file.getName() + "." + df.format(new Date())  + ".bak");
			
			BufferedReader br = null;
			BufferedWriter bw = null;
			
			try
			{
				br = new BufferedReader(new FileReader(file));
				bw = new BufferedWriter(new FileWriter(fileTmp));
				
				String line = null;
				String lastLine = null;
				
				while((line = br.readLine()) != null)
				{
					boolean changed = false;
					
					if(!line.trim().startsWith("#"))
					{
						int offset = line.indexOf('=');
						
						if(offset > 0)
						{
							String key = line.substring(0, offset);
							String value = line.substring(offset+1);
							
							if(prop.containsKey(key))
							{
								String newValue = prop.getProperty(key);
								
								if(!Util.equals(newValue, value))
								{
									bw.write(key);
									bw.write("=");
									bw.write(newValue);
									
									changes++;
									
									changed = true;
								}
								
								prop.remove(key);
							}
							else
							{
								// removes from the file
								changed = true;
								
								changes++;
							}
						}
					}
					
					if(!changed)
					{
						bw.write(line);
					}
					
					// ignore multiple new lines
					if(lastLine != null 
							&& lastLine.trim().length() == 0
							&& line != null 
							&& line.trim().length() == 0)
					{
						continue;
					}
					
					lastLine = line;
					
					bw.write(System.getProperty("line.separator"));
				}
				
				// keys in the properties but not the file
				if(prop.size() > 0)
				{
					for(Entry<Object,Object> entry : prop.entrySet())
					{
						Object key = entry.getKey();
						Object value = entry.getValue();
						
						if(key instanceof String && value instanceof String)
						{
							bw.write(System.getProperty("line.separator"));
							
							bw.write((String)key);
							bw.write("=");
							bw.write((String)value);
							
							changes++;
						}	
					}
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Processing error", e);
			}
			finally
			{
				Util.close(br);
				Util.close(bw);
				
				if(changes > 0)
				{
					//file.renameTo(fileBak);
					
					file.delete();
					
					fileTmp.renameTo(file);
				}
				else
				{
					fileTmp.delete();
				}
			}
		}
		
		return changes > 0;
	}
	
	public static void main(String[] args)
	{
		Properties prop = new Properties();
		prop.put("sid", "21");
		prop.put("key", "541c2c929b20fa777933b3574223aaaf9046d592");
		prop.put("direction", "");
		
		ConfigurationSaver o = new ConfigurationSaver(new File("c:/apps/pvoutput.org/conf/pvoutput.ini"));
		o.save(prop);
	}
	
	
}
