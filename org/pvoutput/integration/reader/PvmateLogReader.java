/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Output;


public class PvmateLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(PvmateLogReader.class);
	
	private SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Connection conn = null;
	private String[] serials = null;
	
	public void startup()
	throws Exception
	{
		super.startup();
		
		Class.forName("org.sqlite.JDBC");
		
		if(properties.containsKey("database"))
		{
			String dbfile = (String)properties.get("database");
			
			File f = new File(dbfile);
			
			if(f.exists())
			{
				try
				{
					conn = DriverManager.getConnection("jdbc:sqlite:" + dbfile);
					
					LOGGER.info("Connected to database: " + f.getAbsolutePath());
				}
				catch(SQLException e)
				{
					throw new Exception("Could not connect to the database: " + f.getAbsolutePath() + " [" + e.getMessage() + "]");
				}
			}
			else
			{
				throw new Exception("The 'database' file does not exist: " + f.getAbsolutePath());
			}
		}
		else
		{
			throw new Exception("The 'database' value not set in pvmate.ini");
		}
		
		if(properties.containsKey("serial_number"))
		{
			String sSerials = (String)properties.get("serial_number");
			
			serials = sSerials.split(",");
		}
		else
		{
			throw new Exception("The 'serial_number' value not set in pvmate.ini");
		}
		
		int n = 1;
		
		for(String serial : serials)
		{
			LOGGER.info("Serial " + n + ": " + serial);
			
			n++;
		}
	}
	
	@Override
	public void get()
	{
		logEntries.clear();
		
		int added = 0;
		
		Calendar c = Calendar.getInstance();
		
		for(String serial : serials)
		{
			Set<Date> fileEntries = new HashSet<Date>();
			
			PreparedStatement stmt = null;
			ResultSet rs = null;
			double total = 0;
			            
			try
			{
				Calendar ts = Calendar.getInstance();	
	            ts.set(Calendar.HOUR_OF_DAY, 0);
	            ts.set(Calendar.MINUTE, 0);
	            ts.set(Calendar.SECOND, 0);
	            ts.set(Calendar.MILLISECOND, 0);

	            String dateFrom = logfileDateFormat.format(ts.getTime());
	            
	            ts.add(Calendar.DAY_OF_YEAR, 1);
	            String dateTo = logfileDateFormat.format(ts.getTime());
	            
	            StringBuffer sql = new StringBuffer();
	            sql.append("SELECT l.monitor_data, l.log_time from MATE_INVERTER i, MATE_MONITOR_LOG l ")
	            .append(" WHERE l.inverter_id = i.id and sn_name='").append(serial).append("' and state_code != 0")
	            .append(" AND l.log_time > '").append(dateFrom).append("' and l.log_time < '").append(dateTo).append("'")
	            .append(" ORDER BY l.log_time");
	            
	            if(LOGGER.isDebugEnabled())
	            {
	            	LOGGER.debug("Query: " + sql.toString());
	            }
	            
			    stmt = conn.prepareStatement(sql.toString());
			    
	            rs = stmt.executeQuery();
	
	            java.util.Date lastDate = null;
	
	            while(rs.next())
	            {
			    	String line = rs.getString("monitor_data");
			    	String data[] = line.split(",");
			    	
			    	double temp = (Integer.parseInt(data[20])/10.0);
	                int power = Integer.parseInt(data[7]);
	                double energy = 0;
	                long elapse = -1;
	                
	                String sTimestamp = rs.getString("log_time");
	                
	                java.util.Date currentDate = null;
	                
	                try
	                {
	                	currentDate = dtf.parse(sTimestamp);
	                }
	                catch(Exception e)
	                {
	                	continue;
	                }
		                
	                if(lastDate != null)
	                {
	                    elapse = currentDate.getTime() - lastDate.getTime();
	                }
	
	                if(elapse > 0)
	                {
	                    energy = power/((60000/(elapse+0.0))*60000)*1000;
	                    total += energy;
	                }
	                
	                c.setTimeInMillis(currentDate.getTime());
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);
					
					if(!fileEntries.contains(c.getTime()))
					{
						fileEntries.add(c.getTime());
					}
					else
					{
						continue;
					}
					
					Output o = null;
					
					if(logEntries.containsKey(c.getTime()))
					{
						o = logEntries.get(c.getTime());
					}
					else
					{
						o = new Output();
						o.time = c.getTime();
						o.power = 0;
						o.energy = 0;
						o.temperature = temp;
		
						logEntries.put(c.getTime(), o);
					}

					o.power += power;
					o.energy += Math.round(total);
					
					added++;

	                lastDate = currentDate;
	            }
			}
	        catch(Exception e)
	        {
	            LOGGER.error("Could not read database", e);
	        }
	        finally
	        {
	            if(rs!=null)
	            {
	                try
	                {
	                    rs.close();
	                }
	                catch(Exception e)
	                {
	
	                }
	            }
	
	            if(stmt!=null)
	            {
	                try
	                {
	                    stmt.close();
	                }
	                catch(Exception e)
	                {
	
	                }
	            }
	        }
	        
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Added " + added + " records");
			}
		}
		
		if(added > 0)
		{
			addLog(c);
		}
	}

	@Override
	public String getLogFilename()
	{
        StringBuffer s = new StringBuffer();
        
        s.append("PM").append("-").append(logfileDateFormat.format(new Date())).append(".log");
        
        return s.toString();
	}
	
	@Override
    public File getLogFile()
    {
        return new File(dir, "PM" + "-" + logfileDateFormat.format(new Date()) + ".log");
    }

}
