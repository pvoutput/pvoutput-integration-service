/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;

import org.pvoutput.integration.ADataLogReader;


public class CmsLogReader extends ADataLogReader
{
	@Override
	public String getLogFilename()
	{
		String pattern = "yyMMdd";
		
		String name = getLogFilename(pattern);
		
		if(name != null)
		{
			return name;
		}
		
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		
		return df.format(new java.util.Date()) + ".csv";
	}
	
	@Override
    public File getLogFile()
    {
    	String pattern = "yyMMdd";
    	
    	File file = getLogFile(pattern);
    	
    	if(file != null)
    	{
    		return file;
    	}

        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return new File(dir, df.format(new java.util.Date()) + ".csv");
    }
	
    @Override
	public void setLabels()
	{
        if(labelTime == null)
        	labelTime = "Time";
        
        if(labelEnergy == null)
        	labelEnergy = "E-Total(kWh)";
        
        if(labelPower == null)
        	labelPower = "Pac(W)";
	}
    
	@Override
	public void startup()
	{
		checkNonZeroEnergyStart = true;
		checkDecreasingEnergy = true;
	}
	
	@Override
	public void shutdown()
	{	
	}
	
	@Override
	public void get()
	{
	}
	
    @Override
    public String getLogDateFormat()
    {
        return "yyyy/MM/dd HH:mm:ss";
    }

    @Override
    public String getLogTimeFormat()
    {
        return "yyyy/MM/dd HH:mm:ss";
    }
    
    @Override
    public void read() throws Exception
    {
    	initReader();
        
		BufferedReader br = null;

		try
		{
			br = new BufferedReader(new FileReader(logfile));
			String line;
			boolean startRead = false;			  
            int columns = 0;
            int row = 0;
            int startEnergy = 0;

            indexDate = 0;
            
			while((line = br.readLine()) != null)
			{
				if(!startRead)
				{
					if(line.trim().toLowerCase().startsWith("time"))
					{
						String measurements = line.trim();

						String[] measurementArray = measurements.split(",");

                        columns = measurementArray.length;

						for(int i = 0; i < columns; i++)
						{
							if(measurementArray[i] != null)
							{
								if("E-Total(kWh)".equalsIgnoreCase(measurementArray[i].trim()))
								{
									indexEnergy = i;
								}
								else if("Pac(W)".equalsIgnoreCase(measurementArray[i].trim()))
								{
									indexPower = i;
								}
								else if("Time".equalsIgnoreCase(measurementArray[i].trim()))
								{
									indexTime = i;
								}
							}
						}

						if(!validateLabels())
						{
							return;
						}

                        startRead = true;
					}
				}
				else
				{					
					String[] data = line.split(",");

                    if(data.length == columns)
                    {
                    	if(row == 0)
                    	{
                    		try
                    		{
                    			startEnergy = (int)Math.round(Double.parseDouble(data[indexEnergy])*1000);
                    		}
                    		catch(Exception e)
                    		{
                    			
                    		}
                    		
                    		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
                    		
                            date = logFileDateFormat.parse(data[indexTime]);
                            graphSubtitle = df.format(date);
                    	}
                    	
                    	String energy = String.valueOf((int)Math.round(Double.parseDouble(data[indexEnergy])*1000)-startEnergy);
                    	
                        add(data, data[indexTime], energy, data[indexPower], null, null, "0");
                        
                        row++;
                    }
				}
			}
		}
		catch(Exception e)
		{
			throw e;
		}
        finally
        {
            if(br != null) try{br.close();}catch(Exception e){}
        }
	}



}
