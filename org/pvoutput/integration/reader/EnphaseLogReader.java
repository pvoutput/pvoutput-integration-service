/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.Sensor;
import org.pvoutput.integration.Util;


public class EnphaseLogReader extends AHttpLogReader
{	
	private static Logger LOGGER = Logger.getLogger(EnphaseLogReader.class);
	private boolean isAPI = true;
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	
	public void startup()
	throws Exception
	{
		super.startup();

		checkNonZeroEnergyStart = true;
		checkDecreasingEnergy = true;
		
		if(urls.length == 1)
		{
			if(urls[0].endsWith("production"))
			{
				isAPI = false;
			}
			else
			{
				if(properties.containsKey("enlighten-api-key") && properties.containsKey("enlighten-system-id"))
				{
					urls[0] += "/systems/" + properties.getProperty("enlighten-system-id") + "/stats?key=" + properties.getProperty("enlighten-api-key");
					
					LOGGER.info("Enlighten URL: " + urls[0].substring(0, urls[0].indexOf("key=")+4) + "hidden");
				}
			}
		}
		
		EnphaseFileWriter w = new EnphaseFileWriter();
		w.start();
	}
	
	public String getName()
	{
		return "enphase";
	}
	
	public AHttpLogReader copy()
	{
		return new EnphaseLogReader();
	}
	
	public String getLogFilePrefix()
	{
		return "EPH";
	}
		
	private class EnphaseFileWriter extends SimpleLogFileWriter
	{
		public List<Output> parse(Sensor sensor, String s)
		{
			List<Output> outputs = new ArrayList<Output>();

			if(LOGGER.isDebugEnabled())
			{
				LOGGER.info("<<< " + s);
			}
			
			if(isAPI)
			{				
				JSONObject jsonObject = (JSONObject)JSONValue.parse(s);
				
				if(jsonObject != null)
				{
					Object ototaldevices = jsonObject.get("total_devices");
					int totaldevices = 0;
					
					if(ototaldevices instanceof Long)
					{
						totaldevices = ((Long)ototaldevices).intValue();
					}
					
					Object o = jsonObject.get("intervals");
					
					if(o instanceof JSONArray)
					{
						JSONArray intervals = (JSONArray)o;
						
						Iterator i = intervals.iterator();
						
						while(i.hasNext())
						{
							Object ointerval = i.next();
							
							if(ointerval instanceof JSONObject)
							{
								JSONObject data = (JSONObject)ointerval;
								
								Object opowr = data.get("powr");
								Object oenwh = data.get("enwh");
								Object odate = data.get("end_date");
								Object odevices = data.get("devices_reporting");
								
								if(odevices instanceof Long)
								{
									int reporting = ((Long)odevices).intValue();
									
									if(reporting >= totaldevices)
									{
										Output output = new Output();
										
										if(opowr instanceof Long)
										{
											output.power = ((Long)opowr).intValue();
										}
										else if(opowr instanceof Double)
										{
											output.power = ((Double)opowr).intValue();
										}
										
										if(oenwh instanceof Long)
										{
											output.energy_change = ((Long)oenwh).doubleValue();
										}
										else if(oenwh instanceof Double)
										{
											output.energy_change = ((Double)oenwh).doubleValue();
										}
										
										String d = (String)odate;
										
										// remove colon
										int offset = d.lastIndexOf(':');
										
										if(offset > 0)
										{
											StringBuffer b = new StringBuffer(d);
											b.deleteCharAt(offset);
											d = b.toString();
										}
										
										Date date = null;
										
										try
										{
											date = df.parse(d);
											output.time = date;
											
											outputs.add(output);
										}
										catch(Exception e)
										{
											LOGGER.error("Could not read date [" + d + "]");
										}
									}
								}
							}
						}
					}
				}
				else
				{
					LOGGER.error("Could not read data [" + s + "]");
				}
			}
			else
			{
				String value = s.replaceFirst(".*<td>Currently</td><td>([\\d\\s\\,]+)W</td>.*", "$1");
	
				if(value != null && value.length() < s.length())
				{
					Output output = new Output();
					output.power = Util.getNumber(value.replaceAll("[^\\p{N}]", ""), 0);
					
					outputs = new ArrayList<Output>();
					outputs.add(output);
				}
			}

			return outputs;
		}
	}
}
