/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.pvoutput.integration.ADataLogReader;

public class XantrexLogReader extends ADataLogReader
{	
	private boolean customLabel = false;
	
	@Override
	public void startup()
	{
		checkNonZeroEnergyStart = true;
		checkDecreasingEnergy = true;
	}
	
	@Override
	public void shutdown()
	{	
	}
	
    @Override
    public String getLogDateFormat() {
        return "MM-dd-yyyy";
    }

    @Override
    public String getLogTimeFormat() {
        return "HH:mm:ss";
    }
	
	@Override
	public String getLogFilename()
	{
		String pattern = "MM-dd-yyyy";
		
		String name = getLogFilename(pattern);
		
		if(name != null)
		{
			return name;
		}
		
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		
		return "GT Data Log " + df.format(new java.util.Date()) + ".xls";
	}
	
    @Override
    public File getLogFile() {
    	
    	String pattern = "MM-dd-yyyy";
    	
    	File f = getLogFile(pattern);

    	if(f != null)
    	{
    		return f;
    	}
    	
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		return new File(dir, "GT Data Log " + df.format(new java.util.Date()) + ".xls");
    }
        
	@Override
	public void get()
	{
	}
		
	@Override
	public void setLabels()
	{
        if(labelDate == null)
        	labelDate = "Date";
        
        if(labelTime == null)
        	labelTime = "Time";
        
        if(labelEnergy == null)
        	labelEnergy = "AC Wh";
        else
        	customLabel = true;
        
        if(labelPower == null)
        	labelPower = "AC Pwr";
        else
        	customLabel = true;
        
        if(labelTemperature == null)
        	labelTemperature = "HS Tmp";
        
        if(labelVoltage == null)
        	labelVoltage = "VAC";
	}
	
    @Override
    public void read() throws Exception
    {
    	initReader();
        
        BufferedReader br = null;

        SimpleDateFormat logdtf = new SimpleDateFormat(getLogDateFormat() + " " + getLogTimeFormat());
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
        
		try
		{
			br = new BufferedReader(new FileReader(logfile));
			String line;
			boolean startRead = false;		
            int columns = 0;
            
			while((line = br.readLine()) != null)
			{
				if(!startRead)
				{
					if(line.startsWith("Inverter Logged Performance Data"))
					{
						String measurements = br.readLine();

						String[] measurementArray = measurements.split("\t");

                        columns = measurementArray.length;
                        
						for(int i = 0; i < measurementArray.length; i++)
						{
							if(labelDate.equalsIgnoreCase(measurementArray[i]))
							{
								indexDate = i;
							}
                            else if(labelTime.equalsIgnoreCase(measurementArray[i]))
							{
								indexTime = i;
							}
                            else if(labelPower.equalsIgnoreCase(measurementArray[i]))
							{
                            	if(indexPower == -1)
                            	{
                            		indexPower = i;
                            	}
							}
							else if(labelEnergy.equalsIgnoreCase(measurementArray[i]))
							{
								if(indexEnergy == -1)
								{
									indexEnergy = i;
								}
							}
							else if("SAC Pwr".equalsIgnoreCase(measurementArray[i]))
							{
								if(!customLabel)
								{
									indexPower = i;
								}
							}
							else if("SAC kWh".equalsIgnoreCase(measurementArray[i]))
							{
								if(!customLabel)
								{
									indexEnergy = i;
								}
							}
							else if(labelTemperature.equalsIgnoreCase(measurementArray[i]))
							{
								indexTemperature = i;
							}
							else if(labelVoltage.equalsIgnoreCase(measurementArray[i]))
							{
								indexVoltage = i;
							}
							else if(labelV7 != null && labelV7.equalsIgnoreCase(measurementArray[i]))
							{
								indexV7 = i;
							}
							else if(labelV8 != null && labelV8.equalsIgnoreCase(measurementArray[i]))
							{
								indexV8 = i;
							}
							else if(labelV9 != null && labelV9.equalsIgnoreCase(measurementArray[i]))
							{
								indexV9 = i;
							}
							else if(labelV10 != null && labelV10.equalsIgnoreCase(measurementArray[i]))
							{
								indexV10 = i;
							}
							else if(labelV11 != null && labelV11.equalsIgnoreCase(measurementArray[i]))
							{
								indexV11 = i;
							}
							else if(labelV12 != null && labelV12.equalsIgnoreCase(measurementArray[i]))
							{
								indexV12 = i;
							}
						}
						
						if(!validateLabels())
						{
							return;
						}

                        startRead = true;
					}
				}
				else
				{
					String[] data = line.split("\\s+");

                    if(data.length >= columns)
                    {
                    	try
                    	{
                    		Date datetime = logdtf.parse(data[indexDate] + " " + data[indexTime]);
                    		
                    		add(data
                    			, datetime
                    			, data[indexEnergy]
                    			, data[indexPower]
                    			, indexTemperature > -1 ? data[indexTemperature] : null
                    			, indexVoltage > -1 ? data[indexVoltage] : null
                    			, "0");
                    	}
                    	catch(ParseException e)
                    	{
                    		continue;
                    	}
                    	
                        if(date == null)
                        {
                            date = logFileDateFormat.parse(data[indexDate]);
                            graphSubtitle = df.format(date);
                        }
                    }
				}
			}
		}
		catch(Exception e)
		{
			throw e;
		}
        finally
        {
            if(br != null) try{br.close();}catch(Exception e){}
        }
    }

}
