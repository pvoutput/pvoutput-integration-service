/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.pvoutput.integration.reader.SimpleLogReader;
import org.pvoutput.integration.util.Constant;

public class Sensor
{
	private static Logger LOGGER = Logger.getLogger(Sensor.class);
	
	public int sensor = 0;
	public int id = 0;
	public Date lastTime = new Date();
	public Date startTime = new Date();
	public int direction = ADataLogReader.IMPORT;
	public double calibration = 1.0;
	public final SortedMap<Date,Output> readings = Collections.synchronizedSortedMap(new TreeMap<Date,Output>());
	public File currentFile = null;
	public double energy = 0;
	public BufferedWriter bw;
	public Set<Integer> channelSet = new HashSet<Integer>();
	
	private NumberFormat nfkwh = NumberFormat.getNumberInstance(Locale.US);
	private NumberFormat nftemp = NumberFormat.getNumberInstance(Locale.US);
	private NumberFormat nfwatts = NumberFormat.getNumberInstance(Locale.US);
	private SimpleDateFormat tf = new SimpleDateFormat("HH:mm:ss");
	private SimpleDateFormat isodf = new SimpleDateFormat("yyyyMMdd");
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	
	private File dir = null;
	private String prefix = null;
	private String delimiter = ";";
		
	public Sensor()
	{
		nfkwh.setMinimumFractionDigits(3);
		nfkwh.setMaximumFractionDigits(3);
		nfkwh.setGroupingUsed(false);

		nfwatts.setMinimumFractionDigits(0);
		nfwatts.setMaximumFractionDigits(0);
		nfwatts.setGroupingUsed(false);
		
		nftemp.setMinimumFractionDigits(1);
		nftemp.setMaximumFractionDigits(1);
		nftemp.setGroupingUsed(false);
	}
	
	public void close()
	{
		if(bw != null)
		{
			try
			{
				bw.close();
			}
			catch(Exception e){}
		}
	}
	
	public void open()
	throws IOException
	{
		bw = new BufferedWriter(new FileWriter(currentFile, true));
	}
	
	public String toString()
	{
		return "Id: " + id + ", sensor: " + sensor + ", channels: " + channelSet + ", direction: " + (direction==ADataLogReader.IMPORT?"Import":"Export") + ", calibration: " + calibration;
	}

	public void setChannels(String s) 
	{
		if(s != null && s.length() > 0)
		{
			String[] channel = s.split("\\+");
			
			if(channel != null)
			{
				for(String c : channel)
				{
					try
					{
						channelSet.add(Integer.parseInt(c));
					}
					catch(Exception e)
					{
					}
				}
			}
		}
	}
	
	public void add(Output o)
	{
		Calendar c = Calendar.getInstance();
		c.setTime(o.time);
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.SECOND, 0);
		
		o.time = c.getTime();
		
		synchronized(readings)
		{
			Output current = readings.get(c.getTime());
			
			if(current != null)
			{
				if(current.power > 0 && o.power > 0)
				{
					current.power = (int)Math.round((current.power + o.power) / 2.0);
				}
				else if(o.power > 0)
				{
					current.power = o.power;
				}
				
				if(current.powerImport > 0 && o.powerImport > 0)
				{
					current.powerImport = (int)Math.round((current.powerImport + o.powerImport) / 2.0);
				}
				else if(o.powerImport > 0)
				{
					current.powerImport = o.powerImport;
				}
				
				if(Util.validTemperature(current.temperature) && Util.validTemperature(o.temperature))
				{
					current.temperature = (current.temperature + o.temperature) / 2.0;
				}
				else if(Util.validTemperature(o.temperature))
				{
					current.temperature = o.temperature;
				}
				
				if(Util.validVoltage(current.voltage) && Util.validVoltage(o.voltage))
				{
					current.voltage = (current.voltage + o.voltage) / 2.0;
				}
				else if(Util.validVoltage(o.voltage))
				{
					current.voltage = o.voltage;
				}
				
				if(!Double.isNaN(current.v7) && !Double.isNaN(o.v7))
				{
					current.v7 = (int)Math.round((current.v7 + o.v7) / 2.0);
				}
				else if(!Double.isNaN(o.v7))
				{
					current.v7 = o.v7;
				}
				
				if(!Double.isNaN(current.v8) && !Double.isNaN(o.v8))
				{
					current.v8 = (int)Math.round((current.v8 + o.v8) / 2.0);
				}
				else if(!Double.isNaN(o.v8))
				{
					current.v8 = o.v8;
				}
				
				if(!Double.isNaN(current.v9) && !Double.isNaN(o.v9))
				{
					current.v9 = (int)Math.round((current.v9 + o.v9) / 2.0);
				}
				else if(!Double.isNaN(o.v9))
				{
					current.v9 = o.v9;
				}
				
				if(!Double.isNaN(current.v10) && !Double.isNaN(o.v10))
				{
					current.v10 = (int)Math.round((current.v10 + o.v10) / 2.0);
				}
				else if(!Double.isNaN(o.v10))
				{
					current.v10 = o.v10;
				}
				
				if(!Double.isNaN(current.v11) && !Double.isNaN(o.v11))
				{
					current.v11 = (int)Math.round((current.v11 + o.v11) / 2.0);
				}
				else if(!Double.isNaN(o.v11))
				{
					current.v11 = o.v11;
				}
				
				if(!Double.isNaN(current.v12) && !Double.isNaN(o.v12))
				{
					current.v12 = (int)Math.round((current.v12 + o.v12) / 2.0);
				}
				else if(!Double.isNaN(o.v12))
				{
					current.v12 = o.v12;
				}
			}
			else
			{
				readings.put(c.getTime(), o);
			}
		}
	}
		
	public void processReadings()
	{
		synchronized(readings)
		{
			int total = readings.size();
			int n = 0;
			
			Iterator<Map.Entry<Date,Output>> ireadings = readings.entrySet().iterator();

			while(ireadings.hasNext())
			{
				Map.Entry<Date,Output> entry = ireadings.next();
				
				Output output = entry.getValue();
				
				if(n < total-1)
				{
					try
					{
						save(output.time
								,output.power
								,-1
								,output.temperature
								,output.voltage
								,output.v7
								,output.v8
								,output.v9
								,output.v10
								,output.v11
								,output.v12);
					}
					catch(Exception e)
					{
						return;
					}
					
					ireadings.remove();
				}
				
				n++;
			}
		}
	}
	
	/**
	 * @deprecated
	 */
	public void process() 
	{
		long start = -1;
		long end = -1;
		int n = 0;
		Date lastDate = null;
		int total = 0;		
		double temperatureTotal = 0;
		int temperatureCount = 0;
		Date marker = null;

		int v7t = 0;
		int v7n = 0;
		int v8t = 0;
		int v8n = 0;
		int v9t = 0;
		int v9n = 0;
		int v10t = 0;
		int v10n = 0;
		int v11t = 0;
		int v11n = 0;
		int v12t = 0;
		int v12n = 0;
		
		synchronized(readings)
		{
			Iterator<Map.Entry<Date,Output>> ireadings = readings.entrySet().iterator();

			while(ireadings.hasNext())
			{
				Map.Entry<Date,Output> entry = ireadings.next();

				lastDate = entry.getKey();
				
				if(n == 0)
				{
					if(startTime != null)
					{
						start = startTime.getTime();
					}
					else
					{
						start = lastDate.getTime();
					}
				}

				Output output = entry.getValue();
				total += output.power;
				
				if(!Double.isNaN(output.v7))
				{
					v7t += output.v7;
					v7n++;
				}
				
				if(!Double.isNaN(output.v8))
				{
					v8t += output.v8;
					v8n++;
				}
				
				if(!Double.isNaN(output.v9))
				{
					v9t += output.v9;
					v9n++;
				}
				
				if(!Double.isNaN(output.v10))
				{
					v10t += output.v10;
					v10n++;
				}
				
				if(!Double.isNaN(output.v11))
				{
					v11t += output.v11;
					v11n++;
				}
				
				if(!Double.isNaN(output.v12))
				{
					v12t += output.v12;
					v12n++;
				}
				
				if(Util.validTemperature(output.temperature))
				{
					temperatureTotal += output.temperature;
					temperatureCount++;
				}
				
				n++;

				end = lastDate.getTime();

				long elapse = (end-start);

				if(elapse >= 60000)
				{					
					double power = total/(n+0.0);
					double temperature = temperatureCount > 0 ? (temperatureTotal/(temperatureCount+0.0)) : Constant.DEFAULT_TEMPERATURE;
					
					double v7 = Double.NaN;
					double v8 = Double.NaN;
					double v9 = Double.NaN;
					double v10 = Double.NaN;
					double v11 = Double.NaN;
					double v12 = Double.NaN;
					
					if(v7n > 0)
						v7 = v7t/(v7n+0.0);
					
					if(v8n > 0)
						v8 = v8t/(v8n+0.0);
					
					if(v9n > 0)
						v9 = v9t/(v9n+0.0);
					
					if(v10n > 0)
						v10 = v10t/(v10n+0.0);
					
					if(v11n > 0)
						v11 = v11t/(v11n+0.0);
					
					if(v12n > 0)
						v12 = v12t/(v12n+0.0);
					
					// limit calculation to 5 minutes
					if(elapse > 60000*Constant.ELAPSED_LIMIT)
					{
						double e = power/((60000/(60000*Constant.ELAPSED_LIMIT+0.0))*60000)*1000;
						
						LOGGER.warn("Elapsed time greater than " + Constant.ELAPSED_LIMIT + " minutes, elapse: " + nfkwh.format((elapse/60000.0)) + ", power: " + nfkwh.format(power) + ", energy: " + nfkwh.format(e));
						
						elapse = 60000*Constant.ELAPSED_LIMIT;
					}
					
					double energy = power/((60000/(elapse+0.0))*60000)*1000;
					this.energy += energy;

					if(LOGGER.isDebugEnabled())
					{
						LOGGER.debug("Sensor=" + sensor + ", power=" + power + ", energy=" + energy + ", total=" + energy + ", size=" + n + ", time=" + elapse + ", start=" + startTime + ", end=" + lastDate);
					}
					
					try
					{
						save(lastDate
								,power
								,energy
								,temperature
								,Constant.DEFAULT_VOLTAGE
								,v7
								,v8
								,v9
								,v10
								,v11
								,v12);
					}
					catch(Exception e)
					{
						return;
					}

					startTime = lastDate;
					total = 0;
					n = 0;
					
					v7t = 0;
					v7n = 0;
					v8t = 0;
					v8n = 0;
					v9t = 0;
					v9n = 0;
					v10t = 0;
					v10n = 0;
					v11t = 0;
					v11n = 0;
					v12t = 0;
					v12n = 0;
					
					temperatureCount = 0;
					temperatureTotal = 0;

					marker = lastDate;
				}
			}

			if(marker != null)
			{
				Iterator<Date> ireadings2 = readings.keySet().iterator();

				while(ireadings2.hasNext())
				{
					Date d = ireadings2.next();

					if(d.before(marker))
					{
						ireadings2.remove();
					}
					else
					{
						break;
					}
				}
			}
		}
	}
	
	private void save(Date time
			, double power
			, double current_energy
			, double temperature
			, double voltage
			, double v7
			, double v8
			, double v9
			, double v10
			, double v11
			, double v12)
	throws IOException
	{
		if(prepareFile(time))
		{
			energy = current_energy;
		}
		
		StringBuffer b = new StringBuffer();
		
		b.append(isodf.format(time)).append(delimiter)
		.append(tf.format(time)).append(delimiter);
		
		if(Util.validTemperature(temperature))
		{
			b.append(nftemp.format(temperature));
		}
		else
		{
			b.append(nftemp.format(Constant.DEFAULT_TEMPERATURE));
		}
		
		b.append(delimiter);
		
		if(Util.validVoltage(voltage))
		{
			b.append(nftemp.format(voltage));
		}
		else
		{
			b.append(nftemp.format(Constant.DEFAULT_VOLTAGE));
		}
		
		b.append(delimiter)
		.append(nfwatts.format(power))
		.append(delimiter)
		.append("-1")
		//.append(nfkwh.format(current_energy))
		.append(delimiter)
		//.append(nfkwh.format(energy))
		.append("-1")
		.append(delimiter);
		
		if(!Double.isNaN(v7))
		{
			b.append(nfkwh.format(v7));
		}
		else
		{
			b.append("NaN");
		}
		
		b.append(delimiter);
		
		if(!Double.isNaN(v8))
		{
			b.append(nfkwh.format(v8));
		}
		else
		{
			b.append("NaN");
		}
		
		b.append(delimiter);
		
		if(!Double.isNaN(v9))
		{
			b.append(nfkwh.format(v9));
		}
		else
		{
			b.append("NaN");
		}
		
		b.append(delimiter);
		
		if(!Double.isNaN(v10))
		{
			b.append(nfkwh.format(v10));
		}
		else
		{
			b.append("NaN");
		}
		
		b.append(delimiter);
		
		if(!Double.isNaN(v11))
		{
			b.append(nfkwh.format(v11));
		}
		else
		{
			b.append("NaN");
		}
		
		b.append(delimiter);
		
		if(!Double.isNaN(v12))
		{
			b.append(nfkwh.format(v12));
		}
		else
		{
			b.append("NaN");
		}
		
		bw.write(b.toString());
		
		bw.newLine();
		bw.flush();
	}
	
	private boolean prepareFile(Date logtime)
	{
		boolean newBuffer = false;
		boolean rollover = false;
		
		File f = getLogFile(id, logtime);
		
		if(currentFile == null)
		{
			newBuffer = true;
			currentFile = f;
		}
		else if(!currentFile.getName().equals(f.getName()))
		{
			newBuffer = true;
			currentFile = f;
			rollover = true;
		}
		
		if(newBuffer)
		{
			LOGGER.info("Logging to " + currentFile.getAbsolutePath());
			
			close();
		
			try
			{
				open();
				
				if(f.length() == 0)
				{
					bw.write(SimpleLogReader.HEADER);
					bw.newLine();
					bw.flush();
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Error", e);
			}
		}
		
		return rollover;
	}
	
	public void setDirectory(File dir)
	{
		this.dir = dir;
	}
	
	public Set<Integer> getChannels()
	{
		return channelSet;
	}
	
	public void setPrefix(String prefix)
	{
		this.prefix = prefix;
	}
	
	private File getLogFile(int sensorId, Date logtime)
	{
		return new File(dir, prefix + sensorId + "-" + df.format(logtime) + ".log");
	}
	
	public void init(int sensorsOut, int sensorsIn, double energyExport, double energyImport)
	{
		File f = getLogFile(id, new Date());
		
		long flen = f.length();
		String energyValue = "";
		String timeValue = "";
		boolean rename = false;
		
		if(f.exists() && flen > 0)
		{
			RandomAccessFile raf = null;
			byte[] b = new byte[1];
			
			try
			{
				raf = new RandomAccessFile(f, "r");
				int read = 0;
				StringBuffer line = new StringBuffer();
				
				for(long pos = flen-2; pos > 0; pos--)
				{
					raf.seek(pos);
					raf.read(b);
					String value = new String(b);
					
					if(Character.isLetterOrDigit(value.charAt(0))
							|| value.charAt(0) == '.'
							|| value.charAt(0) == ';'
							|| value.charAt(0) == ':'
							|| value.charAt(0) == '-')
					{
						line.insert(0, value);	
					}
					else if(line.length() > 0)
					{
						break;
					}
					
					read++;
					
					if(read > 100)
					{
						break;
					}
				}
			
				if(line.length() > 0)
				{
					LOGGER.info(f.getName() + " Status: " + line);
					
					String[] values = line.toString().split(";");
					
					if(values.length >= 5)
					{
						if(values.length >= 7)
						{
							// "DATE;TIME;TEMPERATURE;VOLTAGE;POWER;ENERGY;TOTAL;V7;V8;V9;V10;V11;V12"
							// "DATE;TIME;TEMPERATURE;VOLTAGE;POWER;ENERGY;TOTAL"
							energyValue = values[6];
						}
						else if(values.length == 6)
						{
							// "DATE;TIME;TEMPERATURE;POWER;ENERGY;TOTAL"
							energyValue = values[5];
							
							rename = true;
						}
						else if(values.length == 5)
						{
							// "DATE;TIME;POWER;ENERGY;TOTAL"
							energyValue = values[4];
							
							rename = true;
						}
						
						timeValue = values[0] + " " + values[1];
					}
				}
				else
				{
					LOGGER.info(f.getName() + " Status: [empty]");
				}
			}
			catch(Exception e)
			{
				LOGGER.error("Could not read " + f.getAbsolutePath(), e);
			}
			finally
			{
				try
				{
					raf.close();
				}
				catch(Exception e)
				{
					
				}
			}
			
			// rename old log file format
			if(rename)
			{
				File newFile = new File(f.getParentFile(), f.getName() + "." + System.currentTimeMillis() + ".bak");
				
				f.renameTo(newFile);
			}
			
			if(energyValue.length() > 0)
			{
				try
				{
					energy = Double.parseDouble(energyValue.toString());
					
					LOGGER.info(f.getName() + ", Last Energy: " + energy);
				}
				catch(Exception e)
				{
					
				}
			}
		}
				
		if(timeValue != null && timeValue.length() > 0)
		{
			try
			{
				startTime = dtf.parse(timeValue);
				
				LOGGER.info("Start Time " + id + ": " + dtf.format(startTime));
			}
			catch(Exception e)
			{
				startTime = new Date();
			}
		}
	}
}
