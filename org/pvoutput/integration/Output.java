/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.pvoutput.integration.util.Constant;

public class Output
{	
	public double energy_change = 0;
	public double energy_lifetime = 0;
    public Date time;
    public int imports = 0;
    public int exports = 0;
    
    public double energy = 0;
    public int power = 0;
    public double temperature = Constant.DEFAULT_TEMPERATURE;
    public double voltage = Constant.DEFAULT_VOLTAGE;
    public double energyImport = 0;
    public int powerImport = 0;
    public boolean sent = false;
    public int errors = 0;
    public boolean useEnergy = false;
    public Set<String> inverters = new HashSet<String>();
    
    public double v7 = Double.NaN;
    public double v8 = Double.NaN;
    public double v9 = Double.NaN;
    public double v10 = Double.NaN;
    public double v11 = Double.NaN;
    public double v12 = Double.NaN;
    
    public String toString()
    {
    	StringBuffer s = new StringBuffer();
    	
    	s.append("v1=").append(energy).append(",v2=").append(power).append(",v3=").append(energyImport).append(",v4=").append(powerImport);
    	s.append(",temp=").append(temperature).append(",volt=").append(voltage);
    	s.append(",v7=").append(v7).append(",v8=").append(v8).append(",v9=").append(v9).append(",v10=").append(v10)
    	.append(",v11=").append(v11).append(",v12=").append(v12);
    	
    	
    	return s.toString();
    }
    
}
