/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.util.ArrayList;
import java.util.List;

import org.pvoutput.integration.Output;
import org.pvoutput.integration.Sensor;
import org.pvoutput.integration.Util;


public class Ted5000LogReader extends AHttpLogReader
{
	public void startup()
	throws Exception
	{
		super.startup();

		// allow logs to start with non-zero energy data
		checkNonZeroEnergyStart = false;
		checkDecreasingEnergy = true;

		Ted5000FileWriter w = new Ted5000FileWriter();
		w.start();
	}
	
	public String getName()
	{
		return "ted5000";
	}
	
	public String getLogFilePrefix()
	{
		return "TED";
	}
	
	public AHttpLogReader copy()
	{
		return new Ted5000LogReader();
	}
	
	private class Ted5000FileWriter extends ASimpleLogFileWriter
	{				
		public List<Output> parse(Sensor sensor, String s)
		{
			List<Output> outputs = null;
			
			int total = 0;

			if(sensor.getChannels().size() > 0)
			{
				for(int channel : sensor.getChannels())
				{
					String sPower = Util.getElement(s, "Power");
					
					if(sPower != null)
					{
						String sTotal = null;
						
						if(channel == 0)
						{
							sTotal = Util.getElement(sPower, "Total");
						}
						else
						{
							sTotal = Util.getElement(sPower, "MTU" + String.valueOf(channel));
						}
						
						if(sTotal != null)
						{
							String powerNow = Util.getElement(sTotal, "PowerNow");
							
							total += Math.abs(Util.getNumber(powerNow, 0));
						}
					}
				}
				
				Output output = new Output();
				output.power = total;
				
				outputs = new ArrayList<Output>();
				outputs.add(output);
			}
			else
			{
				String sPower = Util.getElement(s, "Power");
				
				if(sPower != null)
				{
					String sTotal = Util.getElement(sPower, "Total");
					
					if(sTotal != null)
					{
						String powerNow = Util.getElement(sTotal, "PowerNow");
						
						total += Math.abs(Util.getNumber(powerNow, 0));
					}
				}
				
				Output output = new Output();
				output.power = total;
				
				outputs = new ArrayList<Output>();
				outputs.add(output);
			}
			
			return outputs;
		}
	}
}
