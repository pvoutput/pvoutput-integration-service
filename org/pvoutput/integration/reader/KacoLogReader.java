/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.Util;


public class KacoLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(KacoLogReader.class);
		
	private SimpleDateFormat tf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	private int posDate = 0;
	private int posTime = 1;
	private int posPower = 8;
	private int posTemperature = 9;
	private SimpleDateFormat logdf = new SimpleDateFormat("yyyy/yyyyMM/yyyyMMdd");
	
	public void startup()
	throws Exception
	{
		super.startup();
		
		String s = properties.getProperty("position.power");

		if(s != null && s.length() > 0)
		{
			posPower = Util.getNumber(s);
			
			LOGGER.info("Position Power " + posPower + ": '" + indexPower + "'");
		}

		s = properties.getProperty("position.date");

		if(s != null && s.length() > 0)
		{
			posDate = Util.getNumber(s);
			
			LOGGER.info("Position Date " + posDate + ": '" + indexDate + "'");
		}

		s = properties.getProperty("position.time");

		if(s != null && s.length() > 0)
		{
			posTime = Util.getNumber(s);
			
			LOGGER.info("Position Time " + posTime + ": '" + indexTime + "'");
		}
		
		s = properties.getProperty("position.temperature");

		if(s != null && s.length() > 0)
		{
			posTemperature = Util.getNumber(s);
			
			LOGGER.info("Position Temperature " + posTemperature + ": '" + indexTemperature + "'");
		}
		
		if(posDate == -1)
		{
			throw new Exception("Date position not configured correctly in kaco.ini");
		}
		else if(posTime == -1)
		{
			throw new Exception("Time position not configured correctly in kaco.ini");
		}
		else if(posPower == -1)
		{
			throw new Exception("Power position not configured correctly in kaco.ini");
		}		
	}
        	
	@Override
	public void get()
	{
		logEntries.clear();
		
		int added = 0;
		double total = 0;
		Calendar c = Calendar.getInstance();
		BufferedReader br = null;
		java.util.Date lastDate = null;
		Set<Date> fileEntries = new HashSet<Date>();
		
		try
		{
			File file = new File(getDirectory(), logdf.format(c.getTime()) + ".txt");
			
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Reading: " + file.getAbsolutePath());
			}
			
			if(!file.exists())
			{
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.warn("Missing file: " + file.getAbsolutePath());
				}
				
				return;
			}
						
			br = new BufferedReader(new FileReader(file));
			String line;
			
			while((line = br.readLine()) != null)
			{
				String[] data = line.split("\\s+");
				
				int power = (int)Math.round(Util.getDouble(data[posPower], 0));
				
                double energy = 0;
                long elapse = -1;
                
                java.util.Date currentDate = tf.parse(data[posDate] + " " + data[posTime]);
	                
                if(lastDate != null)
                {
                    elapse = currentDate.getTime() - lastDate.getTime();
                }

                if(elapse > 0)
                {
                    energy = power/((60000/(elapse+0.0))*60000)*1000;
                    total += energy;
                }
                
                c.setTimeInMillis(currentDate.getTime());
				c.set(Calendar.MILLISECOND, 0);
				
				if(!fileEntries.contains(c.getTime()))
				{
					fileEntries.add(c.getTime());
				}
				else
				{
					continue;
				}
				
				Output o = null;
				
				if(logEntries.containsKey(c.getTime()))
				{
					o = logEntries.get(c.getTime());
				}
				else
				{
					o = new Output();
					o.time = c.getTime();
					o.power = 0;
					o.energy = 0;
	
					logEntries.put(c.getTime(), o);
				}

				o.power += power;
				o.energy += Math.round(total);
				
				if(posTemperature > -1)
				{
					o.temperature = Util.getDouble(data[posTemperature]);
				}
				
				added++;

                lastDate = currentDate;
			}

			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Added " + added + " records from " + file);
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Could not read data", e);
		}
        finally
        {
            if(br != null)
            {
	            try 
	            {
	            	br.close();
	            }
	            catch(Exception e)
	            {
	            }
            }
        }
		
		if(added > 0)
		{
			addLog(c);
		}
	}

	@Override
	public String getLogFilename()
	{
        StringBuffer s = new StringBuffer();
        
        s.append("KC").append("-").append(logfileDateFormat.format(new Date())).append(".log");
        
        return s.toString();
        
	}
	
	@Override
    public File getLogFile()
    {
        return new File(dir, "KC" + "-" + logfileDateFormat.format(new Date()) + ".log");
    }

}
