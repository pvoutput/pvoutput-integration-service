/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.Util;
import org.pvoutput.integration.util.Constant;


public class SuntelliteLogReader extends SimpleLogReader
{
	private static Logger LOGGER = Logger.getLogger(SuntelliteLogReader.class);

	private String[] filenames = null;
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-M-d");
	private SimpleDateFormat tf = new SimpleDateFormat("yyyy-M-d H:mm");

	@Override
	public void startup()
	throws Exception
	{
		super.startup();

		// allow logs to start with non-zero energy data
		checkNonZeroEnergyStart = false;
		checkDecreasingEnergy = false;
		
		if(properties.containsKey("filename"))
		{
			String s = (String)properties.get("filename");

			filenames = s.split(",");
		}
		else
		{
			LOGGER.error("The 'filename' value not set in suntellite.ini");

			filenames = new String[]{"NONE.CSV"};
		}

		if(properties.containsKey("dir-date-format"))
		{
			String s = (String)properties.get("dir-date-format");

			df.applyPattern(s);
		}

		if(properties.containsKey("data-time-format"))
		{
			String s = (String)properties.get("data-time-format");

			tf.applyPattern(s);

			LOGGER.info("Date time format: " + s);
		}

		int n = 1;

		for(String filename : filenames)
		{
			File f = new File(dir + "/" + df.format(new java.util.Date()) + "/" + filename);

			LOGGER.info("CSV " + n + ": " + f.getAbsolutePath());

			n++;
		}
	}

	public void setSourceHeaders()
	{
		super.setSourceHeaders();
		
		if(columnNameTime == null)
			columnNameTime = "Time";

		if(columnNameEnergy == null)
			columnNameEnergy = "Daily Energy";

		if(columnNamePower == null)
			columnNamePower = "Output Power";

		if(columnNameTemperature == null)
			columnNameTemperature = "Temperature";

		if(columnNameVoltage== null)
			columnNameVoltage = "Grid Voltage";
	}

	@Override
	public void get()
	{
		logEntries.clear();

		int added = 0;

		Calendar c = Calendar.getInstance();

		for(String filename : filenames)
		{
			Set<Date> fileEntries = new HashSet<Date>();

			File f = new File(dir + "/" + df.format(new java.util.Date()) + "/" + filename);

			if(f.exists())
			{
				BufferedReader br = null;

				try
				{
					br = new BufferedReader(new FileReader(f));

					String line;

					boolean startRead = false;
					int columns = 0;

					while((line = br.readLine()) != null)
					{
						if(!startRead)
						{
							String[] measurementArray = line.split(getDelimiter());

							columns = measurementArray.length;

							for(int i = 0; i < columns; i++)
							{
								if(columnNameEnergy.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexEnergy = i;
								}
								else if(columnNamePower.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexPower = i;
								}
								else if(columnNameTime.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexTime = i;
								}
								else if(columnNameTemperature.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexTemperature = i;
								}
								else if(columnNameVoltage.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexVoltage = i;
								}
								else if(columnNameV7 != null && columnNameV7.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexV7 = i;
								}
								else if(columnNameV8 != null && columnNameV8.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexV8 = i;
								}
								else if(columnNameV9 != null && columnNameV9.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexV9 = i;
								}
								else if(columnNameV10 != null && columnNameV10.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexV10 = i;
								}
								else if(columnNameV11 != null && columnNameV11.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexV11 = i;
								}
								else if(columnNameV12 != null && columnNameV12.equalsIgnoreCase(measurementArray[i]))
								{
									columnIndexV12 = i;
								}
							}

							// no date, set to 0 to pass validation
							columnIndexDate = 0;

							if(!validateHeaders())
							{
								return;
							}

							startRead = true;
						}
						else
						{
							String[] data = line.split(getDelimiter());

							if(data.length >= columns)
							{
								int offset = data[columnIndexTime].lastIndexOf(':');

								// remove trailing characters
								if(offset > 0)
								{
									data[columnIndexTime] = data[columnIndexTime].substring(0, offset);

									Date time = null;

									try
									{
										time = tf.parse(data[columnIndexTime]);
									}
									catch(Exception e)
									{
										if(LOGGER.isDebugEnabled())
										{
											LOGGER.debug("Invalid time format: [" + data[columnIndexTime] + "] does not match " + tf.toPattern());
										}
										
										continue;
									}

									try
									{							
										c.setTime(time);
										c.set(Calendar.SECOND, 0);
										c.set(Calendar.MILLISECOND, 0);

										if(!fileEntries.contains(c.getTime()))
										{
											fileEntries.add(c.getTime());
										}
										else
										{
											continue;
										}

										Output o = null;

										if(logEntries.containsKey(c.getTime()))
										{
											o = logEntries.get(c.getTime());
										}
										else
										{
											o = new Output();
											o.time = c.getTime();
											o.power = 0;
											o.energy = 0;

											logEntries.put(c.getTime(), o);
										}

										if(columnIndexTemperature > -1 && o.energy == 0)
										{
											o.temperature = Util.getDouble(data[columnIndexTemperature], Constant.DEFAULT_TEMPERATURE);
										}

										if(columnIndexVoltage > 0 && o.energy == 0)
										{
											o.voltage = Util.getDouble(data[columnIndexVoltage], Constant.DEFAULT_VOLTAGE);
										}

										o.power += Double.parseDouble(data[columnIndexPower]);
										o.energy += Double.parseDouble(data[columnIndexEnergy]) * 1000;

										added++;
									}
									catch(Exception e)
									{
										LOGGER.error("Could not add output record", e);
									}
								}
							}
						}
					}
				}
				catch(Exception e)
				{
					LOGGER.error("Could not read data", e);
				}
				finally
				{
					if(br != null)
					{
						try
						{
							br.close();
						}
						catch(Exception e)
						{
							LOGGER.error("Could not close file", e);
						}
					}
				}

				if(LOGGER.isDebugEnabled())
				{
					LOGGER.debug("Added " + added + " records from " + f.getAbsolutePath());
				}
			}
			else
			{
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.warn("CSV file " + f.getAbsolutePath() + " not found");
				}
			}
		}

		if(added > 0)
		{
			addLog(c);			
		}
	} 

	@Override
	public String getLogFilename()
	{
        StringBuffer s = new StringBuffer();
        
        s.append("ST").append("-").append(df.format(new Date())).append(".log");
        
        return s.toString();
        
	}

	@Override
	public File getLogFile()
	{
		return new File(dir, "ST" + "-" + df.format(new Date()) + ".log");
	}
}
