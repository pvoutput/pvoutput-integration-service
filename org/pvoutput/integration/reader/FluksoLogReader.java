/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.pvoutput.integration.Output;
import org.pvoutput.integration.Sensor;


public class FluksoLogReader extends AHttpLogReader
{	
	private static Logger LOGGER = Logger.getLogger(FluksoLogReader.class);

	public void startup()
	throws Exception
	{
		super.startup();

		// allow logs to start with non-zero energy data
		checkNonZeroEnergyStart = false;
		checkDecreasingEnergy = true;

		String version = (String)properties.get("version");
		String token = (String)properties.get("token");
		
		httpHeaders.put("X-Version", version);
		httpHeaders.put("X-Token", token);
		
		FluksoFileWriter w = new FluksoFileWriter();
		w.start();
	}
	
	public String getName()
	{
		return "flukso";
	}
	
	public AHttpLogReader copy()
	{
		return new FluksoLogReader();
	}
	
	public String getLogFilePrefix()
	{
		return "FKS";
	}
	
	@Override
	public int getDefaultDirection() 
	{
		return IMPORT;
	}
	
	private class FluksoFileWriter extends SimpleLogFileWriter
	{
		public List<Output> parse(Sensor sensor, String s)
		{
			List<Output> outputs = new ArrayList<Output>();

			if(LOGGER.isDebugEnabled())
			{
				LOGGER.info("<<< " + s);
			}
			
			JSONArray jsonArray = (JSONArray)JSONValue.parse(s);
			
			if(jsonArray != null)
			{
				Iterator i = jsonArray.iterator();
				
				while(i.hasNext())
				{
					Object o = i.next();
					
					if(o instanceof JSONArray)
					{
						JSONArray pair = (JSONArray)o;
						
						if(pair.size() == 2)
						{
							try
							{
								Object timestamp = pair.get(0);
								Object opowr = pair.get(1);
								
								if(timestamp instanceof Long)
								{
									Calendar c = Calendar.getInstance();
									c.setTimeInMillis(((Long)timestamp).longValue() * 1000);
		
									if(opowr instanceof Long)
									{
										Output output = new Output();
										output.power = ((Long)opowr).intValue();
										output.energy_change = (Long)opowr/60.0;
										output.time = c.getTime();
										
										outputs.add(output);
									}
									else if(opowr instanceof Double)
									{
										Output output = new Output();
										output.power = ((Double)opowr).intValue();
										output.energy_change = (Double)opowr/60.0;
										output.time = c.getTime();
										
										outputs.add(output);
									}
								}
							}
							catch(Exception e)
							{
								LOGGER.error("Could not read data [" + pair.toJSONString() + "] [" + e.getMessage() + "]", e);
							}
						}
					}
				}
			}
			else
			{
				LOGGER.error("Could not read data [" + s + "]");
			}
			
			return outputs;
		}
	}
}
