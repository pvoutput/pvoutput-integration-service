/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.pvoutput.integration.ADataLogReader;


public class AuroraLogReader extends ADataLogReader
{
	private static Logger LOGGER = Logger.getLogger(AuroraLogReader.class);
	
	private String labelMeasurements = "measurements";
	private String labelDate = "Date";
	private String labelAddress = "Address";
	private String labelModel = "Model";
	private String formatTime = "HH:mm";
	private String formatDecimal = ".";
	
	@Override
	public String getLogFilename()
	{
		String pattern = "yyyy-MM-dd";
		
		String name = getLogFilename(pattern);
		
		if(name != null)
		{
			return name;
		}
		
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(new java.util.Date()) + ".log";
	}
	
	@Override
    public File getLogFile()
    {
    	String pattern = "yyyy-MM-dd";
    	
    	File file = getLogFile(pattern);
    	
    	if(file != null)
    	{
    		return file;
    	}

        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return new File(dir, df.format(new java.util.Date()) + ".log");
    }
	
	@Override
	public void startup()
	{
		checkNonZeroEnergyStart = true;
		checkDecreasingEnergy = true;
		
		String s = properties.getProperty("label.measurements");
		
		if(s != null && s.length() > 0)
		{
			labelMeasurements = s;
			
			LOGGER.info("Label Measurements: " + s);
		}
		
		s = properties.getProperty("label.date");
		
		if(s != null && s.length() > 0)
		{
			labelDate = s;
			
			LOGGER.info("Label Date: " + s);
		}
		
		s = properties.getProperty("label.address");
		
		if(s != null && s.length() > 0)
		{
			labelAddress = s;
			
			LOGGER.info("Label Address: " + s);
		}
		
		s = properties.getProperty("label.model");
		
		if(s != null && s.length() > 0)
		{
			labelModel = s;
			
			LOGGER.info("Label Model: " + s);
		}
		
		s = properties.getProperty("format.time");
		
		if(s != null && s.length() > 0)
		{
			formatTime = s;
			
			LOGGER.info("Time Format: " + s);
		}
		
		s = properties.getProperty("format.decimal");
		
		if(s != null && s.length() > 0)
		{
			formatDecimal = s;
			
			LOGGER.info("Decimal Format: " + s);
		}
	}
	
	@Override
	public void shutdown()
	{	
	}
	
	@Override
	public void get()
	{
	}
	
    @Override
    public String getLogDateFormat()
    {
        return "dd/MM/yyyy";
    }

    @Override
    public String getLogTimeFormat()
    {
        return formatTime;
    }
    
    @Override
	public void setLabels()
	{
        if(labelTime == null)
        	labelTime = "Time";
        
        if(labelEnergy == null)
        	labelEnergy = "ENERGY";
        
        if(labelPower == null)
        	labelPower = "PAC";
        
        if(labelTemperature == null)
        	labelTemperature = "TINV";
        
        if(labelVoltage == null)
        	labelVoltage = "VAC";
	}

	@Override
	public String getDelimiter()
	{
		return ";";
	}
		
    @Override
    public void read() throws Exception
    {
    	initReader();
        
		BufferedReader br = null;

		try
		{
			br = new BufferedReader(new FileReader(logfile));
			String line;
			boolean startRead = false;
			int addressIndex = -1;
            int maxIndex = 0;
            int modelIndex = -1;
			indexDate = 0;

			while((line = br.readLine()) != null)
			{
				if(!startRead)
				{
					if(line.startsWith(labelDate + ": "))
					{
						graphSubtitle = line.substring((labelDate + ": ").length());

						try
						{
							date = logFileDateFormat.parse(graphSubtitle);
						}
						catch(Exception e)
						{
							date = new java.util.Date();
						}
					}
					else if(line.startsWith("[" + labelMeasurements + "]"))
					{
						String measurements = br.readLine();

						String[] measurementArray = measurements.split(getDelimiter());

                        int columns = measurementArray.length;

						for(int i = 0; i < columns; i++)
						{
							if(labelEnergy.equalsIgnoreCase(measurementArray[i]))
							{
								indexEnergy = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelPower.equalsIgnoreCase(measurementArray[i]))
							{
								indexPower = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelTime.equalsIgnoreCase(measurementArray[i]))
							{
								indexTime = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelTemperature.equalsIgnoreCase(measurementArray[i]))
							{
								indexTemperature = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelVoltage.equalsIgnoreCase(measurementArray[i]))
							{
								indexVoltage = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelAddress.equalsIgnoreCase(measurementArray[i]))
							{
								addressIndex = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelModel.equalsIgnoreCase(measurementArray[i]))
							{
								modelIndex = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelV7 != null && labelV7.equalsIgnoreCase(measurementArray[i]))
							{
								indexV7 = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelV8 != null && labelV8.equalsIgnoreCase(measurementArray[i]))
							{
								indexV8 = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelV9 != null && labelV9.equalsIgnoreCase(measurementArray[i]))
							{
								indexV9 = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelV10 != null && labelV10.equalsIgnoreCase(measurementArray[i]))
							{
								indexV10 = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelV11 != null && labelV11.equalsIgnoreCase(measurementArray[i]))
							{
								indexV11 = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
							else if(labelV12 != null && labelV12.equalsIgnoreCase(measurementArray[i]))
							{
								indexV12 = i;
								
								if(i > maxIndex)
									maxIndex = i;
							}
						}

						if(!validateLabels())
						{
							return;
						}

                        startRead = true;
					}
				}
				else
				{
					String[] data = line.split(getDelimiter());

                    if(data.length >= maxIndex)
                    {   
                    	if(modelIndex > -1)
                    	{
	                    	if("PVI-2000-AU".equals(data[modelIndex]))
	                		{
	                    		setSystemSize((int)Math.round(2000*1.05));
	                		}
	                		else if("PVI-3.6-OUTD-AU".equals(data[modelIndex])
	                				|| "PVI-3600-AU".equals(data[modelIndex]))
	                		{
	                			setSystemSize((int)Math.round(3600*1.05));
	                		}
	                		else if("PVI-6000-OUTD-AU".equals(data[modelIndex]))
	                		{
	                			setSystemSize((int)Math.round(6000*1.05));
	                		}
                    	}
                    	
                    	if(".".equals(formatDecimal))
                    	{
                    	    add(data, data[indexTime]
                    	    		, data[indexEnergy]
                    	    		, data[indexPower]
                    	    		, indexTemperature > -1 ? data[indexTemperature] : null
                    	    		, indexVoltage > -1 ? data[indexVoltage] : null
                    	    		, data[addressIndex]
                    	    		, '.');
                    	}
                    	else
                    	{
                    		add(data, data[indexTime]
                    				, data[indexEnergy].replace(formatDecimal.charAt(0), '.')
                    				, data[indexPower].replace(formatDecimal.charAt(0), '.')
                    				, indexTemperature > -1 ? data[indexTemperature].replace(formatDecimal.charAt(0), '.') : null
                    				, indexVoltage > -1 ? data[indexVoltage].replace(formatDecimal.charAt(0), '.') : null
                    				, data[addressIndex]
                    				, formatDecimal.charAt(0));
                    	}
                    }
				}
			}
		}
		catch(Exception e)
		{
			throw e;
		}
        finally
        {
            if(br != null)
            {
	            try 
	            {
	            	br.close();
	            }
	            catch(Exception e)
	            {
	            }
            }
        }
	}
}
