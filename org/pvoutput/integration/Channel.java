/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration;

import java.util.HashMap;
import java.util.Map;

public class Channel 
{
	public boolean positive = true;	
	public int channel = -1;
	
	public static Map<String, Channel> parse(String mtus) 
	{
		if(mtus != null && mtus.length() > 0)
		{
			Map<String,Channel> map = new HashMap<String,Channel>();
			
			String[] s = mtus.split("\\+|\\-");
			
			if(s != null)
			{
				for(String v : s)
				{
					if(v.length() > 0)
					{
						Channel o = new Channel();
						
						try
						{
							o.channel = Integer.parseInt(v);
						}
						catch(Exception e)
						{
							
						}
						
						if(mtus.indexOf("-" + v) > -1)
						{	
							o.positive = false;
						}
						
						map.put(v,  o);
					}
				}
			}
			
			return map;
		}
		
		return null;
	}
}
