/*
 * Copyright 2011 http://pvoutput.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pvoutput.integration.console;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.pvoutput.integration.Main;

public class ConfigurationPageHandler extends AbstractHandler
{
	private static Logger LOGGER = Logger.getLogger(ConfigurationPageHandler.class);
	
	private File installDir;
	
	public ConfigurationPageHandler(File installDir)
	{
		this.installDir = installDir;
	}
	
	private String getStyle()
	{
		StringBuffer s = new StringBuffer();
		
		s.append("body{font-family: 'Lucida Grande', Arial; font-size: 13px; width: 800px; margin-top: 0px; margin-left: 15px; margin-right:10px; margin-bottom: 10px;}");
		s.append("\n label {font-family:  Arial; font-size: 12px; font-weight: normal;}");
		s.append("\n input[type=\"text\"]{padding: 2px;margin: 1px;border:1px solid #7f9db9;}");
		s.append("\n tr.p1 td { background-color: #FCCDE5; padding-right: 8px; padding-top: 2px; padding-bottom: 2px; border-top: 1px solid #d2d2c1; border-bottom: 1px solid #d2d2c1;}");
		s.append("\n tr.p0 td { background-color: #FFFFB3; padding-right: 8px; padding-top: 2px; padding-bottom: 2px; border-top: 1px solid #d2d2c1; border-bottom: 1px solid #d2d2c1;}");
		s.append("\n tr.o td { background-color: #EFFBEF; padding-right: 8px; padding-top: 3px; padding-bottom: 3px;}");
		s.append("\n tr.e td { background-color: white; padding-right: 8px; padding-top: 3px; padding-bottom: 3px;}");
		s.append("\n tr.none th {background-color: white; padding-right: 10px; padding-top: 5px; padding-bottom: 5px;}");
		s.append("\n th { padding-right: 12px; padding-left:4px; background-color: #E0ECF8; padding-top: 5px; padding-bottom: 5px; border-top: 1px solid #99f;}");
		s.append("\n td { padding-left: 4px; padding-right: 5px; padding-top: 1px; padding-bottom: 3px;}");
		s.append("\n table {border-collapse: collapse; padding-top: 0px; font-size: 13px; -moz-border-radius: 0px 0px 0px 0px;}");
		
		s.append("\n p {margin-top: 3px; margin-bottom: 6px;}");
		s.append("\n .s2{font-size: 1.20em;color:#000000;font-weight: bold;margin-top: 30px;margin-bottom: 10px;padding: 2px;background: #E0ECF8;border-bottom: 1px solid #99f;}");
		s.append("\n small.comment {color:#777777; padding-left: 5px; padding-top: 0px; padding-bottom: 8px; font-family: Arial, sans-serif;}");
		
		s.append("\n p.msginfo-fit {font-size: 13px;margin-top: 5px;margin-bottom: 5px;padding: 5px;border: 1px dotted #EC7014;background:#FFF7BC;}");
		
		return s.toString();
	}
	
	private String writeOption(String value, String name, String currentValue)
	{
		StringBuffer s = new StringBuffer();
		
		s.append("<option value='").append(value).append("'");
		
		if(currentValue != null && currentValue.equals(value))
		{
			s.append(" selected");
		}
		
		s.append(">").append(name).append("</option>");
		
		return s.toString();
	}
	
	private String getProperty(String key, Properties prop, String _default)
	{
		String v = prop.getProperty(key);
		
		if(v == null)
			return _default;
		
		return v;
	}
	
	@Override
	public void handle(String arg0, Request arg1, HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException 
	{
		response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        
        PrintWriter out = response.getWriter();
        
        File conf = new File(installDir, "conf/pvoutput.ini");
        
        out.println("<html><head><style type='text/css'>" + getStyle() + "</style></head><body>");
        
        if(conf.exists())
		{
			String action = request.getParameter("action");
			 
			Properties prop = new Properties();
			
			// load hidden values
			FileReader fr = null;
			
			try
			{
				fr = new FileReader(conf);
				prop.load(fr);
			}
			catch(Exception e)
			{
				LOGGER.error("Could not read config file", e);
			}
			finally
			{
				if(fr != null)
				{
					fr.close();
				}
			}
			
			boolean saved = false;
			
			if("save".equals(action))
			{
			 	LOGGER.info("Saving configuration data");
			
				prop.put("format", request.getParameter("format"));
				prop.put("sid", request.getParameter("sid"));
				prop.put("key", request.getParameter("key"));
				prop.put("dir", request.getParameter("dir"));
				
				String s = request.getParameter("direction");
				
				if(s == null || s.length() == 0)
				{
					prop.remove("direction");
				}
				else
				{
					prop.put("direction", s);
				}
				
				s = request.getParameter("timezone");
				
				if(s == null || s.length() == 0)
				{
					prop.remove("timezone");
				}
				else
				{
					prop.put("timezone", s);
				}
				
				s = request.getParameter("endtime");
				
				if(s == null || s.length() == 0)
				{
					prop.remove("endtime");
				}
				else
				{
					prop.put("endtime", s);
				}
				
			 	ConfigurationSaver saver = new ConfigurationSaver(conf);
			 	
			 	if(saver.save(prop))
			 	{
			 		saved = true;
			 	}
			}

			String key = getProperty("key", prop, "xxxxxxxxxxxxxxx");
			String sid = getProperty("sid", prop, "0");
			String dir = getProperty("dir", prop, "c:/logs");
			String format = getProperty("format", prop, "simple");
			String endtime = getProperty("endtime", prop, "19:30");
			String timezone = getProperty("timezone", prop, "");
			String direction = getProperty("direction", prop, "");
			
			
			
			out.println("<p><table><form method='post'>");
			
			if(saved)
			{
				out.println("<tr><td colspan=2><p class='msginfo-fit'>Successfully saved configuration, a service restart is required</p></td></tr>");
			}
			
			out.print("<input type='hidden' name='action' value='save'/>");
			
			out.println("<tr><th colspan='2'>PVOutput Integration Service v" + Main.VERSION + "</th></tr>");
			
			out.println("<tr class='e'>");
			out.println("<td>System Id</td>");
			out.println("<td><input type='text' name='sid' size='8' value=" + sid + "></td>");
			out.println("</tr>");
			out.println("<tr class='e'><td colspan='2'><small class='comment'>Your System Id is shown in the pvoutput.org 'Settings' menu option</small></td></tr>");

			out.println("<tr class='o'><td>API Key</td>");
			out.println("<td><input type='text' name='key' size='50' value=" + key + "></td>");
			out.println("</tr>");
			out.println("<tr class='o'><td colspan='2'><small class='comment'>Request an API key from the pvoutput.org 'Settings' menu option</small></td></tr>");
			
			out.println("<tr class='e'><td>Format</td>");
			out.print("<td><select name='format'>");
			out.print(writeOption("simple", "Simple", format));
			out.print(writeOption("aurora", "Aurora", format));
			out.print(writeOption("currentcost", "Current Cost", format));
			out.print(writeOption("enasolar", "Enasolar", format));
			out.print(writeOption("enphase", "Enphase", format));
			out.print(writeOption("flukso", "Flukso", format));
			out.print(writeOption("growatt", "Growatt", format));
			out.print(writeOption("kaco", "Kaco", format));
			out.print(writeOption("owl", "Owl", format));
			out.print(writeOption("pvmate", "PV Mate", format));
			out.print(writeOption("solarlog", "SolarLog", format));
			out.print(writeOption("sunnyroo", "Sunny Roo", format));
			out.print(writeOption("suntellite", "Suntellite", format));
			out.print(writeOption("ted5000", "TED5000", format));
			out.print(writeOption("xantrex", "Xantrex", format));
			out.println("</select></td></tr>");
			out.println("<tr class='e'><td colspan='2'><small class='comment'>The data format of inverter logs</small></td></tr>");

			out.println("<tr class='o'><td>Directory</td>");
			out.println("<td><input type='text' name='dir' size='20' value=" + dir + "></td>");
			out.println("</tr>");
			out.println("<tr class='o'><td colspan='2'><small class='comment'>The directory where your daily data logs are stored (use forward slashes '/' and no quotes)</small></td></tr>");

			out.println("<tr class='e'><td>End Time</td>");
			out.println("<td><input type='text' name='endtime' size='8' value=" + endtime + "></td>");
			out.println("</tr>");
			out.println("<tr class='e'><td colspan='2'><small class='comment'>Records will not be processed after this time if there is no power and energy has not changed</small></td></tr>");
			
			out.println("<tr class='o'><td>Direction</td>");
			out.println("<td><input type='text' name='direction' size='8' value=" + direction + "></td>");
			out.println("</tr>");
			out.println("<tr class='o'><td colspan='2'><small class='comment'>Optional - Update to change the default direction. 'Out' - solar generation, 'In' - energy consumption</small></td></tr>");

			out.println("<tr class='e'><td>Timezone</td>");
			out.println("<td><input type='text' name='timezone' size='30' value=" + timezone + "></td>");
			out.println("</tr>");
			out.println("<tr class='e'><td colspan='2'><small class='comment'>Optional - update to use a different timezone</small></td></tr>");

			if("currentcost".equals(format))
			{
				out.println("<tr><th colspan='2'>Current Cost</th></tr>");
				
				out.println("<tr class='e'><td>Port</td>");
				out.println("<td><input type='text' size='20' value='auto'></td>");
				out.println("</tr>");
				out.println("<tr class='e'><td colspan='2'><small class='comment'>change to current cost com port e.g. 'COM5', use 'auto' to let the service choose the port</small></td></tr>");
			
				out.println("<tr class='o'><td>Sensors</td>");
				out.println("<td><input type='text' size='20' value=''></td>");
				out.println("</tr>");
				out.println("<tr class='o'><td colspan='2'><small class='comment'>change to current cost com port e.g. 'COM5', use 'auto' to let the service choose the port</small></td></tr>");
			
				out.println("<tr class='e'><td>Direction</td>");
				out.println("<td><input type='text' size='20' value=''></td>");
				out.println("</tr>");
				out.println("<tr class='e'><td colspan='2'><small class='comment'>change to current cost com port e.g. 'COM5', use 'auto' to let the service choose the port</small></td></tr>");
			
				out.println("<tr class='o'><td>Channels</td>");
				out.println("<td><input type='text' size='20' value=''></td>");
				out.println("</tr>");
				out.println("<tr class='o'><td colspan='2'><small class='comment'>change to current cost com port e.g. 'COM5', use 'auto' to let the service choose the port</small></td></tr>");
			
				out.println("<tr class='e'><td>Rule</td>");
				out.println("<td><input type='text' size='40' value=''></td>");
				out.println("</tr>");
				out.println("<tr class='e'><td colspan='2'><small class='comment'>change to current cost com port e.g. 'COM5', use 'auto' to let the service choose the port</small></td></tr>");
			
				out.println("<tr class='o'><td>Temperature Unit</td>");
				out.println("<td><input type='text' size='20' value='C'></td>");
				out.println("</tr>");
				out.println("<tr class='o'><td colspan='2'><small class='comment'>change to current cost com port e.g. 'COM5', use 'auto' to let the service choose the port</small></td></tr>");
			
			}

			out.println("<tr>");
			out.println("<td colspan='2'><center><input type='submit' value='Save'></center></td>");
			out.println("</tr>");
			
			out.println("</table></p>");
		}
        
        out.println("</body></html>");
        
        ((Request)request).setHandled(true);	
	}

}
